db.workingCard.updateMany({},
    {
        $set: {
            "draftCard.attachments": [],
            "publishedCard.attachments": []
        }
    }
)