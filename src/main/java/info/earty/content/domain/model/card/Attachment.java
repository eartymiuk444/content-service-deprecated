package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Attachment {

    AttachmentId attachmentId;
    String filename;

    public static Attachment create(AttachmentId attachmentId, String filename) {
        Assert.notNull(attachmentId, Attachment.class.getSimpleName() + " : attachment id cannot be null");
        Assert.notNull(attachmentId, Attachment.class.getSimpleName() + " : filename cannot be null");
        return new Attachment(attachmentId, filename);
    }

}
