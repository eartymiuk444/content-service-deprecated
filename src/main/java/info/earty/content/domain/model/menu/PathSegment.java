package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.common.Rfc3986Patterns;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class PathSegment {

    String pathSegmentString;

    public PathSegment(String pathSegmentString) {
        Assert.notNull(pathSegmentString, "Error creating path segment; path segment string cannot be null");
        Assert.isTrue(Rfc3986Patterns.PATH_SEGMENT_PATTERN.matcher(pathSegmentString).matches(), this.getClass().getSimpleName() + ": invalid path segment string");

        this.pathSegmentString = pathSegmentString;
    }
}
