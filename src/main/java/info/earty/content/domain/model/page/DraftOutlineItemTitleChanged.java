package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftOutlineItemTitleChanged implements WorkingPageDomainEvent {

    WorkingPageId workingPageId;
    WorkingCardId workingCardId;
    String title;
    Instant occurredOn;

    public static DraftOutlineItemTitleChanged create(WorkingPageId workingPageId, WorkingCardId workingCardId, String title) {
        Assert.notNull(workingPageId, DraftOutlineItemTitleChanged.class.getSimpleName() + ": working page id cannot be null");
        Assert.notNull(workingCardId, DraftOutlineItemTitleChanged.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(title, DraftOutlineItemTitleChanged.class.getSimpleName() + ": title cannot be null");

        return new DraftOutlineItemTitleChanged(workingPageId, workingCardId, title, Instant.now());
    }
}
