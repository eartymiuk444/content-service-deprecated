package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class OutlineItemRemoved implements WorkingPageDomainEvent {

    WorkingPageId workingPageId;
    Set<WorkingCardId> publishedOutlineItems;
    Set<WorkingCardId> draftOutlineItemsRemoved;
    Instant occurredOn;

    static OutlineItemRemoved create(WorkingPageId workingPageId, Set<WorkingCardId> publishedOutlineItems,
                                     Set<WorkingCardId> outlineItemsRemoved) {
        Assert.notNull(workingPageId, OutlineItemRemoved.class.getSimpleName() + ": working page id cannot be null");
        Assert.notNull(publishedOutlineItems, OutlineItemRemoved.class.getSimpleName() + ": published outline items cannot be null");
        Assert.notNull(outlineItemsRemoved, OutlineItemRemoved.class.getSimpleName() + ": outline items removed cannot be null");
        return new OutlineItemRemoved(workingPageId, new HashSet<>(publishedOutlineItems), new HashSet<>(outlineItemsRemoved), Instant.now());
    }

    public Set<WorkingCardId> publishedOutlineItems() {
        return new HashSet<>(this.publishedOutlineItems);
    }

    public Set<WorkingCardId> draftOutlineItemsRemoved() {
        return new HashSet<>(this.draftOutlineItemsRemoved);
    }

    public Set<WorkingCardId> orphanedOutlineItems() {
        return this.draftOutlineItemsRemoved.stream().filter(draftOutlineItemRemoved ->
                !this.publishedOutlineItems.contains(draftOutlineItemRemoved)).collect(Collectors.toSet());
    }

}
