package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Directory {

    @EqualsAndHashCode.Include
    private final DirectoryId id;

    private DirectoryItemName name;
    private PathSegment pathSegment;
    private final List<DirectoryItem> items;

    public DirectoryId id() {
        return this.id;
    }

    public DirectoryItemName name() {
        return this.name;
    }

    public boolean hasPathSegment() {
        return this.pathSegment != null;
    }

    public PathSegment pathSegment() {
        Assert.isTrue(this.hasPathSegment(), this.getClass().getSimpleName() + ": instance does not have a path segment");
        return this.pathSegment;
    }

    public List<DirectoryItem> items() {
        return new ArrayList<>(this.items);
    }

    public void moveSubDirectoryUp(DirectoryId directoryId) {
        Optional<DirectoryItem> oSubDirectory = this.findSubDirectory(directoryId);
        Assert.isTrue(oSubDirectory.isPresent(), "Error moving sub-directory up; sub-directory not found");
        this.moveDirectoryItemUpHelper(oSubDirectory.get());
    }

    public void moveSubDirectoryDown(DirectoryId directoryId) {
        Optional<DirectoryItem> oSubDirectory = this.findSubDirectory(directoryId);
        Assert.isTrue(oSubDirectory.isPresent(), "Error moving sub-directory down; sub-directory not found");
        this.moveDirectoryItemDownHelper(oSubDirectory.get());
    }

    public void movePageUp(WorkingPageId workingPageId) {
        Optional<DirectoryItem> oPage = this.findPage(workingPageId);
        Assert.isTrue(oPage.isPresent(), "Error moving page up; page not found");
        this.moveDirectoryItemUpHelper(oPage.get());
    }

    public void movePageDown(WorkingPageId workingPageId) {
        Optional<DirectoryItem> oPage = this.findPage(workingPageId);
        Assert.isTrue(oPage.isPresent(), "Error moving page down; page not found");
        this.moveDirectoryItemDownHelper(oPage.get());
    }

    static Directory create(DirectoryId id, DirectoryItemName name, PathSegment pathSegment) {
        Assert.notNull(id, "Error creating directory; id cannot be null");
        Assert.notNull(pathSegment, "Error creating non-root directory; path segment cannot be null");

        Directory directory = new Directory(id, new ArrayList<>());
        directory.setName(name);
        directory.setPathSegment(pathSegment);
        return directory;
    }

    static Directory createRoot(DirectoryId id, DirectoryItemName name) {
        Assert.notNull(id, "Error creating directory; id cannot be null");

        Directory directory = new Directory(id, new ArrayList<>());
        directory.setName(name);
        return directory;
    }

    Optional<DirectoryItem> findSubDirectory(DirectoryId directoryId) {
        return this.items().stream().filter(item -> item.isSubDirectory() && item.directoryId().equals(directoryId)).findAny();
    }

    Optional<DirectoryItem> findPage(WorkingPageId workingPageId) {
        return this.items().stream().filter(item -> item.isPage() && item.workingPageId().equals(workingPageId)).findAny();
    }

    void addSubDirectory(Directory directory) {
        Assert.isTrue(this.findSubDirectory(directory.id()).isEmpty(),
                "Error adding sub-directory to directory; the directory already contains a sub-directory with the directory id");
        Assert.isTrue(this.items().stream().noneMatch(item -> item.pathSegment().equals(directory.pathSegment())),
                "Error adding sub-directory to directory; the directory already contains an item with the path segment");

        DirectoryItem subDirectory = DirectoryItem.create(directory);
        this.items.add(subDirectory);
    }

    void addPage(WorkingPageId workingPageId, DirectoryItemName name, PathSegment pathSegment) {
        Assert.isTrue(this.findPage(workingPageId).isEmpty(),
                "Error adding page to directory; the directory already contains a page with the working page id");
        Assert.isTrue(this.items().stream().noneMatch(item -> item.pathSegment().equals(pathSegment)),
                "Error adding page to directory; the directory already contains an item with the path segment");

        DirectoryItem page = DirectoryItem.create(workingPageId, name, pathSegment);
        this.items.add(page);
    }

    void removeSubDirectory(DirectoryId directoryId) {
        Optional<DirectoryItem> oSubDirectory = this.findSubDirectory(directoryId);
        Assert.isTrue(oSubDirectory.isPresent(), "Error removing sub-directory; sub-directory not found");
        DirectoryItem subDirectory = oSubDirectory.get();

        this.items.remove(subDirectory);
    }

    void addSubDirectory(DirectoryItem subDirectory) {
        Assert.isTrue(subDirectory.isSubDirectory(), "Error adding sub-directory to directory; directory item is not a sub-directory");
        Assert.isTrue(this.findSubDirectory(subDirectory.directoryId()).isEmpty(),
                "Error adding sub-directory to directory; the directory already contains a sub-directory with the directory id");
        Assert.isTrue(this.items().stream().noneMatch(existingDirectoryItem ->
                        existingDirectoryItem.pathSegment().equals(subDirectory.pathSegment())),
                "Error adding sub-directory to directory; the directory already contains a directory item with the path segment");

        this.items.add(subDirectory);
    }

    void removePage(WorkingPageId workingPageId) {
        Optional<DirectoryItem> oPage = this.findPage(workingPageId);
        Assert.isTrue(oPage.isPresent(), "Error removing page; page not found");
        DirectoryItem page = oPage.get();

        this.items.remove(page);
    }

    void addPage(DirectoryItem page) {
        Assert.isTrue(page.isPage(), "Error adding page to directory; directory item is not a page");
        Assert.isTrue(this.findPage(page.workingPageId()).isEmpty(),
                "Error adding page to directory; the directory already contains a page with the working page id");
        Assert.isTrue(this.items().stream().noneMatch(existingDirectoryItem ->
                        existingDirectoryItem.pathSegment().equals(page.pathSegment())),
                "Error adding page to directory; the directory already contains a directory item with the path segment");

        this.items.add(page);
    }

    void changeName(DirectoryItemName name) {
        this.setName(name);
    }

    void changePathSegment(PathSegment pathSegment) {
        Assert.notNull(pathSegment, "Error changing path segment; path segment cannot be null");
        this.setPathSegment(pathSegment);
    }

    void changeSubDirectoryName(DirectoryId directoryId, DirectoryItemName name) {
        Assert.notNull(directoryId, "Error changing sub-directory name; directory id cannot be null");

        Optional<DirectoryItem> oDirectoryItem = this.findSubDirectory(directoryId);
        Assert.isTrue(oDirectoryItem.isPresent(), "Error changing sub-directory name; sub-directory not found");
        DirectoryItem subDirectory = oDirectoryItem.get();

        int position = this.items().indexOf(subDirectory);
        this.items.set(position, subDirectory.changeName(name));
    }

    void changeSubDirectoryPathSegment(DirectoryId directoryId, PathSegment pathSegment) {
        Assert.notNull(directoryId, "Error changing sub-directory path segment; directory id cannot be null");

        Optional<DirectoryItem> oDirectoryItem = this.findSubDirectory(directoryId);
        Assert.isTrue(oDirectoryItem.isPresent(), "Error changing sub-directory path segment; sub-directory not found");

        Assert.isTrue(this.items().stream().noneMatch(existingDirectoryItem ->
                        existingDirectoryItem.pathSegment().equals(pathSegment)),
                "Error changing sub-directory path segment; the directory already contains a directory item with the path segment");

        DirectoryItem subDirectory = oDirectoryItem.get();

        int position = this.items().indexOf(subDirectory);
        this.items.set(position, subDirectory.changePathSegment(pathSegment));
    }

    void changePageName(WorkingPageId workingPageId, DirectoryItemName name) {
        Assert.notNull(workingPageId, "Error changing page name; working page id cannot be null");

        Optional<DirectoryItem> oDirectoryItem = this.findPage(workingPageId);
        Assert.isTrue(oDirectoryItem.isPresent(), "Error changing page name; page corresponding to working page not found");
        DirectoryItem page = oDirectoryItem.get();

        int position = this.items().indexOf(page);
        this.items.set(position, page.changeName(name));
    }

    void changePagePathSegment(WorkingPageId workingPageId, PathSegment pathSegment) {
        Assert.notNull(workingPageId, "Error changing page path segment; working page id cannot be null");

        Optional<DirectoryItem> oDirectoryItem = this.findPage(workingPageId);
        Assert.isTrue(oDirectoryItem.isPresent(), "Error changing page path segment; page not found");

        Assert.isTrue(this.items().stream().noneMatch(existingDirectoryItem ->
                        existingDirectoryItem.pathSegment().equals(pathSegment)),
                "Error changing page path segment; the directory already contains a directory item with the path segment");

        DirectoryItem page = oDirectoryItem.get();

        int position = this.items().indexOf(page);
        this.items.set(position, page.changePathSegment(pathSegment));
    }

    private void setName(DirectoryItemName name) {
        Assert.notNull(name, this.getClass().getSimpleName() + ": name cannot be null");
        this.name = name;
    }

    private void setPathSegment(PathSegment pathSegment) {
        this.pathSegment = pathSegment;
    }

    private void moveDirectoryItemUpHelper(DirectoryItem movingUp) {
        int itemPosition = this.items().indexOf(movingUp);
        Assert.isTrue(itemPosition != 0,
                "Error moving directory item up; directory item is already at the highest position");

        Collections.swap(this.items, itemPosition, itemPosition - 1);
    }

    private void moveDirectoryItemDownHelper(DirectoryItem movingDown) {
        int itemPosition = this.items().indexOf(movingDown);
        Assert.isTrue(itemPosition != this.items().size() - 1,
                "Error moving directory item down; directory item is already at the lowest position");

        Collections.swap(this.items, itemPosition, itemPosition + 1);
    }
}
