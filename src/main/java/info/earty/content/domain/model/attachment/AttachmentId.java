package info.earty.content.domain.model.attachment;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class AttachmentId {
    String id;

    public static AttachmentId create(String attachmentId) {
        Assert.notNull(attachmentId, AttachmentId.class.getSimpleName() + " : attachment id cannot be null");
        return new AttachmentId(attachmentId);
    }
}
