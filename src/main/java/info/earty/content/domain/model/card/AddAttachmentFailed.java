package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AddAttachmentFailed implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;
    AttachmentId attachmentId;
    Instant occurredOn;

    public static AddAttachmentFailed create(WorkingCardId workingCardId, AttachmentId attachmentId) {
        Assert.notNull(workingCardId, AddAttachmentFailed.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(attachmentId, AddAttachmentFailed.class.getSimpleName() + ": attachment id cannot be null");

        return new AddAttachmentFailed(workingCardId, attachmentId, Instant.now());
    }
}
