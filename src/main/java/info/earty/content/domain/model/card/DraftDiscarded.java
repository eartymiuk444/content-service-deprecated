package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftDiscarded implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;
    
    List<ImageId> priorDraftImages;
    List<ImageId> currentImages;

    List<AttachmentId> priorDraftAttachments;
    List<AttachmentId> currentAttachments;
    
    Instant occurredOn;

    public List<ImageId> priorDraftImages() {
        return new ArrayList<>(priorDraftImages);
    }

    public List<ImageId> currentImages() {
        return new ArrayList<>(currentImages);
    }
    
    public List<AttachmentId> priorDraftAttachments() {
        return new ArrayList<>(priorDraftAttachments);
    }

    public List<AttachmentId> currentAttachments() {
        return new ArrayList<>(currentAttachments);
    }

    static DraftDiscarded create(WorkingCardId workingCardId, List<ImageId> priorDraftImages, List<ImageId> currentImages,
                                 List<AttachmentId> priorDraftAttachments, List<AttachmentId> currentAttachments) {

        Assert.notNull(workingCardId, DraftDiscarded.class.getSimpleName() + " : working card id cannot be null");
        Assert.notNull(priorDraftImages, DraftDiscarded.class.getSimpleName() + " : prior draft images cannot be null");
        Assert.notNull(currentAttachments, DraftDiscarded.class.getSimpleName() + " : current attachments cannot be null");
        Assert.notNull(priorDraftAttachments, DraftDiscarded.class.getSimpleName() + " : prior draft attachments cannot be null");
        Assert.notNull(currentImages, DraftDiscarded.class.getSimpleName() + " : current images cannot be null");

        return new DraftDiscarded(workingCardId, 
                new ArrayList<>(priorDraftImages), 
                new ArrayList<>(currentImages),
                new ArrayList<>(priorDraftAttachments),
                new ArrayList<>(currentAttachments),
                Instant.now());
    }

    public Set<ImageId> draftImagesRemoved() {
        return this.priorDraftImages().stream().filter(priorDraftImageId ->
                !currentImages().contains(priorDraftImageId)).collect(Collectors.toSet());
    }

    public Set<AttachmentId> draftAttachmentsRemoved() {
        return this.priorDraftAttachments().stream().filter(priorDraftAttachmentId ->
                !currentAttachments().contains(priorDraftAttachmentId)).collect(Collectors.toSet());
    }

}
