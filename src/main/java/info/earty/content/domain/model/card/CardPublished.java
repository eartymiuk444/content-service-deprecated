package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class CardPublished implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;

    List<ImageId> priorPublishedImages;
    List<ImageId> currentPublishedImages;

    List<AttachmentId> priorPublishedAttachments;
    List<AttachmentId> currentPublishedAttachments;

    Instant occurredOn;

    public List<ImageId> priorPublishedImages() {
        return new ArrayList<>(priorPublishedImages);
    }

    public List<ImageId> currentPublishedImages() {
        return new ArrayList<>(currentPublishedImages);
    }

    public List<AttachmentId> priorPublishedAttachments() {
        return new ArrayList<>(priorPublishedAttachments);
    }

    public List<AttachmentId> currentPublishedAttachments() {
        return new ArrayList<>(currentPublishedAttachments);
    }

    static CardPublished create(WorkingCardId workingCardId,
                                List<ImageId> priorPublishedImages,
                                List<ImageId> currentPublishedImages,
                                List<AttachmentId> priorPublishedAttachments,
                                List<AttachmentId> currentPublishedAttachments) {

        Assert.notNull(workingCardId, CardPublished.class.getSimpleName() + " : working card id cannot be null");
        Assert.notNull(priorPublishedImages, CardPublished.class.getSimpleName() + " : prior published images cannot be null");
        Assert.notNull(currentPublishedImages, CardPublished.class.getSimpleName() + " : current published images cannot be null");
        Assert.notNull(priorPublishedAttachments, CardPublished.class.getSimpleName() + " : prior published attachments cannot be null");
        Assert.notNull(currentPublishedAttachments, CardPublished.class.getSimpleName() + " : current published attachments cannot be null");
        return new CardPublished(workingCardId,
                new ArrayList<>(priorPublishedImages),
                new ArrayList<>(currentPublishedImages),
                new ArrayList<>(priorPublishedAttachments),
                new ArrayList<>(currentPublishedAttachments),
                Instant.now());
    }

    public Set<ImageId> publishedImagesRemoved() {
        return priorPublishedImages.stream().filter(previous -> !currentPublishedImages.contains(previous)).collect(Collectors.toSet());
    }

    public Set<ImageId> publishedImagesAdded() {
        return currentPublishedImages.stream().filter(current -> !priorPublishedImages.contains(current)).collect(Collectors.toSet());
    }

    public Set<AttachmentId> publishedAttachmentsRemoved() {
        return priorPublishedAttachments.stream().filter(previous -> !currentPublishedAttachments.contains(previous)).collect(Collectors.toSet());
    }

    public Set<AttachmentId> publishedAttachmentsAdded() {
        return currentPublishedAttachments.stream().filter(current -> !priorPublishedAttachments.contains(current)).collect(Collectors.toSet());
    }

}
