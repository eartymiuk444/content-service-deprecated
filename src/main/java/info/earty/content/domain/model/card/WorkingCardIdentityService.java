package info.earty.content.domain.model.card;

public interface WorkingCardIdentityService {
    WorkingCardId generate();
}
