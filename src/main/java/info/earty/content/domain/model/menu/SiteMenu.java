package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.DomainEventPublisher;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.*;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor(access=AccessLevel.PACKAGE)
public class SiteMenu implements Aggregate<SiteMenuId> {

    @EqualsAndHashCode.Include
    private final SiteMenuId id;

    private final DirectoryId rootDirectoryId;
    private final Map<DirectoryId, Directory> allDirectories;
    private final Map<WorkingPageId, DirectoryItem> allPages;
    private final Map<DirectoryId, DirectoryId> directoryParents;
    private final Map<WorkingPageId, DirectoryId> pageParents;

    private int nextDirectoryIdInt;

    public SiteMenuId id() {
        return this.id;
    }

    public Directory rootDirectory() {
        return this.allDirectories.get(this.rootDirectoryId);
    }

    public Optional<Directory> findDirectory(DirectoryId directoryId) {
        return this.allDirectories.containsKey(directoryId) ?
                Optional.of(this.allDirectories.get(directoryId)) : Optional.empty();
    }

    public Optional<DirectoryItem> findPage(WorkingPageId workingPageId) {
        return this.allPages.containsKey(workingPageId) ?
                Optional.of(this.allPages.get(workingPageId)) : Optional.empty();
    }

    public void addDirectory(DirectoryItemName name, PathSegment pathSegment, DirectoryId parentId) {
        Assert.notNull(name, "Error adding directory to parent; name cannot be null");
        Assert.isTrue(this.findDirectory(parentId).isPresent(),
                "Error adding directory to parent; directory with parent id not found");

        DirectoryId nextDirectoryId = this.nextDirectoryId();
        Directory directory = Directory.create(nextDirectoryId, name, pathSegment);

        this.allDirectories.put(nextDirectoryId, directory);
        this.findDirectory(parentId).get().addSubDirectory(directory);
        this.directoryParents.put(nextDirectoryId, parentId);
    }

    public void addPage(WorkingPageId workingPageId, DirectoryItemName name, PathSegment pathSegment, DirectoryId parentId) {
        Assert.isTrue(this.findPage(workingPageId).isEmpty(), "Error adding page to parent; menu already contains this page");
        Assert.isTrue(this.findDirectory(parentId).isPresent(),
                "Error adding page to parent); directory with parent id not found");

        Directory parentDirectory = this.findDirectory(parentId).get();
        this.findDirectory(parentId).get().addPage(workingPageId, name, pathSegment);
        this.allPages.put(workingPageId, parentDirectory.findPage(workingPageId).get());
        this.pageParents.put(workingPageId, parentId);

        DomainEventPublisher.instance().publish(PageAdded.create(this.id(), workingPageId, name));
    }

    public void moveDirectory(DirectoryId directoryId, DirectoryId parentId) {
        Optional<Directory> oDirectory = this.findDirectory(directoryId);
        Assert.isTrue(oDirectory.isPresent(),
                "Error moving directory to new parent; no directory found for directory id");
        Assert.isTrue(!directoryId.equals(this.rootDirectoryId), "Error moving directory; root directory cannot be moved");
        Assert.isTrue(!directoryId.equals(parentId), this.getClass().getSimpleName() + ": cannot make a directory its own parent");

        Optional<Directory> oNewParentDirectory = this.findDirectory(parentId);
        Assert.isTrue(oNewParentDirectory.isPresent(),
                "Error moving directory to new parent; no directory found for parent id");

        Assert.isTrue(this.directoryParents.get(directoryId) != parentId,
                "Error moving directory to new parent; the proposed new parent is already this directory's parent");

        Directory directory = oDirectory.get();
        Assert.isTrue(this.directAndInheritedSubDirectories(directory).stream().noneMatch(directAndDerivedSubDirectory ->
                        directAndDerivedSubDirectory.directoryId().equals(parentId)),
                "Error moving directory to new parent; " +
                        "the directory contains the proposed new parent as a sub-directory which introduces an infinite cycle");

        Directory newParentDirectory = oNewParentDirectory.get();
        Directory currParentDirectory = this.findDirectory(this.directoryParents.get(directoryId)).get();
        DirectoryItem subDirectory = currParentDirectory.findSubDirectory(directoryId).get();

        currParentDirectory.removeSubDirectory(directoryId);
        newParentDirectory.addSubDirectory(subDirectory);
        this.directoryParents.put(directoryId, parentId);
    }

    public void movePage(WorkingPageId workingPageId, DirectoryId parentId) {
        Optional<DirectoryItem> oPage = this.findPage(workingPageId);
        Assert.isTrue(oPage.isPresent(),
                "Error moving page to new directory; no page found for working page id");

        Optional<Directory> oNewParentDirectory = this.findDirectory(parentId);
        Assert.isTrue(oNewParentDirectory.isPresent(),
                "Error moving page to new directory; no directory found for parent id");

        Assert.isTrue(this.pageParents.get(workingPageId) != parentId,
                "Error moving page to new directory; the proposed new parent is already this page's parent");

        DirectoryItem page = oPage.get();
        Directory newParentDirectory = oNewParentDirectory.get();
        Directory currParentDirectory = this.findDirectory(this.pageParents.get(workingPageId)).get();

        currParentDirectory.removePage(page.workingPageId());
        newParentDirectory.addPage(page);
        this.pageParents.put(workingPageId, parentId);
    }

    public void changeDirectoryName(DirectoryId directoryId, DirectoryItemName name) {
        Optional<Directory> oDirectory = this.findDirectory(directoryId);
        Assert.isTrue(oDirectory.isPresent(), "Error changing directory name; directory with id not found");
        Assert.isTrue(!directoryId.equals(this.rootDirectoryId),
                "Error changing directory name; cannot change the name of the root directory");

        Directory directory = oDirectory.get();
        directory.changeName(name);

        Directory parentDirectory = this.findDirectory(this.directoryParents.get(directoryId)).get();
        parentDirectory.changeSubDirectoryName(directoryId, name);
    }

    public void changeDirectoryPathSegment(DirectoryId directoryId, PathSegment pathSegment) {
        Optional<Directory> oDirectory = this.findDirectory(directoryId);
        Assert.isTrue(oDirectory.isPresent(), "Error changing directory path segment; directory with id not found");
        Assert.isTrue(!directoryId.equals(this.rootDirectoryId),
                "Error changing directory path segment; cannot change the path segment of the root directory");

        Directory directory = oDirectory.get();
        directory.changePathSegment(pathSegment);

        Directory parentDirectory = this.findDirectory(this.directoryParents.get(directoryId)).get();
        parentDirectory.changeSubDirectoryPathSegment(directoryId, pathSegment);
    }

    public void changePageName(WorkingPageId workingPageId, DirectoryItemName name) {
        Optional<DirectoryItem> oPage = this.findPage(workingPageId);
        Assert.isTrue(oPage.isPresent(), "Error changing page name; page corresponding to working page not found");

        DirectoryItem page = oPage.get();
        this.allPages.put(workingPageId, page.changeName(name));

        Directory parentDirectory = this.findDirectory(this.pageParents.get(workingPageId)).get();
        parentDirectory.changePageName(workingPageId, name);
    }

    public void changePagePathSegment(WorkingPageId workingPageId, PathSegment pathSegment) {
        Optional<DirectoryItem> oPage = this.findPage(workingPageId);
        Assert.isTrue(oPage.isPresent(), "Error changing page path segment; page with id not found");

        DirectoryItem page = oPage.get();
        this.allPages.put(workingPageId, page.changePathSegment(pathSegment));

        Directory parentDirectory = this.findDirectory(this.pageParents.get(workingPageId)).get();
        parentDirectory.changePagePathSegment(workingPageId, pathSegment);
    }

    public void removeDirectory(DirectoryId directoryId) {
        DomainEventPublisher.instance().publish(this.removeDirectoryInternal(directoryId));
    }

    private DirectoryRemoved removeDirectoryInternal(DirectoryId directoryId) {
        Optional<Directory> oDirectory = this.findDirectory(directoryId);
        Assert.isTrue(oDirectory.isPresent(),
                "Error removing the directory; no directory found with id");
        Assert.isTrue(!directoryId.equals(this.rootDirectoryId), "Error removing directory; cannot remove the root directory");

        Directory directory = oDirectory.get();

        Set<WorkingPageId> pagesRemoved = new HashSet<>();

        directory.items().forEach(directoryItem -> {
            if (directoryItem.isSubDirectory()) {
                DirectoryRemoved directoryRemoved = this.removeDirectoryInternal(directoryItem.directoryId());
                pagesRemoved.addAll(directoryRemoved.pagesRemoved());
            }
            else {
                PageRemoved pageRemoved = this.removePageInternal(directoryItem.workingPageId());
                pagesRemoved.add(pageRemoved.workingPageId());
            }
        });

        Directory parent = this.findDirectory(this.directoryParents.get(directoryId)).get();
        parent.removeSubDirectory(directoryId);
        this.directoryParents.remove(directoryId);
        this.allDirectories.remove(directoryId);

        return DirectoryRemoved.create(this.id, pagesRemoved);
    }

    public void removePage(WorkingPageId workingPageId) {
        DomainEventPublisher.instance().publish(this.removePageInternal(workingPageId));
    }

    void setNextDirectoryIdInt(int nextDirectoryIdInt) {
        this.nextDirectoryIdInt = nextDirectoryIdInt;
    }

    private PageRemoved removePageInternal(WorkingPageId workingPageId) {
        Assert.isTrue(this.findPage(workingPageId).isPresent(),
                "Error removing the page; no page found with id");

        Directory parent = this.findDirectory(this.pageParents.get(workingPageId)).get();
        parent.removePage(workingPageId);
        this.pageParents.remove(workingPageId);
        this.allPages.remove(workingPageId);

        return PageRemoved.create(this.id, workingPageId);
    }

    private DirectoryId nextDirectoryId() {
        DirectoryId nextDirectoryId = new DirectoryId(this.nextDirectoryIdInt);
        this.setNextDirectoryIdInt(this.nextDirectoryIdInt + 1);
        return nextDirectoryId;
    }

    private Collection<DirectoryItem> directAndInheritedSubDirectories(Directory directory) {
        Collection<DirectoryItem> directAndInheritedSubDirectories = directory.items().stream().filter(
                DirectoryItem::isSubDirectory).collect(Collectors.toSet());
        directory.items().stream().filter(DirectoryItem::isSubDirectory).forEach(subDirectory ->
                directAndInheritedSubDirectories.addAll(this.directAndInheritedSubDirectories(
                        this.findDirectory(subDirectory.directoryId()).get())));
        return directAndInheritedSubDirectories;
    }
}
