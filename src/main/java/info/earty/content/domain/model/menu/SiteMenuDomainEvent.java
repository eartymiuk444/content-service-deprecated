package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.common.DomainEvent;

public interface SiteMenuDomainEvent extends DomainEvent {
    SiteMenuId siteMenuId();
}
