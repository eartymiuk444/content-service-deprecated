package info.earty.content.domain.model.common;

import java.time.Instant;

public interface DomainEvent {
    Instant occurredOn();
}
