package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Draft {

    @EqualsAndHashCode.Include
    private final DraftId id;

    private Title title;

    private final Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems;
    private final Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItems;

    public final DraftId id() {
        return this.id;
    }

    public Title title() {
        return this.title;
    }

    public DraftOutlineItem rootOutlineItem() {
        return this.findOutlineItem(DraftOutlineItemId.createRootId()).get();
    }

    public Optional<DraftOutlineItem> findOutlineItem(WorkingCardId workingCardId) {
        return this.findOutlineItem(DraftOutlineItemId.create(workingCardId));
    }

    public Set<WorkingCardId> allWorkingCardIds() {
        return this.allOutlineItems.keySet().stream().filter(DraftOutlineItemId::isNonRootId)
                .map(DraftOutlineItemId::workingCardId).collect(Collectors.toSet());
    }

    public void changeTitle(Title title) {
        this.setTitle(title);
    }

    public void addFragmentToOutlineItem(WorkingCardId workingCardId, Fragment fragment) {
        Assert.notNull(workingCardId, "Error adding a fragment to outline item; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error adding a fragment to outline item; no outline item found for working card id");

        Assert.isTrue(this.allOutlineItems.values().stream().filter(DraftOutlineItem::hasFragment)
                .noneMatch(o -> o.fragment().equals(fragment)), this.getClass().getSimpleName() +
                ": adding this fragment results in duplicate fragments");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.addFragment(fragment);

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.addFragmentToSubItem(workingCardId, fragment);
    }

    public void changeOutlineItemFragment(WorkingCardId workingCardId, Fragment fragment) {
        Assert.notNull(workingCardId, "Error changing the outline item's fragment; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error changing the outline item's fragment; no outline item found for working card id");

        Assert.isTrue(this.allOutlineItems.values().stream().filter(DraftOutlineItem::hasFragment)
                .noneMatch(o -> o.fragment().equals(fragment)), this.getClass().getSimpleName() +
                ": changing this fragment results in duplicate fragments");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.changeFragment(fragment);

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.changeSubItemFragment(workingCardId, fragment);
    }

    public void removeOutlineItemFragment(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error changing the outline item's fragment; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error removing the outline item's fragment; no outline item found for working card id");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.removeFragment();

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.removeSubItemFragment(workingCardId);
    }

    public void moveOutlineItemToRoot(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error moving outline item to root; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error moving outline item to root; no outline item found for working card id");

        DraftOutlineItem rootOutlineItem = this.rootOutlineItem();
        Assert.isTrue(rootOutlineItem.findSubItem(workingCardId).isEmpty(),
                "Error moving outline item to root; this outline item already has the root as its parent");

        DraftOutlineItem outlineItem = oOutlineItem.get();

        DraftOutlineItem currParentOutlineItem = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        DraftOutlineSubItem subItem = currParentOutlineItem.findSubItem(workingCardId).get();

        currParentOutlineItem.removeSubItem(workingCardId);
        rootOutlineItem.addSubItem(subItem);
        this.parentOutlineItems.put(outlineItem.id(), rootOutlineItem.id());
    }

    public void moveOutlineItemToParent(WorkingCardId workingCardId, WorkingCardId parentId) {
        Assert.notNull(workingCardId, "Error moving outline item to new parent; working card id can't be null");
        Assert.notNull(parentId, "Error moving outline item to new parent; parent id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error moving outline item to new parent; no outline item found for working card id");

        Assert.isTrue(!workingCardId.equals(parentId), this.getClass().getSimpleName() + ": cannot make an outline item its own parent");

        Optional<DraftOutlineItem> oNewParentOutlineItem = this.findOutlineItem(parentId);
        Assert.isTrue(oNewParentOutlineItem.isPresent(),
                "Error moving outline item to new parent; no outline item found for parent id");
        DraftOutlineItem newParentOutlineItem = oNewParentOutlineItem.get();

        Assert.isTrue(newParentOutlineItem.findSubItem(workingCardId).isEmpty(),
                "Error moving outline item to new parent; the proposed new parent is already this outline items parent");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        Assert.isTrue(this.directAndInheritedSubItems(outlineItem).stream().noneMatch(directAndDerivedSubItem ->
                        directAndDerivedSubItem.workingCardId().equals(parentId)),
                "Error moving outline item to new parent; " +
                        "the outline item contains the proposed new parent as a sub-item which introduces an infinite cycle");

        DraftOutlineItem currParentOutlineItem = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        DraftOutlineSubItem subItem = currParentOutlineItem.findSubItem(workingCardId).get();
        currParentOutlineItem.removeSubItem(workingCardId);
        newParentOutlineItem.addSubItem(subItem);
        this.parentOutlineItems.put(outlineItem.id(), newParentOutlineItem.id());
    }

    void addOutlineItemToRoot(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error adding outline item to page draft; card id cannot be null");
        Assert.isTrue(this.findOutlineItem(workingCardId).isEmpty(),
                "Error adding outline item to page draft; draft already contains a card with id");

        DraftOutlineItem rootOutlineItem = this.rootOutlineItem();

        DraftOutlineItem outlineItem = DraftOutlineItem.create(workingCardId, title);
        this.allOutlineItems.put(outlineItem.id(), outlineItem);

        rootOutlineItem.addSubItem(workingCardId, title);
        this.parentOutlineItems.put(outlineItem.id(), rootOutlineItem.id());
    }

    void changeOutlineItemTitle(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error changing the outline item's title; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error changing the outline item's title; no outline item found for working card");

        DraftOutlineItem outlineItem = oOutlineItem.get();
        outlineItem.changeTitle(workingCardId, title);

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.changeSubItemTitle(workingCardId, title);
    }

    Set<WorkingCardId> removeOutlineItem(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error removing the outline item; working card id can't be null");

        Optional<DraftOutlineItem> oOutlineItem = this.findOutlineItem(workingCardId);
        Assert.isTrue(oOutlineItem.isPresent(),
                "Error removing the outline item; no outline item found for working card id");

        DraftOutlineItem outlineItem = oOutlineItem.get();

        Set<WorkingCardId> outlineItemsRemoved = new HashSet<>();
        outlineItem.subItems().forEach(s -> {
            outlineItemsRemoved.addAll(this.removeOutlineItem(s.workingCardId()));
        });

        DraftOutlineItem parent = this.findOutlineItem(this.parentOutlineItems.get(outlineItem.id())).get();
        parent.removeSubItem(workingCardId);

        this.parentOutlineItems.remove(outlineItem.id());
        this.allOutlineItems.remove(outlineItem.id());
        outlineItemsRemoved.add(workingCardId);
        return outlineItemsRemoved;
    }

    static Draft create(DraftId id, Title title) {
        Assert.notNull(id, "Error creating draft page; id cannot be null");

        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems = new HashMap<>();
        allOutlineItems.put(DraftOutlineItemId.createRootId(), DraftOutlineItem.createRoot());

        Draft draft = new Draft(id, allOutlineItems, new HashMap<>());
        draft.setTitle(title);
        return draft;
    }

    static Draft create(DraftId id, PublishedPage publishedPage) {
        Assert.notNull(id, "Error creating draft page; id cannot be null");
        Assert.notNull(publishedPage, "Error creating draft page; published page cannot be null");

        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems =
                draftItemsFromPublishedRoot(publishedPage.rootOutlineItem(), publishedPage);
        Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItems = new HashMap<>();

        allOutlineItems.values().forEach(draftOutlineItem -> draftOutlineItem.subItems().forEach(draftOutlineSubItem ->
                parentOutlineItems.put(DraftOutlineItemId.create(draftOutlineSubItem.workingCardId()), draftOutlineItem.id())));

        Draft draft = new Draft(id, allOutlineItems, parentOutlineItems);
        draft.setTitle(publishedPage.title());
        return draft;
    }

    private Optional<DraftOutlineItem> findOutlineItem(DraftOutlineItemId draftOutlineItemId) {
        return this.allOutlineItems.containsKey(draftOutlineItemId) ?
                Optional.of(this.allOutlineItems.get(draftOutlineItemId)) : Optional.empty();
    }

    private static Map<DraftOutlineItemId, DraftOutlineItem> draftItemsFromPublishedRoot(PublishedOutlineItem publishedOutlineItem, PublishedPage publishedPage)  {
        Map<DraftOutlineItemId, DraftOutlineItem> draftOutlineItems = new HashMap<>();
        DraftOutlineItem draftOutlineItem = DraftOutlineItem.create(publishedOutlineItem);
        draftOutlineItems.put(publishedOutlineItem.draftOutlineItemId(), draftOutlineItem);

        publishedOutlineItem.subItems().forEach(
                publishedOutlineSubItem -> {
                    draftOutlineItems.putAll(draftItemsFromPublishedRoot(publishedPage.findOutlineItem(
                            publishedOutlineSubItem.workingCardId()).get(), publishedPage));
                });
        return draftOutlineItems;
    }

    private Collection<DraftOutlineSubItem> directAndInheritedSubItems(DraftOutlineItem outlineItem) {
        Collection<DraftOutlineSubItem> directAndInheritedSubItems = new HashSet<>(outlineItem.subItems());
        outlineItem.subItems().forEach(subItem ->
            directAndInheritedSubItems.addAll(this.directAndInheritedSubItems(
                    this.findOutlineItem(subItem.workingCardId()).get())));
        return directAndInheritedSubItems;
    }

    private void setTitle(Title title) {
        Assert.notNull(title, this.getClass().getSimpleName() + ": title cannot be null");
        this.title = title;
    }
}