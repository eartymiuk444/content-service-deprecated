package info.earty.content.domain.model.page;

public interface WorkingPageIdentityService {
    WorkingPageId generate();
}
