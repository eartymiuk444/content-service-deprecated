package info.earty.content.domain.model.menu;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@Service
public class SiteMenuFactory {

    private static final String ROOT_DIRECTORY_NAME = "ROOT";
    private static final int INITIAL_DIRECTORY_ID = 1;

    public SiteMenu create(SiteMenuId siteMenuId) {
        Assert.notNull(siteMenuId, SiteMenuFactory.class.getSimpleName() + ": site menu id cannot be null");

        DirectoryId rootDirectoryId = new DirectoryId(INITIAL_DIRECTORY_ID);

        Map<DirectoryId, Directory> allDirectories = new HashMap<>();
        Directory rootDirectory = Directory.createRoot(rootDirectoryId, DirectoryItemName.create(ROOT_DIRECTORY_NAME));
        allDirectories.put(rootDirectoryId, rootDirectory);

        SiteMenu siteMenu = new SiteMenu(siteMenuId, new DirectoryId(INITIAL_DIRECTORY_ID), allDirectories, new HashMap<>(), new HashMap<>(), new HashMap<>());
        siteMenu.setNextDirectoryIdInt(INITIAL_DIRECTORY_ID + 1);
        return siteMenu;
    }

}
