package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class AttachmentRemoved implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;
    AttachmentId attachmentRemoved;
    boolean attachmentOrphaned;
    Instant occurredOn;

    static AttachmentRemoved create(WorkingCardId workingCardId, AttachmentId attachmentRemoved, boolean attachmentOrphaned) {
        Assert.notNull(workingCardId, AttachmentRemoved.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(attachmentRemoved, AttachmentRemoved.class.getSimpleName() + ": attachment removed cannot be null");
        return new AttachmentRemoved(workingCardId, attachmentRemoved, attachmentOrphaned, Instant.now());
    }
}
