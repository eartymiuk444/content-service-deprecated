package info.earty.content.domain.model.page;

import info.earty.content.domain.model.common.DomainEvent;

public interface WorkingPageDomainEvent extends DomainEvent {

    WorkingPageId workingPageId();

}
