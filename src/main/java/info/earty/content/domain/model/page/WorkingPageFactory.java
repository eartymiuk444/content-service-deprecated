package info.earty.content.domain.model.page;

import info.earty.content.domain.model.menu.SiteMenuId;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.Instant;

@Service
public class WorkingPageFactory {

    private static final int INITIAL_DRAFT_ID = 1;

    public WorkingPage create(SiteMenuId siteMenuId, WorkingPageId workingPageId, Title title) {
        Assert.notNull(siteMenuId, WorkingPageFactory.class.getSimpleName() + ": site menu id cannot be null");
        Assert.notNull(workingPageId, WorkingPageFactory.class.getSimpleName() + ": working page id cannot be null");

        WorkingPage workingPage = new WorkingPage(workingPageId, siteMenuId);

        Draft draft = Draft.create(new DraftId(INITIAL_DRAFT_ID), title);
        workingPage.setNextDraftIdInt(INITIAL_DRAFT_ID + 1);
        workingPage.setDraft(draft);
        workingPage.setPublishedPage(PublishedPage.create(draft));
        workingPage.setLastPublished(Instant.now());
        return workingPage;
    }

}
