package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftOutlineItemId {

    WorkingCardId workingCardId;

    public static DraftOutlineItemId createRootId() {
        return new DraftOutlineItemId(null);
    }

    public static DraftOutlineItemId create(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error creating draft outline item id; working card id cannot be null");
        return new DraftOutlineItemId(workingCardId);
    }

    public boolean isRootId() {
        return workingCardId == null;
    }

    public boolean isNonRootId() {
        return !this.isRootId();
    }

    public WorkingCardId workingCardId() {
        Assert.isTrue(this.isNonRootId(), "Error retrieving working card id; the draft outline item id is the root id");
        return this.workingCardId;
    }
}
