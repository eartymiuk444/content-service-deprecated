package info.earty.content.domain.model.menu;

import org.springframework.stereotype.Service;

@Service
public class SiteMenuIdentityService {
    private final static String EARTY_INFO_MENU_ID = "earty-info";

    public SiteMenuId eartyInfoId() {
        return new SiteMenuId(EARTY_INFO_MENU_ID);
    }
}
