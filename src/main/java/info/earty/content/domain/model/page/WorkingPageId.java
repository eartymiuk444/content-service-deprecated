package info.earty.content.domain.model.page;

import info.earty.content.domain.model.common.AggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class WorkingPageId implements AggregateId {
    String id;
}
