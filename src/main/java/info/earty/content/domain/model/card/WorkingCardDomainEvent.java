package info.earty.content.domain.model.card;

import info.earty.content.domain.model.common.DomainEvent;

public interface WorkingCardDomainEvent extends DomainEvent {

    WorkingCardId workingCardId();

}
