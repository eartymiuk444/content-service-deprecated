package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PageRemoved implements SiteMenuDomainEvent {

    SiteMenuId siteMenuId;
    WorkingPageId workingPageId;
    Instant occurredOn;

    static PageRemoved create(SiteMenuId siteMenuId, WorkingPageId workingPageId) {
        Assert.notNull(siteMenuId, PageRemoved.class.getSimpleName() + ": site menu id cannot be null");
        Assert.notNull(workingPageId, PageRemoved.class.getSimpleName() + ": working page id cannot be null");

        return new PageRemoved(siteMenuId, workingPageId, Instant.now());
    }

}
