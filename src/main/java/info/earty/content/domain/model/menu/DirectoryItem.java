package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class DirectoryItem {

    DirectoryId directoryId;
    WorkingPageId workingPageId;
    DirectoryItemName name;
    PathSegment pathSegment;

    public boolean isSubDirectory() {
        return this.directoryId != null;
    }

    public boolean isPage() {
        return this.workingPageId != null;
    }

    public DirectoryId directoryId() {
        Assert.isTrue(this.isSubDirectory(), "Error retrieving directory id; item is not a sub-directory");
        return this.directoryId;
    }

    public WorkingPageId workingPageId() {
        Assert.isTrue(this.isPage(), "Error retrieving working page id; item is not a page");
        return this.workingPageId;
    }

    public String toString() {
        if (this.isSubDirectory()) {
            return "SubDirectory(directoryId=" + this.directoryId() + ", name=" + this.name() + ", pathSegment=" + this.pathSegment() + ")";
        }
        else {
            return "Page(workingPageId=" + this.workingPageId() + ", name=" + this.name() + ", pathSegment=" + this.pathSegment() + ")";
        }
    }

    DirectoryItem changeName(DirectoryItemName name) {
        Assert.notNull(name, this.getClass().getSimpleName() + ": name cannot be null");
        return this.isPage() ?
                new DirectoryItem(null, this.workingPageId(), name, this.pathSegment()) :
                new DirectoryItem(this.directoryId(), null, name, this.pathSegment());
    }

    DirectoryItem changePathSegment(PathSegment pathSegment) {
        Assert.notNull(pathSegment, this.getClass().getSimpleName() + ": pathSegment cannot be null");
        return this.isPage() ?
                new DirectoryItem(null, this.workingPageId(), this.name(), pathSegment) :
                new DirectoryItem(this.directoryId(), null, this.name(), pathSegment);
    }

    static DirectoryItem create(Directory directory) {
        Assert.notNull(directory, "Error creating directory item; directory cannot be null");
        Assert.notNull(directory.pathSegment(), "Error creating directory item; path segment cannot be null");
        return new DirectoryItem(directory.id(), null, directory.name(), directory.pathSegment());
    }

    static DirectoryItem create(WorkingPageId workingPageId, DirectoryItemName name, PathSegment pathSegment) {
        Assert.notNull(workingPageId, "Error creating page; working page id cannot be null");
        Assert.notNull(name, "Error creating page; name cannot be null");
        Assert.notNull(pathSegment, "Error creating page; path segment cannot be null");
        return new DirectoryItem(null, workingPageId, name, pathSegment);
    }

}
