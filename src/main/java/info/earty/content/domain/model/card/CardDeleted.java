package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class CardDeleted implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;

    List<ImageId> publishedImages;
    List<ImageId> draftImages;

    List<AttachmentId> publishedAttachments;
    List<AttachmentId> draftAttachments;

    Instant occurredOn;

    public static CardDeleted create(WorkingCardId workingCardId, List<ImageId> publishedImages,
                              List<ImageId> draftImages, List<AttachmentId> publishedAttachments, List<AttachmentId> draftAttachments) {
        Assert.notNull(workingCardId, CardDeleted.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(publishedImages, CardDeleted.class.getSimpleName() + ": published images cannot be null");
        Assert.notNull(draftImages, CardDeleted.class.getSimpleName() + ": draft images cannot be null");
        Assert.notNull(publishedAttachments, CardDeleted.class.getSimpleName() + ": published attachments cannot be null");
        Assert.notNull(draftAttachments, CardDeleted.class.getSimpleName() + ": draft attachments cannot be null");
        return new CardDeleted(workingCardId,
                new ArrayList<>(publishedImages),
                new ArrayList<>(draftImages),
                new ArrayList<>(publishedAttachments),
                new ArrayList<>(draftAttachments),
                Instant.now());
    }

    public List<ImageId> publishedImages() {
        return new ArrayList<>(this.publishedImages);
    }

    public List<ImageId> draftImages() {
        return new ArrayList<>(this.draftImages);
    }

    public List<AttachmentId> publishedAttachments() {
        return new ArrayList<>(this.publishedAttachments);
    }

    public List<AttachmentId> draftAttachments() {
        return new ArrayList<>(this.draftAttachments);
    }

    public Set<ImageId> orphanedImages() {
        Set<ImageId> orphanedImages = new HashSet<>(publishedImages);
        orphanedImages.addAll(draftImages);
        return orphanedImages;
    }

    public Set<AttachmentId> orphanedAttachments() {
        Set<AttachmentId> orphanedAttachments = new HashSet<>(publishedAttachments);
        orphanedAttachments.addAll(draftAttachments);
        return orphanedAttachments;
    }

}
