package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.menu.SiteMenuId;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PagePublished implements WorkingPageDomainEvent {
    SiteMenuId siteMenuId;
    WorkingPageId workingPageId;

    Title publishedTitle;

    Set<WorkingCardId> priorPublishedOutlineItems;
    Set<WorkingCardId> currentPublishedOutlineItems;

    Instant occurredOn;

    public Set<WorkingCardId> publishedOutlineItemsRemoved() {
        return this.priorPublishedOutlineItems.stream().filter(priorPublishedCardId ->
                !currentPublishedOutlineItems.contains(priorPublishedCardId)).collect(Collectors.toSet());
    }

    static PagePublished create(SiteMenuId siteMenuId, WorkingPageId workingPageId, Title currentPublishedTitle, Set<WorkingCardId> priorPublishedCardIds,
                                Set<WorkingCardId> currentPublishedCardIds) {
        Assert.notNull(workingPageId, PagePublished.class.getSimpleName() + ": working page id cannot be null");
        Assert.notNull(currentPublishedTitle, PagePublished.class.getSimpleName() + ": current published title cannot be null");
        Assert.notNull(priorPublishedCardIds, PagePublished.class.getSimpleName() + ": prior published card ids cannot be null");
        Assert.notNull(currentPublishedCardIds, PagePublished.class.getSimpleName() + ": current published card ids cannot be null");

        return new PagePublished(siteMenuId, workingPageId, currentPublishedTitle,
                new HashSet<>(priorPublishedCardIds), new HashSet<>(currentPublishedCardIds), Instant.now());
    }

}
