package info.earty.content.domain.model.common;

import java.util.ArrayList;
import java.util.List;

public class DomainEventPublisher {
    private static final ThreadLocal<DomainEventPublisher> INSTANCE = ThreadLocal.withInitial(DomainEventPublisher::new);
    public static DomainEventPublisher instance() {
        return INSTANCE.get();
    }

    private boolean publishing;
    private final List<DomainEventSubscriber> subscribers;

    public void subscribe(DomainEventSubscriber aSubscriber) {
        if (!this.publishing) {
            this.subscribers.add(aSubscriber);
        }
    }

    public void publish(DomainEvent aDomainEvent) {
        if (!this.publishing && !this.subscribers.isEmpty()) {
            try {
                this.publishing = true;

                this.subscribers.forEach(subscriber -> {
                    if (subscriber.subscribedToEventType().isAssignableFrom(aDomainEvent.getClass())) {
                        subscriber.handleEvent(aDomainEvent);
                    }
                });
            } finally {
                this.publishing = false;
            }
        }
    }

    public void reset() {
        if (!this.publishing) {
            this.subscribers.clear();
        }
    }

    private DomainEventPublisher() {
        this.publishing = false;
        this.subscribers = new ArrayList<>();
    }
}
