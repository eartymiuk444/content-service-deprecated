package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.DomainEventPublisher;
import info.earty.content.domain.model.image.ImageId;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access=AccessLevel.PACKAGE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingCard implements Aggregate<WorkingCardId> {

    @EqualsAndHashCode.Include
    private final WorkingCardId id;

    private final WorkingPageId workingPageId;
    private PublishedCard publishedCard;
    private DraftCard draftCard;

    private int nextDraftCardIdInt;

    public WorkingCardId id() {
        return this.id;
    }

    public WorkingPageId workingPageId() {
        return this.workingPageId;
    }

    public PublishedCard publishedCard() {
        return this.publishedCard;
    }

    public DraftCard draftCard() {
        return this.draftCard;
    }

    public void publish() {
        List<ImageId> priorPublishedImages = this.publishedCard.images().stream().map(Image::imageId).collect(Collectors.toList());
        List<AttachmentId> priorPublishedAttachments = this.publishedCard.attachments().stream().map(Attachment::attachmentId).collect(Collectors.toList());
        this.setPublishedCard(PublishedCard.create(this.draftCard()));
        
        DomainEventPublisher.instance().publish(CardPublished.create(id,
                priorPublishedImages,
                this.publishedCard.images().stream().map(Image::imageId).collect(Collectors.toList()),
                priorPublishedAttachments,
                this.publishedCard.attachments().stream().map(Attachment::attachmentId).collect(Collectors.toList())));
    }

    public void discardDraft() {
        List<ImageId> priorDraftImages = this.draftCard().images().stream().map(Image::imageId).collect(Collectors.toList());
        List<AttachmentId> priorDraftAttachments = this.draftCard().attachments().stream().map(Attachment::attachmentId).collect(Collectors.toList());

        this.setDraftCard(DraftCard.create(this.nextDraftCardId(), this.publishedCard()));
        DomainEventPublisher.instance().publish(DraftDiscarded.create(id,
                priorDraftImages,
                this.draftCard().images().stream().map(Image::imageId).collect(Collectors.toList()),
                priorDraftAttachments,
                this.draftCard().attachments().stream().map(Attachment::attachmentId).collect(Collectors.toList())));
    }

    public void removeImage(ImageId imageId) {
        this.draftCard.removeImage(imageId);
        DomainEventPublisher.instance().publish(ImageRemoved.create(this.id(), imageId,
                this.publishedCard().images().stream().noneMatch(i -> i.imageId().equals(imageId))));
    }

    public void removeAttachment(AttachmentId attachmentId) {
        this.draftCard.removeAttachment(attachmentId);
        DomainEventPublisher.instance().publish(AttachmentRemoved.create(this.id(), attachmentId,
                this.publishedCard().attachments().stream().noneMatch(a -> a.attachmentId().equals(attachmentId))));
    }

    void setPublishedCard(PublishedCard publishedCard) {
        Assert.notNull(publishedCard, this.getClass().getSimpleName() + ": published card cannot be null");
        this.publishedCard = publishedCard;
    }

    void setDraftCard(DraftCard draftCard) {
        Assert.notNull(draftCard, this.getClass().getSimpleName() + ": draft card cannot be null");
        this.draftCard = draftCard;
    }

    void setNextDraftCardIdInt(int nextDraftCardIdInt) {
        this.nextDraftCardIdInt = nextDraftCardIdInt;
    }

    private DraftCardId nextDraftCardId() {
        DraftCardId draftCardId = new DraftCardId(this.nextDraftCardIdInt);
        this.setNextDraftCardIdInt(this.nextDraftCardIdInt + 1);
        return draftCardId;
    }
}
