package info.earty.content.domain.model.card;

import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AddImageFailed implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;
    ImageId imageId;
    Instant occurredOn;

    public static AddImageFailed create(WorkingCardId workingCardId, ImageId imageId) {
        Assert.notNull(workingCardId, AddImageFailed.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(imageId, AddImageFailed.class.getSimpleName() + ": image id cannot be null");

        return new AddImageFailed(workingCardId, imageId, Instant.now());
    }
}
