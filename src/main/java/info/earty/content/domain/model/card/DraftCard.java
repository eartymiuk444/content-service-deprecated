package info.earty.content.domain.model.card;

import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftCard {

    @EqualsAndHashCode.Include
    private final DraftCardId id;
    private String title;
    private String text;
    private final List<Image> images;
    private final List<Attachment> attachments;

    static DraftCard create(DraftCardId id, String title) {
        Assert.notNull(id, DraftCard.class.getSimpleName() + ": id cannot be null");
        DraftCard draftCard = new DraftCard(id, new ArrayList<>(), new ArrayList<>());
        draftCard.setTitle(title);
        draftCard.setText("");
        return draftCard;
    }

    static DraftCard create(DraftCardId id, PublishedCard publishedCard) {
        Assert.notNull(id, DraftCard.class.getSimpleName() + ": id cannot be null");
        Assert.notNull(publishedCard, DraftCard.class.getSimpleName() + ": published card cannot be null");
        DraftCard draftCard = new DraftCard(id, new ArrayList<>(publishedCard.images()), new ArrayList<>(publishedCard.attachments()));
        draftCard.setTitle(publishedCard.title());
        draftCard.setText(publishedCard.text());
        return draftCard;
    }

    public DraftCardId id() {
        return this.id;
    }

    public String title() {
        return this.title;
    }

    public String text() {
        return this.text;
    }

    public List<Image> images() {
        return new ArrayList<>(this.images);
    }

    public List<Attachment> attachments() {
        return new ArrayList<>(this.attachments);
    }

    public void changeTitle(String title) {
        this.setTitle(title);
    }

    public void changeText(String text) {
        this.setText(text);
    }

    public void addImage(Image image) {
        Assert.notNull(image, this.getClass().getSimpleName() + " : image cannot be null");
        Assert.isTrue(this.images.stream().noneMatch(i -> i.imageId().equals(image.imageId())),
                this.getClass().getSimpleName() + " : this draft card already contains an image with this id");
        this.images.add(image);
    }

    void removeImage(ImageId imageId) {
        Assert.notNull(imageId, this.getClass().getSimpleName() + " : image id cannot be null");
        Assert.isTrue(this.images().stream().anyMatch(i -> i.imageId().equals(imageId)),
                this.getClass().getSimpleName() + " : Error removing the image; no image found for image id");

        this.images.removeIf(i -> i.imageId().equals(imageId));
    }

    public void addAttachment(Attachment attachment) {
        Assert.notNull(attachment, this.getClass().getSimpleName() + " : attachment cannot be null");
        Assert.isTrue(this.attachments.stream().noneMatch(i -> i.attachmentId().equals(attachment.attachmentId())),
                this.getClass().getSimpleName() + " : this draft card already contains an attachment with this id");
        this.attachments.add(attachment);
    }

    void removeAttachment(AttachmentId attachmentId) {
        Assert.notNull(attachmentId, this.getClass().getSimpleName() + " : attachment id cannot be null");
        Assert.isTrue(this.attachments().stream().anyMatch(a -> a.attachmentId().equals(attachmentId)),
                this.getClass().getSimpleName() + " : Error removing the attachment; no attachment found for attachment id");

        this.attachments.removeIf(a -> a.attachmentId().equals(attachmentId));
    }

    private void setTitle(String title) {
        Assert.notNull(title, this.getClass().getSimpleName() + ": title cannot be null");
        this.title = title;
    }

    private void setText(String text) {
        Assert.notNull(text, this.getClass().getSimpleName() + ": text cannot be null");
        this.text = text;
    }
}
