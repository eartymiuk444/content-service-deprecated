package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftOutlineItem {

    @EqualsAndHashCode.Include
    private final DraftOutlineItemId id;
    private String title;
    private Fragment fragment;
    private final List<DraftOutlineSubItem> subItems;

    public DraftOutlineItemId id() {
        return this.id;
    }

    public String title() {
        return this.title;
    }

    public boolean hasFragment() {
        return this.fragment != null;
    }

    public Fragment fragment() {
        Assert.isTrue(this.hasFragment(), "Error retrieving fragment for outline item; this outline item doesn't have a fragment");
        return this.fragment;
    }

    public List<DraftOutlineSubItem> subItems() {
        return new ArrayList<>(this.subItems);
    }

    public void moveSubItemUp(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error moving sub-item up; working card id can't be null");

        Optional<DraftOutlineSubItem> oDraftOutlineSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oDraftOutlineSubItem.isPresent(), "Error moving sub-item up; sub-item not found");

        int subItemPosition = this.subItems.indexOf(oDraftOutlineSubItem.get());
        Assert.isTrue(subItemPosition != 0,
                "Error moving sub-item up in outline; sub-item is already at the highest position");

        Collections.swap(this.subItems, subItemPosition, subItemPosition - 1);
    }

    public void moveSubItemDown(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error moving sub-item up; working card id can't be null");

        Optional<DraftOutlineSubItem> oDraftOutlineSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oDraftOutlineSubItem.isPresent(), "Error moving sub-item down; sub-item not found");

        int subItemPosition = this.subItems.indexOf(oDraftOutlineSubItem.get());
        Assert.isTrue(subItemPosition != this.subItems.size() - 1,
                "Error moving sub-item down in outline; sub-item is already at the lowest position");

        Collections.swap(this.subItems, subItemPosition, subItemPosition + 1);
    }

    static DraftOutlineItem create(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error creating draft outline item; cannot create draft outline item from null working card id");
        DraftOutlineItemId draftOutlineItemId = DraftOutlineItemId.create(workingCardId);
        DraftOutlineItem draftOutlineItem = new DraftOutlineItem(draftOutlineItemId, new ArrayList<>());
        draftOutlineItem.setTitle(title);
        draftOutlineItem.setFragment(null);
        return draftOutlineItem;
    }

    static DraftOutlineItem create(PublishedOutlineItem publishedOutlineItem) {
        Assert.notNull(publishedOutlineItem,
                "Error creating draft outline item; cannot create draft outline item from null published outline item");

        List<DraftOutlineSubItem> subItems = new ArrayList<>();
        publishedOutlineItem.subItems().forEach(publishedOutlineSubItem ->
                subItems.add(DraftOutlineSubItem.create(publishedOutlineSubItem)));

        DraftOutlineItem draftOutlineItem = new DraftOutlineItem(publishedOutlineItem.draftOutlineItemId(), subItems);
        draftOutlineItem.setTitle(publishedOutlineItem.title());
        draftOutlineItem.setFragment(publishedOutlineItem.hasFragment() ? publishedOutlineItem.fragment() : null);

        return draftOutlineItem;
    }

    static DraftOutlineItem createRoot() {
        DraftOutlineItem draftOutlineItem = new DraftOutlineItem(DraftOutlineItemId.createRootId(), new ArrayList<>());
        draftOutlineItem.setTitle("");
        draftOutlineItem.setFragment(null);
        return draftOutlineItem;
    }

    void changeTitle(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error changing outline item's title; working card id cannot be null");
        this.setTitle(title);
    }

    void changeFragment(Fragment fragment) {
        Assert.isTrue(this.hasFragment(), "Error changing outline item fragment; no fragment to change");
        Assert.notNull(fragment, "Error changing outline item fragment; fragment cannot be null");
        this.setFragment(fragment);
    }

    void addFragment(Fragment fragment) {
        Assert.isTrue(!this.hasFragment(), "Error adding fragment to outline item; outline item already has a fragment");
        Assert.notNull(fragment, "Error adding outline item fragment; fragment cannot be null");
        this.setFragment(fragment);
    }

    void removeFragment() {
        Assert.isTrue(this.hasFragment(), "Error removing outline item fragment; no fragment to remove");
        this.setFragment(null);
    }

    void addSubItem(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, "Error adding outline sub-item to outline; working card id cannot be null");
        Assert.isTrue(this.findSubItem(workingCardId).isEmpty(),
                "Error adding outline sub-item to outline; the outline already contains a sub-item with the working card id");

        this.subItems.add(DraftOutlineSubItem.create(workingCardId, title));
    }

    void changeSubItemTitle(WorkingCardId workingCardId, String title) {
        Optional<DraftOutlineSubItem> oSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oSubItem.isPresent(), "Error changing sub-item title; sub-item not found");
        DraftOutlineSubItem subItem = oSubItem.get();

        int position = this.subItems.indexOf(subItem);
        this.subItems.set(position, subItem.changeTitle(title));
    }

    void addFragmentToSubItem(WorkingCardId workingCardId, Fragment fragment) {
        Optional<DraftOutlineSubItem> oSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oSubItem.isPresent(), "Error adding fragment to sub-item; sub-item not found");
        DraftOutlineSubItem subItem = oSubItem.get();

        Assert.isTrue(!(this.hasFragment() && this.fragment().equals(fragment)),
                this.getClass().getSimpleName() + ": adding this fragment to the sub-item results in duplicate fragments");
        Assert.isTrue(this.subItems().stream().filter(DraftOutlineSubItem::hasFragment)
                        .noneMatch(o -> o.fragment().equals(fragment)),
                this.getClass().getSimpleName() + ": adding this fragment to the sub-item results in duplicate fragments");

        int position = this.subItems.indexOf(subItem);
        this.subItems.set(position, subItem.addFragment(fragment));
    }

    void changeSubItemFragment(WorkingCardId workingCardId, Fragment fragment) {
        Optional<DraftOutlineSubItem> oSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oSubItem.isPresent(), "Error changing sub-item fragment; sub-item not found");
        DraftOutlineSubItem subItem = oSubItem.get();

        Assert.isTrue(!(this.hasFragment() && this.fragment().equals(fragment)),
                this.getClass().getSimpleName() + ": changing this sub-items fragment results in duplicate fragments");
        Assert.isTrue(this.subItems().stream().filter(DraftOutlineSubItem::hasFragment)
                        .noneMatch(o -> o.fragment().equals(fragment)),
                this.getClass().getSimpleName() + ": changing this sub-items fragment results in duplicate fragments");

        int position = this.subItems.indexOf(subItem);
        this.subItems.set(position, subItem.changeFragment(fragment));
    }

    void removeSubItemFragment(WorkingCardId workingCardId) {
        Optional<DraftOutlineSubItem> oSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oSubItem.isPresent(), "Error removing sub-item fragment; sub-item not found");
        DraftOutlineSubItem subItem = oSubItem.get();

        int position = this.subItems.indexOf(subItem);
        this.subItems.set(position, subItem.removeFragment());
    }

    void removeSubItem(WorkingCardId workingCardId) {
        Optional<DraftOutlineSubItem> oSubItem = this.findSubItem(workingCardId);
        Assert.isTrue(oSubItem.isPresent(), "Error removing sub-item; sub-item not found");
        DraftOutlineSubItem subItem = oSubItem.get();

        this.subItems.remove(subItem);
    }

    Optional<DraftOutlineSubItem> findSubItem(WorkingCardId workingCardId) {
        return this.subItems.stream().filter(s -> s.workingCardId().equals(workingCardId)).findAny();
    }

    void addSubItem(DraftOutlineSubItem subItem) {
        Assert.notNull(subItem, "Error adding outline sub-item to outline; sub-item cannot be null");
        Assert.isTrue(this.findSubItem(subItem.workingCardId()).isEmpty(),
                "Error adding outline sub-item to outline; the outline already contains a sub-item with the working card id");
        Assert.isTrue(!(this.hasFragment() && subItem.hasFragment() && this.fragment().equals(subItem.fragment())),
                this.getClass().getSimpleName() + ": adding this sub-item results in duplicate fragments");
        Assert.isTrue(!subItem.hasFragment() || this.subItems().stream().filter(DraftOutlineSubItem::hasFragment)
                        .noneMatch(o -> o.fragment().equals(subItem.fragment())),
                this.getClass().getSimpleName() + ": adding this sub-item results in duplicate fragments");

        this.subItems.add(subItem);
    }

    private void setTitle(String title) {
        Assert.notNull(title, this.getClass().getSimpleName() + ": title cannot be null");
        this.title = title;
    }

    private void setFragment(Fragment fragment) {
        Assert.isTrue(fragment == null || this.subItems().stream().filter(DraftOutlineSubItem::hasFragment)
                .noneMatch(o -> o.fragment().equals(fragment)), this.getClass().getSimpleName() +
                ": setting the fragment to this value results in duplicate fragments");
        this.fragment = fragment;
    }
}
