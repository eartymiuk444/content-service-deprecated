package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishedOutlineSubItem {
    WorkingCardId workingCardId;
    String title;
    Fragment fragment;

    public boolean hasFragment() {
        return this.fragment != null;
    }

    public Fragment fragment() {
        Assert.isTrue(this.hasFragment(), "Error retrieving fragment for sub-item; this sub-item doesn't have a fragment");
        return this.fragment;
    }

    public String toString() {
        return "PublishedOutlineSubItem(workingCardId=" + this.workingCardId + ", title=" + this.title +
                (this.hasFragment() ? ", fragment=" + this.fragment + ")" : ")");
    }

    static PublishedOutlineSubItem create(DraftOutlineSubItem draftOutlineSubItem) {
        Assert.notNull(draftOutlineSubItem, PublishedOutlineItem.class.getSimpleName() + ": draft outline sub-item cannot be null");

        return new PublishedOutlineSubItem(draftOutlineSubItem.workingCardId(), draftOutlineSubItem.title(),
                draftOutlineSubItem.hasFragment() ? draftOutlineSubItem.fragment() : null);
    }
}
