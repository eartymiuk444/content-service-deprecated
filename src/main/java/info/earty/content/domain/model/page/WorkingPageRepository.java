package info.earty.content.domain.model.page;

import java.util.Optional;

public interface WorkingPageRepository {

    void add(WorkingPage workingPage);
    Optional<WorkingPage> findById(WorkingPageId workingPageId);
    void remove(WorkingPage workingPage);
    boolean existsById(WorkingPageId workingPageId);
}
