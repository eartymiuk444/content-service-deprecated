package info.earty.content.domain.model.card;

import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Image {

    ImageId imageId;
    String caption;

    public static Image create(ImageId imageId) {
        Assert.notNull(imageId, Image.class.getSimpleName() + " : image id cannot be null");
        return new Image(imageId, "");
    }

}
