package info.earty.content.domain.model.common;

import java.util.regex.Pattern;

public class Rfc3986Patterns {

    //NOTE EA 11/24/2021 - Using 'pchar' rule defined here: https://datatracker.ietf.org/doc/html/rfc3986#section-3.3
        //Verified here https://regex101.com/
    private static final String UNRESERVED = "\\p{Alnum}\\-._~";
    private static final String PCT_ENCODED = "(?:\\%\\p{XDigit}\\p{XDigit})";
    private static final String SUB_DELIMS = "!$&'()*+,;=";
    private static final String PCHAR_ADDITIONAL = ":@";
    //NOTE EA 11/29/2021 - Extending using 'fragment' rule defined here: https://datatracker.ietf.org/doc/html/rfc3986#section-3.5
    private static final String FRAGMENT_ADDITIONAL = "/?";
    //NOTE EA 11/24/2021 - Resolves to "(?:(?:\%\p{XDigit}\p{XDigit})|[\p{Alnum}\-._~!$&'()*+,;=:@])+"
    public static final Pattern PATH_SEGMENT_PATTERN = Pattern.compile(String.format("(?:%s|[%s%s%s])+",
                    PCT_ENCODED, UNRESERVED, SUB_DELIMS, PCHAR_ADDITIONAL),
            Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern FRAGMENT_PATTERN = Pattern.compile(String.format("(?:%s|[%s%s%s%s])+",
                    PCT_ENCODED, UNRESERVED, SUB_DELIMS, PCHAR_ADDITIONAL, FRAGMENT_ADDITIONAL),
            Pattern.UNICODE_CHARACTER_CLASS);
}
