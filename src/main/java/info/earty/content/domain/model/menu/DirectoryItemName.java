package info.earty.content.domain.model.menu;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DirectoryItemName {

    String nameString;

    public static DirectoryItemName create(String nameString) {
        Assert.notNull(nameString, "name cannot be null");
        Assert.isTrue(nameString.length() > 0, "name must include at least one character");
        return new DirectoryItemName(nameString);
    }

}
