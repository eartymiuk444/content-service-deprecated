package info.earty.content.domain.model.common;

public interface DomainEventSubscriber<T extends DomainEvent> {

    void handleEvent(T aDomainEvent);
    Class<T> subscribedToEventType();

}
