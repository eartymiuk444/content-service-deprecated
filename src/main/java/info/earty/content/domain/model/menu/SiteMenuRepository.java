package info.earty.content.domain.model.menu;

import java.util.Optional;

public interface SiteMenuRepository {
    void add(SiteMenu siteMenu);
    Optional<SiteMenu> findById(SiteMenuId siteMenuId);
    void remove(SiteMenu siteMenu);
}
