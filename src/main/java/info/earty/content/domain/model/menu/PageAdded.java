package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PageAdded implements SiteMenuDomainEvent {

    SiteMenuId siteMenuId;
    WorkingPageId workingPageId;
    DirectoryItemName name;
    Instant occurredOn;

    static PageAdded create(SiteMenuId siteMenuId, WorkingPageId workingPageId, DirectoryItemName name) {
        Assert.notNull(siteMenuId, PageAdded.class.getSimpleName() + ": site menu id cannot be null");
        Assert.notNull(workingPageId, PageAdded.class.getSimpleName() + ": working page id cannot be null");
        Assert.notNull(name, PageAdded.class.getSimpleName() + ": name cannot be null");

        return new PageAdded(siteMenuId, workingPageId, name, Instant.now());
    }

}
