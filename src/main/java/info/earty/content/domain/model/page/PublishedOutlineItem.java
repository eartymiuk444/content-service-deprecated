package info.earty.content.domain.model.page;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishedOutlineItem {
    DraftOutlineItemId draftOutlineItemId;
    String title;
    Fragment fragment;
    List<PublishedOutlineSubItem> subItems;

    public boolean hasFragment() {
        return this.fragment != null;
    }

    public Fragment fragment() {
        Assert.isTrue(this.hasFragment(), "Error retrieving fragment for outline item; this outline item doesn't have a fragment");
        return this.fragment;
    }

    public List<PublishedOutlineSubItem> subItems() {
        return new ArrayList<>(this.subItems);
    }

    static PublishedOutlineItem create(DraftOutlineItem draftOutlineItem) {
        Assert.notNull(draftOutlineItem, PublishedOutlineItem.class.getSimpleName() + ": draft outline item cannot be null");

        List<PublishedOutlineSubItem> subItems = new ArrayList<>();
        draftOutlineItem.subItems().forEach(s -> subItems.add(PublishedOutlineSubItem.create(s)));

        return new PublishedOutlineItem(draftOutlineItem.id(), draftOutlineItem.title(),
                draftOutlineItem.hasFragment() ? draftOutlineItem.fragment() : null, subItems);
    }
}
