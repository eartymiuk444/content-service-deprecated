package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DirectoryRemoved implements SiteMenuDomainEvent {

    SiteMenuId siteMenuId;
    Set<WorkingPageId> pagesRemoved;
    Instant occurredOn;

    static DirectoryRemoved create(SiteMenuId siteMenuId, Set<WorkingPageId> pagesRemoved) {
        Assert.notNull(siteMenuId, DirectoryRemoved.class.getSimpleName() + ": site menu id cannot be null");
        Assert.notNull(pagesRemoved, DirectoryRemoved.class.getSimpleName() + ": pages removed cannot be null");

        return new DirectoryRemoved(siteMenuId, new HashSet<>(pagesRemoved), Instant.now());
    }

    public Set<WorkingPageId> pagesRemoved() {
        return new HashSet<>(this.pagesRemoved);
    }
}
