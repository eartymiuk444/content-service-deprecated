package info.earty.content.domain.model.menu;

import info.earty.content.domain.model.common.AggregateId;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
public class SiteMenuId implements AggregateId {
    String id;
}
