package info.earty.content.domain.model.card;

import info.earty.content.domain.model.page.WorkingPageId;

import java.util.Optional;
import java.util.Set;

public interface WorkingCardRepository {
    void add(WorkingCard workingCard);
    Optional<WorkingCard> findById(WorkingCardId workingCardId);
    void remove(WorkingCard workingCard);
    boolean existsById(WorkingCardId workingCardId);
    Set<WorkingCard> findByWorkingPageId(WorkingPageId workingPageId);
}
