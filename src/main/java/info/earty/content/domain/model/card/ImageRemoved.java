package info.earty.content.domain.model.card;

import info.earty.content.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Instant;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ImageRemoved implements WorkingCardDomainEvent {

    WorkingCardId workingCardId;
    ImageId imageRemoved;
    boolean imageOrphaned;
    Instant occurredOn;

    static ImageRemoved create(WorkingCardId workingCardId, ImageId imageRemoved, boolean imageOrphaned) {
        Assert.notNull(workingCardId, ImageRemoved.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(imageRemoved, ImageRemoved.class.getSimpleName() + ": image removed cannot be null");
        return new ImageRemoved(workingCardId, imageRemoved, imageOrphaned, Instant.now());
    }
}
