package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishedPage {

    Title title;

    Map<DraftOutlineItemId, PublishedOutlineItem> allOutlineItems;

    public PublishedOutlineItem rootOutlineItem() {
        return this.findOutlineItem(DraftOutlineItemId.createRootId()).get();
    }

    public Optional<PublishedOutlineItem> findOutlineItem(WorkingCardId workingCardId) {
        return this.findOutlineItem(DraftOutlineItemId.create(workingCardId));
    }

    public Set<WorkingCardId> allWorkingCardIds() {
        return this.allOutlineItems.keySet().stream().filter(DraftOutlineItemId::isNonRootId)
                .map(DraftOutlineItemId::workingCardId).collect(Collectors.toSet());
    }

    static PublishedPage create(Draft draft) {
        Assert.notNull(draft, "Error creating published page; draft cannot be null");

        Map<DraftOutlineItemId, PublishedOutlineItem> allOutlineItems = publishedItemsFromDraftRoot(
                draft.rootOutlineItem(), draft);

        return new PublishedPage(draft.title(), allOutlineItems);
    }

    private Optional<PublishedOutlineItem> findOutlineItem(DraftOutlineItemId draftOutlineItemId) {
        return this.allOutlineItems.containsKey(draftOutlineItemId) ?
                Optional.of(this.allOutlineItems.get(draftOutlineItemId)) : Optional.empty();
    }

    private static Map<DraftOutlineItemId, PublishedOutlineItem> publishedItemsFromDraftRoot(DraftOutlineItem draftOutlineItem, Draft draft)  {
        Map<DraftOutlineItemId, PublishedOutlineItem> publishedOutlineItems = new HashMap<>();
        PublishedOutlineItem publishedOutlineItem = PublishedOutlineItem.create(draftOutlineItem);
        publishedOutlineItems.put(draftOutlineItem.id(), publishedOutlineItem);

        draftOutlineItem.subItems().forEach(
            draftOutlineSubItem -> publishedOutlineItems.putAll(
                    publishedItemsFromDraftRoot(draft.findOutlineItem(draftOutlineSubItem.workingCardId()).get(), draft)));
        return publishedOutlineItems;
    }
}
