package info.earty.content.domain.model.card;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishedCard {
    String title;
    String text;
    List<Image> images;
    List<Attachment> attachments;

    public List<Image> images() {
        return new ArrayList<>(this.images);
    }
    public List<Attachment> attachments() {
        return new ArrayList<>(this.attachments);
    }

    static PublishedCard create(DraftCard draftCard) {
        Assert.notNull(draftCard, "Error creating published card; draft card cannot be null");
        return new PublishedCard(draftCard.title(), draftCard.text(), new ArrayList<>(draftCard.images()), new ArrayList<>(draftCard.attachments()));
    }
}
