package info.earty.content.domain.model.common;

public interface Aggregate<ID extends AggregateId> {
    ID id();
}
