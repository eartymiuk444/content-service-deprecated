package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DraftOutlineSubItem {
    WorkingCardId workingCardId;
    String title;
    Fragment fragment;

    public boolean hasFragment() {
        return this.fragment != null;
    }

    public Fragment fragment() {
        Assert.isTrue(this.hasFragment(), "Error retrieving fragment for sub-item; this sub-item doesn't have a fragment");
        return this.fragment;
    }

    public String toString() {
        return "DraftOutlineSubItem(workingCardId=" + this.workingCardId + ", title=" + this.title +
                (this.hasFragment() ? ", fragment=" + this.fragment + ")" : ")");
    }

    static DraftOutlineSubItem create(WorkingCardId workingCardId, String title) {
        Assert.notNull(workingCardId, DraftOutlineSubItem.class.getSimpleName() + ": working card id cannot be null");
        Assert.notNull(title, DraftOutlineSubItem.class.getSimpleName() + ": title cannot be null");
        return new DraftOutlineSubItem(workingCardId, title, null);
    }

    static DraftOutlineSubItem create(PublishedOutlineSubItem publishedOutlineSubItem) {
        Assert.notNull(publishedOutlineSubItem, "Error creating draft outline sub-item; published outline sub-item cannot be null");
        return new DraftOutlineSubItem(publishedOutlineSubItem.workingCardId(), publishedOutlineSubItem.title(),
                publishedOutlineSubItem.hasFragment() ? publishedOutlineSubItem.fragment() : null);
    }

    DraftOutlineSubItem changeTitle(String title) {
        Assert.notNull(title, "Error changing sub-item title; title cannot be null");
        return new DraftOutlineSubItem(this.workingCardId, title, this.fragment);
    }

    DraftOutlineSubItem addFragment(Fragment fragment) {
        Assert.isTrue(!this.hasFragment(), "Error adding sub-item fragment; sub-item already has a fragment");
        Assert.notNull(fragment, "Error changing sub-item fragment; fragment cannot be null");
        return new DraftOutlineSubItem(this.workingCardId, this.title, fragment);
    }

    DraftOutlineSubItem changeFragment(Fragment fragment) {
        Assert.isTrue(this.hasFragment(), "Error changing sub-item fragment; no fragment to change");
        Assert.notNull(fragment, "Error changing sub-item fragment; fragment cannot be null");
        return new DraftOutlineSubItem(this.workingCardId, this.title, fragment);
    }

    DraftOutlineSubItem removeFragment() {
        Assert.isTrue(this.hasFragment(), "Error removing sub-item fragment; no fragment to remove");
        return new DraftOutlineSubItem(this.workingCardId, this.title, null);
    }
}
