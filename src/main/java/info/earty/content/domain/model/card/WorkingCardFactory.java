package info.earty.content.domain.model.card;

import info.earty.content.domain.model.page.WorkingPageId;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class WorkingCardFactory {

    private static final int INITIAL_DRAFT_CARD_ID = 1;

    public WorkingCard create(WorkingCardId id, WorkingPageId workingPageId, String title) {
        Assert.notNull(id, "Error creating working card; id cannot be null");
        Assert.notNull(workingPageId, "Error creating working card; working page id cannot be null");

        WorkingCard workingCard = new WorkingCard(id, workingPageId);
        DraftCard draftCard = DraftCard.create(new DraftCardId(INITIAL_DRAFT_CARD_ID), title);
        workingCard.setDraftCard(draftCard);
        workingCard.setPublishedCard(PublishedCard.create(draftCard));
        workingCard.setNextDraftCardIdInt(INITIAL_DRAFT_CARD_ID);
        return workingCard;
    }

}
