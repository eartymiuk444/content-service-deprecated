package info.earty.content.domain.model.page;

import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.DomainEventPublisher;
import info.earty.content.domain.model.menu.SiteMenuId;
import lombok.*;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.Set;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingPage implements Aggregate<WorkingPageId> {

    @EqualsAndHashCode.Include
    private final WorkingPageId id;

    private final SiteMenuId siteMenuId;

    private PublishedPage publishedPage;
    private Draft draft;

    private Instant lastPublished;

    private int nextDraftIdInt;

    public SiteMenuId siteMenuId() {
        return this.siteMenuId;
    }

    public WorkingPageId id() {
        return this.id;
    }

    public Draft draft() {
        return this.draft;
    }

    public PublishedPage publishedPage() {
        return this.publishedPage;
    }

    public Instant lastPublished() {
        return this.lastPublished;
    }

    public void addOutlineItemToRoot(WorkingCardId workingCardId, String title) {
        this.draft().addOutlineItemToRoot(workingCardId, title);
        DomainEventPublisher.instance().publish(OutlineItemAdded.create(this.id(), workingCardId, title));
    }

    public void removeOutlineItem(WorkingCardId workingCardId) {
        DomainEventPublisher.instance().publish(OutlineItemRemoved.create(this.id, this.publishedPage().allWorkingCardIds(),
                this.draft().removeOutlineItem(workingCardId)));
    }

    public void publish() {
        Set<WorkingCardId> priorPublishedCardIds = this.publishedPage().allWorkingCardIds();

        this.setPublishedPage(PublishedPage.create(draft));
        this.setLastPublished(Instant.now());

        DomainEventPublisher.instance().publish(PagePublished.create(this.siteMenuId(), this.id(), this.publishedPage().title(),
                priorPublishedCardIds, this.publishedPage().allWorkingCardIds()));
    }

    public void discardDraft() {
        Set<WorkingCardId> priorDraftCardIds = this.draft().allWorkingCardIds();

        this.setDraft(Draft.create(this.nextDraftId(), this.publishedPage));

        DomainEventPublisher.instance().publish(DraftDiscarded.create(this.id(), priorDraftCardIds, this.draft().allWorkingCardIds()));
    }

    //NOTE EA 8/29/2021 - More likely future point of change.
    public void changeOutlineItemTitle(WorkingCardId workingCardId, String title) {
        this.draft().changeOutlineItemTitle(workingCardId, title);
        DomainEventPublisher.instance().publish(DraftOutlineItemTitleChanged.create(this.id(), workingCardId, title));
    }

    private DraftId nextDraftId() {
        DraftId nextDraftId = new DraftId(this.nextDraftIdInt);
        this.setNextDraftIdInt(this.nextDraftIdInt + 1);
        return nextDraftId;
    }

    void setPublishedPage(PublishedPage publishedPage) {
        Assert.notNull(publishedPage, this.getClass().getSimpleName() + ": published page cannot be null");
        this.publishedPage = publishedPage;
    }

    void setDraft(Draft draft) {
        Assert.notNull(draft, this.getClass().getSimpleName() + ": draft cannot be null");
        this.draft = draft;
    }

    void setLastPublished(Instant lastPublished) {
        Assert.notNull(lastPublished, this.getClass().getSimpleName() + ": last published cannot be null");
        this.lastPublished = lastPublished;
    }

    void setNextDraftIdInt(int nextDraftIdInt) {
        this.nextDraftIdInt = nextDraftIdInt;
    }
}
