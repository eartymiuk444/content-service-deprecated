package info.earty.content;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import info.earty.content.presentation.*;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiCustomizer;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.apache.cxf.jaxrs.swagger.ui.SwaggerUiConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

//NOTE EA 9/8/2021 - Spring boot tries to autoconfigure the mapping and content for '/error.' With
//	cxf mapping to '/' it instead tries to handle the request and returns a 404. Disabling the
//	autoconfiguration so instead the 500 error is returned to the client (assuming no other error handling is done).
@SpringBootApplication(exclude = { ErrorMvcAutoConfiguration.class })
@RequiredArgsConstructor
public class ContentApplication {

	private final Bus bus;
	private final SiteMenuCommandApiService siteMenuCommandApiService;
	private final SiteMenuQueryApiService siteMenuQueryApiService;
	private final WorkingPageCommandApiService workingPageCommandApiService;
	private final WorkingPageQueryApiService workingPageQueryApiService;
	private final WorkingCardCommandApiService workingCardCommandApiService;
	private final WorkingCardQueryApiService workingCardQueryApiService;

	public static void main(String[] args) {
		SpringApplication.run(ContentApplication.class, args);
	}

	@Bean
	public Server rsServer() {
		JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
		endpoint.setBus(bus);
		endpoint.setAddress("/");
		endpoint.setServiceBeans(Arrays.<Object>asList(siteMenuCommandApiService, siteMenuQueryApiService,
				workingPageCommandApiService, workingPageQueryApiService, workingCardCommandApiService,
				workingCardQueryApiService));
		endpoint.setProviders(Arrays.<Object>asList(jacksonJsonProvider(), illegalArgumentExceptionMapper()));
		endpoint.setFeatures(Arrays.asList(createOpenApiFeature()));
		return endpoint.create();
	}

	@Bean
	public JacksonJsonProvider jacksonJsonProvider() {
		JacksonJsonProvider jacksonJsonProvider = new JacksonJsonProvider(objectMapper());
		return jacksonJsonProvider;
	}

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		return objectMapper;
	}

	@Bean
	public IllegalArgumentExceptionMapper illegalArgumentExceptionMapper() {
		return new IllegalArgumentExceptionMapper();
	}

	@Bean
	public OpenApiFeature createOpenApiFeature() {
		final OpenApiFeature openApiFeature = new OpenApiFeature();
		openApiFeature.setPrettyPrint(true);
		openApiFeature.setTitle("earty.info content api");
		openApiFeature.setContactName("Erik Artymiuk");
		openApiFeature.setDescription("api for managing content on the earty.info site");
		openApiFeature.setVersion("1.0.0");
		//TODO EA 6/29/2021 - Determine why this doesn't need to be specified and how resolution of the endpoints is working.
		//openApiFeature.setResourcePackages(new HashSet<>(Arrays.asList("info.earty.content.presentation")));
		openApiFeature.setSwaggerUiConfig(new SwaggerUiConfig().url("openapi.json"));

		OpenApiCustomizer openApiCustomizer = new OpenApiCustomizer();
		openApiCustomizer.setDynamicBasePath(true);
		openApiFeature.setCustomizer(openApiCustomizer);

		return openApiFeature;
	}
}
