package info.earty.content.application;

import info.earty.content.application.command.card.*;
import info.earty.content.application.common.ApplicationServiceCommand;
import info.earty.content.application.common.ApplicationServiceLifeCycle;
import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardInboxRepository;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.card.WorkingCardOutboxRepository;
import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.card.*;
import info.earty.content.domain.model.common.DomainEventPublisher;
import info.earty.content.domain.model.image.ImageId;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WorkingCardCommandService {

    private final WorkingCardFactory workingCardFactory;

    private final WorkingCardRepository workingCardRepository;
    private final WorkingCardOutboxRepository workingCardOutboxRepository;
    private final WorkingCardInboxRepository workingCardInboxRepository;

    private final ApplicationServiceLifeCycle applicationServiceLifeCycle;

    @ApplicationServiceCommand(aggregateType = WorkingCard.class)
    public void changeTitle(ChangeTitleCommand aCommand) { //event command
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingCard workingCard = workingCardRepository.findById(new WorkingCardId(aCommand.getWorkingCardId()))
                .orElseThrow(() -> new IllegalArgumentException("Error changing card title; no card found with id"));

        WorkingCardInbox workingCardInbox = workingCardInboxRepository.findByWorkingCardId(workingCardId).get();
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        if (!workingCardInbox.duplicateEvent(workingPageId, aCommand.getEventId())) { //deduplication
            if (!workingCard.draftCard().title().equals(aCommand.getTitle())) {
                workingCard.draftCard().changeTitle(aCommand.getTitle());
            }
            workingCardInbox.eventProcessed(workingPageId, aCommand.getEventId());
        }
    }

    @ApplicationServiceCommand(aggregateType = WorkingCard.class)
    public void changeText(ChangeTextCommand aCommand) {
        WorkingCard workingCard = workingCardRepository.findById(new WorkingCardId(aCommand.getWorkingCardId()))
                .orElseThrow(() -> new IllegalArgumentException("Error changing card text; no card found with id"));
        workingCard.draftCard().changeText(aCommand.getText());
    }

    @ApplicationServiceCommand(aggregateType = WorkingCard.class) //event command
    public void create(CreateCommand aCommand) {
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        if (!workingCardRepository.existsById(workingCardId)) { //deduplication
            WorkingCard workingCard = workingCardFactory.create(new WorkingCardId(aCommand.getWorkingCardId()),
                    new WorkingPageId(aCommand.getWorkingPageId()), aCommand.getTitle());
            workingCardRepository.add(workingCard);
            workingCardOutboxRepository.add(WorkingCardOutbox.create(workingCardId));
            workingCardInboxRepository.add(WorkingCardInbox.create(workingCardId));
        }
    }

    public void publish(PublishCommand aCommand) { //event command
        aCommand.getPublishWorkingCardIds().stream().map(WorkingCardId::new).forEach(workingCardId -> {
            this.publish(workingCardId, new WorkingPageId(aCommand.getWorkingPageId()), aCommand.getEventId());
        });

        aCommand.getRemoveWorkingCardIds().stream().map(WorkingCardId::new).forEach(this::remove);
    }

    //TODO EA 8/25/2021 - Add AspectJ
    //@ApplicationServiceCommand(listen = true, aggregateType = WorkingCard.class)
    private void publish(WorkingCardId workingCardId, WorkingPageId workingPageId, int eventId) {
        applicationServiceLifeCycle.begin(WorkingCard.class, true);
        try {
            WorkingCardInbox inbox = workingCardInboxRepository.findByWorkingCardId(workingCardId)
                    .orElseThrow(() -> new IllegalArgumentException("Error publishing working card; no working card inbox with id found"));

            if (!inbox.duplicateEvent(workingPageId, eventId)) {
                WorkingCard workingCard = workingCardRepository.findById(workingCardId).get();
                workingCard.publish();
                inbox.eventProcessed(workingPageId, eventId);
            }
        } catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
        applicationServiceLifeCycle.success();
    }

    public void discard(DiscardCommand aCommand) { //event command

        aCommand.getDiscardWorkingCardIds().stream().map(WorkingCardId::new).forEach(workingCardId -> {
            this.discard(workingCardId, new WorkingPageId(aCommand.getWorkingPageId()), aCommand.getEventId());
        });

        aCommand.getRemoveWorkingCardIds().stream().map(WorkingCardId::new).forEach(this::remove);
    }

    //TODO EA 8/25/2021 - Add AspectJ
    //@ApplicationServiceCommand(aggregateType = WorkingCard.class)
    private void discard(WorkingCardId workingCardId, WorkingPageId workingPageId, int eventId) {
        applicationServiceLifeCycle.begin(WorkingCard.class, true);
        try {
            WorkingCardInbox inbox = workingCardInboxRepository.findByWorkingCardId(workingCardId)
                    .orElseThrow(() -> new IllegalArgumentException("Error discarding working card; no working card inbox with id found"));

            if (!inbox.duplicateEvent(workingPageId, eventId)) {
                WorkingCard workingCard = workingCardRepository.findById(workingCardId).get();
                workingCard.discardDraft();
                inbox.eventProcessed(workingPageId, eventId);
            }
        } catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
        applicationServiceLifeCycle.success();
    }

    //TODO EA 8/25/2021 - Add AspectJ
    //@ApplicationServiceCommand(aggregateType = WorkingCard.class)
    private void remove(WorkingCardId workingCardId) {
        applicationServiceLifeCycle.begin(WorkingCard.class, true);
        try {
            Optional<WorkingCard> oWorkingCard = workingCardRepository.findById(workingCardId);
            oWorkingCard.ifPresent(workingCard -> { //deduplication

                List<ImageId> publishedImages = workingCard.publishedCard().images().stream().map(Image::imageId).collect(Collectors.toList());
                List<ImageId> draftImages = workingCard.draftCard().images().stream().map(Image::imageId).collect(Collectors.toList());

                List<AttachmentId> publishedAttachments = workingCard.publishedCard().attachments().stream().map(Attachment::attachmentId).collect(Collectors.toList());
                List<AttachmentId> draftAttachments = workingCard.draftCard().attachments().stream().map(Attachment::attachmentId).collect(Collectors.toList());

                workingCardRepository.remove(workingCard);
                //NOTE EA 2022-04-06 - Keep the outbox around in case we have 'AddImageFailed' events to process
                //workingCardOutboxRepository.remove(workingCardOutboxRepository.findByWorkingCardId(workingCardId).get());
                workingCardInboxRepository.remove(workingCardInboxRepository.findByWorkingCardId(workingCardId).get());

                DomainEventPublisher.instance().publish(CardDeleted.create(workingCardId, publishedImages, draftImages, publishedAttachments, draftAttachments));
            });
        } catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
        applicationServiceLifeCycle.success();
    }

    public void remove(RemoveCommand aCommand) { //event command
        aCommand.getWorkingCardIds().stream().map(WorkingCardId::new).forEach(this::remove);
    }

    public void removeCardsOnPage(RemoveOnPageCommand aCommand) {
        Set<WorkingCard> workingCards = workingCardRepository.findByWorkingPageId(new WorkingPageId(aCommand.getWorkingPageId()));
        workingCards.stream().map(WorkingCard::id).forEach(this::remove);
    }

    public void removeCardsOnPages(RemoveOnPagesCommand aCommand) {
        Set<WorkingCard> workingCards = new HashSet<>();
        aCommand.getWorkingPageIds().stream().map(WorkingPageId::new).forEach(workingPageId -> {
            workingCards.addAll(workingCardRepository.findByWorkingPageId(workingPageId));
        });
        workingCards.stream().map(WorkingCard::id).forEach(this::remove);
    }

    @ApplicationServiceCommand(aggregateType = WorkingCard.class, listen = true) //event command
    public void addImage(AddImageCommand aCommand) {
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        Optional<WorkingCard> optionalWorkingCard = workingCardRepository.findById(workingCardId);
        if (optionalWorkingCard.isPresent()) {
            optionalWorkingCard.get().draftCard().addImage(Image.create(ImageId.create(aCommand.getImageId())));
        }
        else {
            DomainEventPublisher.instance().publish(AddImageFailed.create(workingCardId, ImageId.create(aCommand.getImageId())));
        }
    }

    @ApplicationServiceCommand(aggregateType = WorkingCard.class, listen = true)
    public void removeImage(RemoveImageCommand aCommand) {
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingCard workingCard = workingCardRepository.findById(workingCardId)
                .orElseThrow(() -> new IllegalArgumentException(this.getClass().getSimpleName() + " : no card found with id"));

        workingCard.removeImage(ImageId.create(aCommand.getImageId()));
    }
    
    @ApplicationServiceCommand(aggregateType = WorkingCard.class, listen = true) //event command
    public void addAttachment(AddAttachmentCommand aCommand) {
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        Optional<WorkingCard> optionalWorkingCard = workingCardRepository.findById(workingCardId);
        if (optionalWorkingCard.isPresent()) {
            optionalWorkingCard.get().draftCard().addAttachment(Attachment.create(AttachmentId.create(aCommand.getAttachmentId()), aCommand.getAttachmentFilename()));
        }
        else {
            DomainEventPublisher.instance().publish(AddAttachmentFailed.create(workingCardId, AttachmentId.create(aCommand.getAttachmentId())));
        }
    }

    @ApplicationServiceCommand(aggregateType = WorkingCard.class, listen = true)
    public void removeAttachment(RemoveAttachmentCommand aCommand) {
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingCard workingCard = workingCardRepository.findById(workingCardId)
                .orElseThrow(() -> new IllegalArgumentException(this.getClass().getSimpleName() + " : no card found with id"));

        workingCard.removeAttachment(AttachmentId.create(aCommand.getAttachmentId()));
    }
}
