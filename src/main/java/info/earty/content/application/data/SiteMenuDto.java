package info.earty.content.application.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SiteMenuDto {

    private String siteMenuId;
    private Integer rootDirectoryId;
    private Map<Integer, DirectoryDto> allDirectories;

    @Data
    public static class DirectoryDto {
        private Integer directoryId;
        private String name;
        private String pathSegment;
        private List<DirectoryItemDto> items;
    }

    public interface DirectoryItemDto {
        String getName();
        String getPathSegment();
    }

    @Data
    public static class SubDirectoryDto implements DirectoryItemDto {
        private Integer directoryId;
        private String name;
        private String pathSegment;
    }

    @Data
    public static class PageDto implements DirectoryItemDto {
        private String workingPageId;
        private String name;
        private String pathSegment;
    }
}
