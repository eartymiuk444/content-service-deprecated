package info.earty.content.application.data;

import lombok.Data;

import java.util.List;

@Data
public class PublishedCardDto {
    private String workingCardId;
    private String title;
    private String text;
    private List<Image> images;
    private List<Attachment> attachments;

    @Data
    public static class Image {
        private String imageId;
    }

    @Data
    public static class Attachment {
        private String attachmentId;
        private String filename;
    }
}
