package info.earty.content.application.common;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class ApplicationServiceCommandAspect {

    private final ApplicationServiceLifeCycle applicationServiceLifeCycle;

    @Around("@annotation(applicationServiceCommand))")
    public Object applicationServiceCommandAroundAdvice(ProceedingJoinPoint joinPoint,
                                                        ApplicationServiceCommand applicationServiceCommand) throws Throwable {
        return this.applicationServiceCommandAdvice(joinPoint, applicationServiceCommand);
    }

    @Around("@within(applicationServiceCommand) && !@annotation(info.earty.content.application.common.ApplicationServiceCommand)")
    public Object applicationServiceCommandWithinAdvice(ProceedingJoinPoint joinPoint,
                                                        ApplicationServiceCommand applicationServiceCommand) throws Throwable {
        return this.applicationServiceCommandAdvice(joinPoint, applicationServiceCommand);
    }

    private Object applicationServiceCommandAdvice(ProceedingJoinPoint joinPoint,
                                                   ApplicationServiceCommand applicationServiceCommand) throws Throwable {
        applicationServiceLifeCycle.begin(applicationServiceCommand.aggregateType(), applicationServiceCommand.listen());
        try {
            Object result = joinPoint.proceed();
            applicationServiceLifeCycle.success();
            return result;
        } catch (Throwable e) {
            applicationServiceLifeCycle.fail(e);
            throw e;
        }
    }

}
