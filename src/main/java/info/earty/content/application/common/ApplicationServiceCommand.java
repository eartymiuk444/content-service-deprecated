package info.earty.content.application.common;

import info.earty.content.domain.model.common.Aggregate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationServiceCommand {
    boolean listen() default false;
    Class<? extends Aggregate<?>> aggregateType();
}
