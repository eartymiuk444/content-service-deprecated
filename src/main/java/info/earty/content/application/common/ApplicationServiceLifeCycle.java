package info.earty.content.application.common;

import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.card.WorkingCardOutboxRepository;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.application.event.menu.SiteMenuOutboxRepository;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.application.event.page.WorkingPageOutboxRepository;
import info.earty.content.domain.model.card.WorkingCardDomainEvent;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;
import info.earty.content.domain.model.common.DomainEventPublisher;
import info.earty.content.domain.model.common.DomainEventSubscriber;
import info.earty.content.domain.model.menu.SiteMenuDomainEvent;
import info.earty.content.domain.model.page.WorkingPageDomainEvent;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class ApplicationServiceLifeCycle {

    private final SiteMenuOutboxRepository siteMenuOutboxRepository;
    private final WorkingPageOutboxRepository workingPageOutboxRepository;
    private final WorkingCardOutboxRepository workingCardOutboxRepository;

    abstract public <A extends Aggregate> void begin(Class<A> aggregateType);
    abstract public void success();
    abstract public void fail();
    abstract public void fail(RuntimeException anException);
    abstract public void fail(Throwable aThrowable) throws Throwable;

    public <A extends Aggregate<? extends AggregateId>> void begin(Class<A> aggregateType, boolean isListening) {
        DomainEventPublisher.instance().reset();

        if (isListening) {
            this.listen();
        }
        this.begin(aggregateType);
    }

    public void listen() {
        //DomainEventPublisher.instance().reset(); //NOTE EA 10/25/2021 - Original location of the reset. Moved to begin
            //  as it seems more appropriate to always reset.
        DomainEventPublisher.instance().subscribe(siteMenuDomainEventSubscriber());
        DomainEventPublisher.instance().subscribe(workingPageDomainEventSubscriber());
        DomainEventPublisher.instance().subscribe(workingCardDomainEventSubscriber());
    }

    private DomainEventSubscriber<SiteMenuDomainEvent> siteMenuDomainEventSubscriber() {
        return new DomainEventSubscriber<>() {
            @Override
            public void handleEvent(SiteMenuDomainEvent aDomainEvent) {
                SiteMenuOutbox siteMenuOutbox =
                        siteMenuOutboxRepository.findBySiteMenuId(aDomainEvent.siteMenuId()).get();
                siteMenuOutbox.enqueue(aDomainEvent);
            }

            @Override
            public Class<SiteMenuDomainEvent> subscribedToEventType() {
                return SiteMenuDomainEvent.class;
            }
        };
    }

    private DomainEventSubscriber<WorkingPageDomainEvent> workingPageDomainEventSubscriber() {
        return new DomainEventSubscriber<>() {
            @Override
            public void handleEvent(WorkingPageDomainEvent aDomainEvent) {
                WorkingPageOutbox workingPageOutbox =
                        workingPageOutboxRepository.findByWorkingPageId(aDomainEvent.workingPageId()).get();
                workingPageOutbox.enqueue(aDomainEvent);
            }

            @Override
            public Class<WorkingPageDomainEvent> subscribedToEventType() {
                return WorkingPageDomainEvent.class;
            }
        };
    }

    private DomainEventSubscriber<WorkingCardDomainEvent> workingCardDomainEventSubscriber() {
        return new DomainEventSubscriber<>() {
            @Override
            public void handleEvent(WorkingCardDomainEvent aDomainEvent) {
                WorkingCardOutbox workingCardOutbox =
                        workingCardOutboxRepository.findByWorkingCardId(aDomainEvent.workingCardId()).get();
                workingCardOutbox.enqueue(aDomainEvent);
            }

            @Override
            public Class<WorkingCardDomainEvent> subscribedToEventType() {
                return WorkingCardDomainEvent.class;
            }
        };
    }
}
