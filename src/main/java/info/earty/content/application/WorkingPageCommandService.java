package info.earty.content.application;

import info.earty.content.application.command.page.*;
import info.earty.content.application.common.ApplicationServiceCommand;
import info.earty.content.application.common.ApplicationServiceLifeCycle;
import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageInboxRepository;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.application.event.page.WorkingPageOutboxRepository;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.card.WorkingCardIdentityService;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.page.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WorkingPageCommandService {

    private final WorkingPageFactory workingPageFactory;

    private final WorkingPageRepository workingPageRepository;
    private final WorkingCardIdentityService workingCardIdentityService;

    private final WorkingPageInboxRepository workingPageInboxRepository;

    private final WorkingPageOutboxRepository workingPageOutboxRepository;

    private final ApplicationServiceLifeCycle applicationServiceLifeCycle;

    private static final String NEW_CARD_TITLE = "Title";

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void createPage(CreateCommand aCommand) { //event command
        SiteMenuId siteMenuId = new SiteMenuId(aCommand.getSiteMenuId());
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        if (!workingPageRepository.existsById(workingPageId)) { //deduplication
            WorkingPage workingPage = workingPageFactory.create(siteMenuId, workingPageId, Title.create(aCommand.getTitle()));
            workingPageRepository.add(workingPage);

            WorkingPageInbox workingPageInbox = WorkingPageInbox.create(workingPageId);
            workingPageInbox.eventProcessed(new SiteMenuId(aCommand.getSiteMenuId()), aCommand.getEventId());
            workingPageInboxRepository.add(workingPageInbox);

            workingPageOutboxRepository.add(WorkingPageOutbox.create(workingPageId));
        }
    }

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void removePage(RemoveCommand aCommand) { //event command
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());

        Optional<WorkingPage> oWorkingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()));

        if (oWorkingPage.isPresent()) { //deduplication
            workingPageRepository.remove(oWorkingPage.get());
            workingPageOutboxRepository.remove(workingPageOutboxRepository.findByWorkingPageId(workingPageId).get());
            workingPageInboxRepository.remove(workingPageInboxRepository.findByWorkingPageId(workingPageId).get());
        }
    }

    @ApplicationServiceCommand(listen = true, aggregateType = WorkingPage.class)
    public void addOutlineItem(AddOutlineItemCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException("Error adding card to working page; working page with id not found"));
        WorkingCardId workingCardId = workingCardIdentityService.generate();
        workingPage.addOutlineItemToRoot(workingCardId, NEW_CARD_TITLE);
    }

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void changeOutlineItemFragment(ChangeOutlineItemFragmentCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException("Error changing outline item fragment; working page with id not found"));

        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        if (aCommand.getFragment() == null || aCommand.getFragment().equals("")) {
            workingPage.draft().removeOutlineItemFragment(workingCardId);
        }
        else {
            DraftOutlineItem draftOutlineItem = workingPage.draft().findOutlineItem(workingCardId)
                    .orElseThrow(() -> new IllegalArgumentException(
                            "Error changing outline item fragment; no draft outline item found with working card id"));
            Fragment fragment = new Fragment(aCommand.getFragment());

            if (draftOutlineItem.hasFragment()) {
                workingPage.draft().changeOutlineItemFragment(workingCardId, fragment);
            }
            else {
                workingPage.draft().addFragmentToOutlineItem(workingCardId, fragment);
            }
        }
    }

    @ApplicationServiceCommand(listen = true, aggregateType = WorkingPage.class)
    public void removeOutlineItem(RemoveOutlineItemCommand aCommand) {
        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingPage workingPage = workingPageRepository.findById(workingPageId).orElseThrow(
                () -> new IllegalArgumentException("Error removing outline item; working page with id not found"));

        workingPage.removeOutlineItem(workingCardId);
    }

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void moveOutlineItem(MoveOutlineItemCommand aCommand) {
        Assert.isTrue(aCommand.getMoveToRoot() && aCommand.getParentWorkingCardId() == null ||
                !aCommand.getMoveToRoot() && aCommand.getParentWorkingCardId() != null,
                "Error moving outline item; mismatch between 'move to root' and 'parent working card id'");

        WorkingPageId workingPageId = new WorkingPageId(aCommand.getWorkingPageId());
        WorkingCardId workingCardId = new WorkingCardId(aCommand.getWorkingCardId());

        WorkingPage workingPage = workingPageRepository.findById(workingPageId).orElseThrow(
                () -> new IllegalArgumentException("Error removing outline item; working page with id not found"));

        if (aCommand.getMoveToRoot()) {
            workingPage.draft().moveOutlineItemToRoot(workingCardId);
        }
        else {
            WorkingCardId parentWorkingCardId = new WorkingCardId(aCommand.getParentWorkingCardId());
            workingPage.draft().moveOutlineItemToParent(workingCardId, parentWorkingCardId);
        }

    }

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void moveOutlineSubItemUp(MoveOutlineSubItemUpCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error moving outline item up; working page with id not found"));

        if (aCommand.getParentWorkingCardId() == null || aCommand.getParentWorkingCardId().equals("")) {
            workingPage.draft().rootOutlineItem().moveSubItemUp(new WorkingCardId(aCommand.getWorkingCardId()));
        }
        else {
            DraftOutlineItem draftOutlineItem = workingPage.draft().findOutlineItem(new WorkingCardId(aCommand.getParentWorkingCardId()))
                    .orElseThrow(() -> new IllegalArgumentException(
                            "Error moving outline item up; working card with parent id not found"));

            draftOutlineItem.moveSubItemUp(new WorkingCardId(aCommand.getWorkingCardId()));
        }

    }

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void moveOutlineSubItemDown(MoveOutlineSubItemDownCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error moving outline item down; working page with id not found"));

        if (aCommand.getParentWorkingCardId() == null || aCommand.getParentWorkingCardId().equals("")) {
            workingPage.draft().rootOutlineItem().moveSubItemDown(new WorkingCardId(aCommand.getWorkingCardId()));
        }
        else {
            DraftOutlineItem draftOutlineItem = workingPage.draft().findOutlineItem(new WorkingCardId(aCommand.getParentWorkingCardId()))
                    .orElseThrow(() -> new IllegalArgumentException(
                            "Error moving outline item down; working card with parent id not found"));
            draftOutlineItem.moveSubItemDown(new WorkingCardId(aCommand.getWorkingCardId()));
        }
    }

    @ApplicationServiceCommand(listen = true, aggregateType = WorkingPage.class)
    public void discardDraft(DiscardDraftCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error discarding page draft; working page with id not found"));
        workingPage.discardDraft();
    }

    @ApplicationServiceCommand(listen = true, aggregateType = WorkingPage.class)
    public void publish(PublishCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error publishing page; working page with id not found"));
        workingPage.publish();
    }

    @ApplicationServiceCommand(aggregateType = WorkingPage.class)
    public void changeTitle(ChangeTitleCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error changing page's title; working page with id not found"));
        workingPage.draft().changeTitle(Title.create(aCommand.getTitle()));
    }

    @ApplicationServiceCommand(listen = true, aggregateType = WorkingPage.class)
    public void changeOutlineItemTitle(ChangeOutlineItemTitleCommand aCommand) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(aCommand.getWorkingPageId()))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Error changing outline item's title; working page with id not found"));
        workingPage.changeOutlineItemTitle(new WorkingCardId(aCommand.getWorkingCardId()), aCommand.getTitle());
    }

    public void removePages(RemoveMultipleCommand aCommand) {
        aCommand.getWorkingPageIds().stream().map(WorkingPageId::new).forEach(this::removePage);
    }

    //@ApplicationServiceCommand(aggregateType = WorkingPage.class)
    //TODO EA 8/27/2021 - Use AspectJ
    private void removePage(WorkingPageId workingPageId) {
        applicationServiceLifeCycle.begin(WorkingPage.class);
        try {
            Optional<WorkingPage> oWorkingPage = workingPageRepository.findById(workingPageId);

            if (oWorkingPage.isPresent()) { //deduplication
                workingPageRepository.remove(oWorkingPage.get());
                workingPageOutboxRepository.remove(workingPageOutboxRepository.findByWorkingPageId(workingPageId).get());
                workingPageInboxRepository.remove(workingPageInboxRepository.findByWorkingPageId(workingPageId).get());
            }
        } catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
        applicationServiceLifeCycle.success();
    }
}
