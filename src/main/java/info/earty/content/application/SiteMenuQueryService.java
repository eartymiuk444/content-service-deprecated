package info.earty.content.application;

import info.earty.content.application.data.SiteMenuDto;
import info.earty.content.domain.model.menu.Directory;
import info.earty.content.domain.model.menu.SiteMenu;
import info.earty.content.domain.model.menu.SiteMenuIdentityService;
import info.earty.content.domain.model.menu.SiteMenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SiteMenuQueryService {

    private final SiteMenuIdentityService siteMenuIdentityService;
    private final SiteMenuRepository siteMenuRepository;

    public SiteMenuDto get() {
        Optional<SiteMenu> oSiteMenu = siteMenuRepository.findById(siteMenuIdentityService.eartyInfoId());
        Assert.isTrue(oSiteMenu.isPresent(), "Error getting site menu; site menu doesn't exist");
        SiteMenu siteMenu = oSiteMenu.get();
        return assembleDto(siteMenu);
    }

    private SiteMenuDto assembleDto(SiteMenu siteMenu) {

        SiteMenuDto siteMenuDto = new SiteMenuDto();
        Map<Integer, SiteMenuDto.DirectoryDto> allDirectoryDtos = new HashMap<>();
        SiteMenuDto.DirectoryDto rootDirectoryDto = assembleDirectoryDto(siteMenu.rootDirectory(), siteMenu, allDirectoryDtos);
        allDirectoryDtos.put(rootDirectoryDto.getDirectoryId(), rootDirectoryDto);

        siteMenuDto.setSiteMenuId(siteMenu.id().id());
        siteMenuDto.setRootDirectoryId(rootDirectoryDto.getDirectoryId());
        siteMenuDto.setAllDirectories(allDirectoryDtos);

        return siteMenuDto;
    }

    private SiteMenuDto.DirectoryDto assembleDirectoryDto(Directory directory, SiteMenu siteMenu,
                                                          Map<Integer, SiteMenuDto.DirectoryDto> allDirectoryDtos) {

        SiteMenuDto.DirectoryDto directoryDto = new SiteMenuDto.DirectoryDto();
        directoryDto.setDirectoryId(directory.id().id());
        directoryDto.setName(directory.name().nameString());
        directoryDto.setPathSegment(directory.hasPathSegment() ? directory.pathSegment().pathSegmentString() : "");
        directoryDto.setItems(new ArrayList<>());

        directory.items().forEach(directoryItem -> {
            if (directoryItem.isSubDirectory()) {
                SiteMenuDto.SubDirectoryDto subDirectoryDto = new SiteMenuDto.SubDirectoryDto();
                subDirectoryDto.setDirectoryId(directoryItem.directoryId().id());
                subDirectoryDto.setName(directoryItem.name().nameString());
                subDirectoryDto.setPathSegment(directoryItem.pathSegment().pathSegmentString());

                directoryDto.getItems().add(subDirectoryDto);

                Directory childDirectory = siteMenu.findDirectory(directoryItem.directoryId()).get();
                allDirectoryDtos.put(childDirectory.id().id(),
                        assembleDirectoryDto(childDirectory, siteMenu, allDirectoryDtos));
            }
            else {
                SiteMenuDto.PageDto pageDto = new SiteMenuDto.PageDto();
                pageDto.setWorkingPageId(directoryItem.workingPageId().id());
                pageDto.setName(directoryItem.name().nameString());
                pageDto.setPathSegment(directoryItem.pathSegment().pathSegmentString());

                directoryDto.getItems().add(pageDto);
            }
        });

        return directoryDto;
    }

}
