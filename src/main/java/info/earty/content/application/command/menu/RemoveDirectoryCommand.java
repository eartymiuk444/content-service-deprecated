package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class RemoveDirectoryCommand {

    private String siteMenuId;
    private Integer directoryId;

}
