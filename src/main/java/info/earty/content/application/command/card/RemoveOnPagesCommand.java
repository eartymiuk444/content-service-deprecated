package info.earty.content.application.command.card;

import lombok.Data;

import java.util.Set;

@Data
public class RemoveOnPagesCommand {
    private int eventId;
    private String siteMenuId;
    private Set<String> workingPageIds;
}
