package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class ChangeOutlineItemFragmentCommand {
    private String workingPageId;
    private String workingCardId;
    private String fragment;
}
