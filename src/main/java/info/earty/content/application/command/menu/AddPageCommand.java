package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class AddPageCommand {
    private String siteMenuId;
    private String title;
    private Integer parentDirectoryId;
    private String pathSegment;
}
