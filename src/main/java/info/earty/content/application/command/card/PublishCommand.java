package info.earty.content.application.command.card;

import lombok.Data;

import java.util.Set;

@Data
public class PublishCommand {

    private int eventId;
    private String workingPageId;
    private Set<String> publishWorkingCardIds;
    private Set<String> removeWorkingCardIds;

}
