package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class EditDirectoryCommand {
    private String siteMenuId;
    private Integer directoryId;
    private String name;
    private String pathSegment;
}
