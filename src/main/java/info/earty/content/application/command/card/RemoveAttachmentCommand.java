package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class RemoveAttachmentCommand {

    private String workingCardId;
    private String attachmentId;

}
