package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class CreateCommand {

    private int eventId;
    private String workingCardId;
    private String workingPageId;
    private String title;

}
