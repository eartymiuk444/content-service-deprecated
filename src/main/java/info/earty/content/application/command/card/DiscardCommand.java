package info.earty.content.application.command.card;

import lombok.Data;

import java.util.Set;

@Data
public class DiscardCommand {

    private int eventId;
    private String workingPageId;
    Set<String> discardWorkingCardIds;
    Set<String> removeWorkingCardIds;

}
