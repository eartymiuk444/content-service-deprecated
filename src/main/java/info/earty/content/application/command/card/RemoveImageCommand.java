package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class RemoveImageCommand {

    private String workingCardId;
    private String imageId;

}
