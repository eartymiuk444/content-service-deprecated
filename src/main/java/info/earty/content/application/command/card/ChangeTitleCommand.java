package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class ChangeTitleCommand {
    private int eventId;
    private String workingPageId;
    private String workingCardId;
    private String title;
}
