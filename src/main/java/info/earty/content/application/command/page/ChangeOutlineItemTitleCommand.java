package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class ChangeOutlineItemTitleCommand {

    private String workingPageId;
    private String workingCardId;
    private String title;

}
