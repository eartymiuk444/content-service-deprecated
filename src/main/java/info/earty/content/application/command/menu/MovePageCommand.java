package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class MovePageCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
