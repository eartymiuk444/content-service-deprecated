package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class ChangeTextCommand {
    private String workingCardId;
    private String text;
}
