package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class AddAttachmentCommand {

    private String workingCardId;
    private String attachmentId;
    private String attachmentFilename;

}
