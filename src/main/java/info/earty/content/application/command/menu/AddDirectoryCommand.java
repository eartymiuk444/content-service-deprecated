package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class AddDirectoryCommand {

    private String siteMenuId;
    private String name;
    private Integer parentDirectoryId;
    private String pathSegment;

}
