package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class ChangeTitleCommand {
    private String workingPageId;
    private String title;
}
