package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class PublishCommand {
    private String workingPageId;
}
