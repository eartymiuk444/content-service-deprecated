package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class RemoveOutlineItemCommand {

    private String workingPageId;
    private String workingCardId;

}
