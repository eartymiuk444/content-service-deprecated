package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class ChangePageNameCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
    private String pageName;
}
