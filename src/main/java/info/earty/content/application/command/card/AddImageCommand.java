package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class AddImageCommand {

    private String workingCardId;
    private String imageId;

}
