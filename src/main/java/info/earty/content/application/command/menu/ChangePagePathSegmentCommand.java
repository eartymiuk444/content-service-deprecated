package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class ChangePagePathSegmentCommand {
    private String siteMenuId;
    private String workingPageId;
    private String pathSegment;
}
