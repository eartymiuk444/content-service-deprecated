package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class MoveOutlineSubItemDownCommand {
    private String workingPageId;
    private String parentWorkingCardId;
    private String workingCardId;
}
