package info.earty.content.application.command.page;

import lombok.Data;

import java.util.Set;

@Data
public class RemoveMultipleCommand {
    private int eventId;
    private String siteMenuId;
    private Set<String> workingPageIds;
}
