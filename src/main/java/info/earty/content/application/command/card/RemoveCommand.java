package info.earty.content.application.command.card;

import lombok.Data;

import java.util.Set;

@Data
public class RemoveCommand {
    private int eventId;
    private String workingPageId;
    private Set<String> workingCardIds;
}
