package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class RemovePageCommand {

    private String siteMenuId;
    private String workingPageId;

}
