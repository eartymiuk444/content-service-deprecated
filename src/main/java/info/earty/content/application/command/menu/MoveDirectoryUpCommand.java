package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class MoveDirectoryUpCommand {

    private String siteMenuId;
    private Integer directoryId;
    private Integer parentDirectoryId;

}
