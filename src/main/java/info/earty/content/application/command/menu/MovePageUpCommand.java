package info.earty.content.application.command.menu;

import lombok.Data;

@Data
public class MovePageUpCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
