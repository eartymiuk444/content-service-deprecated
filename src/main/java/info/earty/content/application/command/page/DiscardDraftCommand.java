package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class DiscardDraftCommand {
    private String workingPageId;
}
