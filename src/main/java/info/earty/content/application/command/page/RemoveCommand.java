package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class RemoveCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
}
