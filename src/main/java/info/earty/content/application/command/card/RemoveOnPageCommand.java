package info.earty.content.application.command.card;

import lombok.Data;

@Data
public class RemoveOnPageCommand {
    private int eventId;
    private String siteMenuId;
    private String workingPageId;
}
