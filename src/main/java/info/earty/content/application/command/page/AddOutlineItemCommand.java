package info.earty.content.application.command.page;

import lombok.Data;

@Data
public class AddOutlineItemCommand {
    private String workingPageId;
}
