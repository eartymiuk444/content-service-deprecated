package info.earty.content.application.event.menu;

import info.earty.content.domain.model.menu.SiteMenuId;

import java.util.Optional;

public interface SiteMenuInboxRepository {
    Optional<SiteMenuInbox> findBySiteMenuId(SiteMenuId siteMenuId);
    void add(SiteMenuInbox siteMenuInbox);
    void remove(SiteMenuInbox siteMenuInbox);
}
