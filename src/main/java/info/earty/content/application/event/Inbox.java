package info.earty.content.application.event;

import info.earty.content.domain.model.common.AggregateId;

public interface Inbox<ID extends AggregateId> {
    ID aggregateId();
}
