package info.earty.content.application.event.card;

import info.earty.content.application.event.Inbox;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingCardInbox implements Inbox<WorkingCardId> {

    @EqualsAndHashCode.Include
    private WorkingCardId workingCardId;
    private Map<WorkingPageId, WorkingCardWorkingPageInbox> workingPageInboxes;

    public static WorkingCardInbox create(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error creating working card inbox; working card id cannot be null");
        return new WorkingCardInbox(workingCardId, new HashMap<>());
    }

    @Override
    public WorkingCardId aggregateId() {
        return this.workingCardId;
    }

    public void eventProcessed(WorkingPageId workingPageId, int eventId) {
        if (!workingPageInboxes.containsKey(workingPageId)) {
            workingPageInboxes.put(workingPageId, WorkingCardWorkingPageInbox.create(workingPageId));
        }
        workingPageInboxes.get(workingPageId).eventProcessed(eventId);
    }

    public boolean duplicateEvent(WorkingPageId workingPageId, int eventId) {
        return workingPageInboxes.containsKey(workingPageId) &&
                workingPageInboxes.get(workingPageId).duplicateEvent(eventId);
    }
}
