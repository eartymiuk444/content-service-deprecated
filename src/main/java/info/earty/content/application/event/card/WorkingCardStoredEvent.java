package info.earty.content.application.event.card;

import info.earty.content.domain.model.card.WorkingCardDomainEvent;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class WorkingCardStoredEvent {
    @EqualsAndHashCode.Include
    private final int id;
    private final WorkingCardDomainEvent domainEvent;

    static WorkingCardStoredEvent create(int id, WorkingCardDomainEvent domainEvent) {
        return new WorkingCardStoredEvent(id, domainEvent);
    }

    public int id() {
        return this.id;
    }

    public WorkingCardDomainEvent domainEvent() {
        return this.domainEvent;
    }
}
