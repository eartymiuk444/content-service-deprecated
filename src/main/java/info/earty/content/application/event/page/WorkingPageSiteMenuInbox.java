package info.earty.content.application.event.page;

import info.earty.content.domain.model.menu.SiteMenuId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class WorkingPageSiteMenuInbox {

    @EqualsAndHashCode.Include
    private final SiteMenuId siteMenuId;
    private int lastProcessedEventId;

    void eventProcessed(int eventId) {
        lastProcessedEventId = eventId;
    }

    boolean duplicateEvent(int eventId) {
        return eventId <= lastProcessedEventId;
    }

    static WorkingPageSiteMenuInbox create(SiteMenuId siteMenuId) {
        return new WorkingPageSiteMenuInbox(siteMenuId, -1);
    }

}
