package info.earty.content.application.event.card;


public interface WorkingCardMessagePublisher {
    void send(WorkingCardStoredEvent workingCardStoredEvent);
}
