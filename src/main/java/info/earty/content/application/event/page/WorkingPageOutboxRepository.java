package info.earty.content.application.event.page;

import info.earty.content.domain.model.page.WorkingPageId;

import java.util.Collection;
import java.util.Optional;

public interface WorkingPageOutboxRepository {
    Optional<WorkingPageOutbox> findByWorkingPageId(WorkingPageId workingPageId);
    void add(WorkingPageOutbox workingPageOutbox);
    Collection<WorkingPageOutbox> findByEventQueueNotEmpty();
    void remove(WorkingPageOutbox outbox);
}
