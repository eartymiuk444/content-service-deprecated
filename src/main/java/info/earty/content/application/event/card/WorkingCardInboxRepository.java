package info.earty.content.application.event.card;

import info.earty.content.domain.model.card.WorkingCardId;

import java.util.Optional;

public interface WorkingCardInboxRepository {
    Optional<WorkingCardInbox> findByWorkingCardId(WorkingCardId workingCardId);
    void add(WorkingCardInbox workingCardInbox);
    void remove(WorkingCardInbox workingCardInbox);
}
