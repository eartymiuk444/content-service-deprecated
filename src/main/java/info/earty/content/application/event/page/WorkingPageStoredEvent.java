package info.earty.content.application.event.page;

import info.earty.content.domain.model.page.WorkingPageDomainEvent;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class WorkingPageStoredEvent {

    @EqualsAndHashCode.Include
    private final int id;
    private final WorkingPageDomainEvent domainEvent;

    static WorkingPageStoredEvent create(int id, WorkingPageDomainEvent domainEvent) {
        return new WorkingPageStoredEvent(id, domainEvent);
    }

    public int id() {
        return this.id;
    }

    public WorkingPageDomainEvent domainEvent() {
        return this.domainEvent;
    }
}
