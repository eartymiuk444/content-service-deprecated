package info.earty.content.application.event.page;

import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.page.WorkingPageDomainEvent;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingPageOutbox implements Outbox<WorkingPageId> {

    private static final int INITIAL_EVENT_ID = 1;

    @EqualsAndHashCode.Include
    private WorkingPageId workingPageId;
    private int nextEventId;
    private List<WorkingPageStoredEvent> eventQueue;

    public static WorkingPageOutbox create(WorkingPageId workingPageId) {
        Assert.notNull(workingPageId, "Error create working page outbox; working page id cannot be null");
        return new WorkingPageOutbox(workingPageId, INITIAL_EVENT_ID, new ArrayList<>());
    }

    public void enqueue(WorkingPageDomainEvent domainEvent) {
        this.eventQueue.add(WorkingPageStoredEvent.create(this.nextEventId(), domainEvent));
    }

    public WorkingPageStoredEvent dequeue() {
        return this.eventQueue.remove(0);
    }

    public boolean empty() {
        return this.eventQueue.isEmpty();
    }

    private int nextEventId() {
        int nextEventId = this.nextEventId;
        this.nextEventId = this.nextEventId + 1;
        return nextEventId;
    }

    @Override
    public WorkingPageId aggregateId() {
        return this.workingPageId;
    }
}
