package info.earty.content.application.event;

import info.earty.content.application.common.ApplicationServiceLifeCycle;
import info.earty.content.application.event.card.WorkingCardMessagePublisher;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.card.WorkingCardOutboxRepository;
import info.earty.content.application.event.menu.SiteMenuMessagePublisher;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.application.event.menu.SiteMenuOutboxRepository;
import info.earty.content.application.event.page.WorkingPageMessagePublisher;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.application.event.page.WorkingPageOutboxRepository;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.menu.SiteMenu;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.menu.SiteMenuIdentityService;
import info.earty.content.domain.model.page.WorkingPage;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NotificationService {

    private final SiteMenuOutboxRepository siteMenuOutboxRepository;
    private final SiteMenuIdentityService siteMenuIdentityService;

    private final WorkingPageOutboxRepository workingPageOutboxRepository;
    private final WorkingCardOutboxRepository workingCardOutboxRepository;

    private final SiteMenuMessagePublisher siteMenuMessagePublisher;
    private final WorkingPageMessagePublisher workingPageMessagePublisher;
    private final WorkingCardMessagePublisher workingCardMessagePublisher;

    private final ApplicationServiceLifeCycle applicationServiceLifeCycle;

    public void publishSiteMenuNotifications() {
        Collection<SiteMenuOutbox> candidateSiteMenuOutboxes =
                siteMenuOutboxRepository.findByEventQueueNotEmpty();
        candidateSiteMenuOutboxes.forEach(candidateSiteMenuOutbox -> {
            this.publishSiteMenuNotifications(candidateSiteMenuOutbox.aggregateId());
        });
    }

    private void publishSiteMenuNotifications(SiteMenuId siteMenuId) {
        try {
            applicationServiceLifeCycle.begin(SiteMenu.class);
            Optional<SiteMenuOutbox> oSiteMenuOutbox =
                    siteMenuOutboxRepository.findBySiteMenuId(siteMenuId);
            oSiteMenuOutbox.ifPresent(siteMenuOutbox -> {
                while (!siteMenuOutbox.empty()) {
                    siteMenuMessagePublisher.send(siteMenuOutbox.dequeue());
                }
            });
            applicationServiceLifeCycle.success();
        }
        catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
    }

    public void publishWorkingPageNotifications() {
        Collection<WorkingPageOutbox> candidateWorkingPageOutboxes =
                workingPageOutboxRepository.findByEventQueueNotEmpty();
        candidateWorkingPageOutboxes.forEach(candidateWorkingPageOutbox -> {
            this.publishWorkingPageNotifications(candidateWorkingPageOutbox.aggregateId());
        });
    }

    private void publishWorkingPageNotifications(WorkingPageId workingPageId) {
        try {
            applicationServiceLifeCycle.begin(WorkingPage.class);
            Optional<WorkingPageOutbox> oWorkingPageOutbox =
                    workingPageOutboxRepository.findByWorkingPageId(workingPageId);
            oWorkingPageOutbox.ifPresent(workingPageOutbox -> {
                while (!workingPageOutbox.empty()) {
                    workingPageMessagePublisher.send(workingPageOutbox.dequeue());
                }
            });
            applicationServiceLifeCycle.success();
        }
        catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
    }

    public void publishWorkingCardNotifications() {
        Collection<WorkingCardOutbox> candidateWorkingCardOutboxes =
                workingCardOutboxRepository.findByEventQueueNotEmpty();
        candidateWorkingCardOutboxes.forEach(candidateWorkingCardOutbox -> {
            this.publishWorkingCardNotifications(candidateWorkingCardOutbox.aggregateId());
        });
    }

    private void publishWorkingCardNotifications(WorkingCardId workingCardId) {
        try {
            applicationServiceLifeCycle.begin(WorkingCard.class);
            Optional<WorkingCardOutbox> oWorkingCardOutbox =
                    workingCardOutboxRepository.findByWorkingCardId(workingCardId);
            oWorkingCardOutbox.ifPresent(workingCardOutbox -> {
                while (!workingCardOutbox.empty()) {
                    workingCardMessagePublisher.send(workingCardOutbox.dequeue());
                }
            });
            applicationServiceLifeCycle.success();
        }
        catch (RuntimeException e) {
            applicationServiceLifeCycle.fail(e);
        }
    }

}
