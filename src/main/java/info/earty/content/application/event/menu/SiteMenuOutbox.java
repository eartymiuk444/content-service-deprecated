package info.earty.content.application.event.menu;

import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.menu.SiteMenuDomainEvent;
import info.earty.content.domain.model.menu.SiteMenuId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SiteMenuOutbox implements Outbox<SiteMenuId> {

    private static final int INITIAL_EVENT_ID = 1;

    @EqualsAndHashCode.Include
    private SiteMenuId siteMenuId;
    private int nextEventId;
    private List<SiteMenuStoredEvent> eventQueue;

    public static SiteMenuOutbox create(SiteMenuId siteMenuId) {
        Assert.notNull(siteMenuId, "Error creating site menu outbox; site menu id cannot be null");
        return new SiteMenuOutbox(siteMenuId, INITIAL_EVENT_ID, new ArrayList<>());
    }

    public void enqueue(SiteMenuDomainEvent domainEvent) {
        this.eventQueue.add(SiteMenuStoredEvent.create(this.nextEventId(), domainEvent));
    }

    public SiteMenuStoredEvent dequeue() {
        return this.eventQueue.remove(0);
    }

    public boolean empty() {
        return this.eventQueue.isEmpty();
    }

    private int nextEventId() {
        int nextEventId = this.nextEventId;
        this.nextEventId = this.nextEventId + 1;
        return nextEventId;
    }

    @Override
    public SiteMenuId aggregateId() {
        return this.siteMenuId;
    }
}
