package info.earty.content.application.event.page;

import info.earty.content.application.event.Inbox;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingPageInbox implements Inbox<WorkingPageId> {

    @EqualsAndHashCode.Include
    private WorkingPageId workingPageId;

    private Map<SiteMenuId, WorkingPageSiteMenuInbox> siteMenuInboxes;

    @Override
    public WorkingPageId aggregateId() {
        return this.workingPageId;
    }

    public static WorkingPageInbox create(WorkingPageId workingPageId) {
        Assert.notNull(workingPageId, "Error creating working page inbox; working page id cannot be null");
        return new WorkingPageInbox(workingPageId, new HashMap<>());
    }

    public void eventProcessed(SiteMenuId siteMenuId, int eventId) {
        if (!siteMenuInboxes.containsKey(siteMenuId)) {
            siteMenuInboxes.put(siteMenuId, WorkingPageSiteMenuInbox.create(siteMenuId));
        }
        siteMenuInboxes.get(siteMenuId).eventProcessed(eventId);
    }

    public boolean duplicateEvent(SiteMenuId siteMenuId, int eventId) {
        return siteMenuInboxes.containsKey(siteMenuId) &&
                siteMenuInboxes.get(siteMenuId).duplicateEvent(eventId);
    }
}
