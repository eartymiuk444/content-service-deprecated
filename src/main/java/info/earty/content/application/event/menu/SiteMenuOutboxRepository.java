package info.earty.content.application.event.menu;

import info.earty.content.domain.model.menu.SiteMenuId;

import java.util.Collection;
import java.util.Optional;

public interface SiteMenuOutboxRepository {
    Optional<SiteMenuOutbox> findBySiteMenuId(SiteMenuId siteMenuId);
    void add(SiteMenuOutbox siteMenuOutbox);
    void remove(SiteMenuOutbox siteMenuOutbox);
    Collection<SiteMenuOutbox> findByEventQueueNotEmpty();
}
