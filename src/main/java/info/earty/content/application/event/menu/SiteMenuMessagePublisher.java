package info.earty.content.application.event.menu;

public interface SiteMenuMessagePublisher {
    void send(SiteMenuStoredEvent siteMenuStoredEvent);
}
