package info.earty.content.application.event.page;

import info.earty.content.domain.model.page.WorkingPageId;

import java.util.Optional;

public interface WorkingPageInboxRepository {
    Optional<WorkingPageInbox> findByWorkingPageId(WorkingPageId workingPageId);
    void add(WorkingPageInbox workingPageInbox);
    void remove(WorkingPageInbox inbox);
}
