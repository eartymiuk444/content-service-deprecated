package info.earty.content.application.event.card;

import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.card.WorkingCardDomainEvent;
import info.earty.content.domain.model.card.WorkingCardId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingCardOutbox implements Outbox<WorkingCardId> {

    private static final int INITIAL_EVENT_ID = 1;

    @EqualsAndHashCode.Include
    private WorkingCardId workingCardId;
    private int nextEventId;
    private List<WorkingCardStoredEvent> eventQueue;

    public static WorkingCardOutbox create(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error create working card outbox; working card id cannot be null");
        return new WorkingCardOutbox(workingCardId, INITIAL_EVENT_ID, new ArrayList<>());
    }

    @Override
    public WorkingCardId aggregateId() {
        return this.workingCardId;
    }

    public void enqueue(WorkingCardDomainEvent domainEvent) {
        this.eventQueue.add(WorkingCardStoredEvent.create(this.nextEventId(), domainEvent));
    }

    public WorkingCardStoredEvent dequeue() {
        return this.eventQueue.remove(0);
    }

    public boolean empty() {
        return this.eventQueue.isEmpty();
    }

    private int nextEventId() {
        int nextEventId = this.nextEventId;
        this.nextEventId = this.nextEventId + 1;
        return nextEventId;
    }
}
