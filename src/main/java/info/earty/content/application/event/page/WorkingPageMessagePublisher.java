package info.earty.content.application.event.page;

public interface WorkingPageMessagePublisher {
    void send(WorkingPageStoredEvent workingPageStoredEvent);
}
