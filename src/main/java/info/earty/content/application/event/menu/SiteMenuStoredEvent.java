package info.earty.content.application.event.menu;

import info.earty.content.domain.model.menu.SiteMenuDomainEvent;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SiteMenuStoredEvent {

    @EqualsAndHashCode.Include
    private final int id;
    private final SiteMenuDomainEvent domainEvent;

    static SiteMenuStoredEvent create(int id, SiteMenuDomainEvent domainEvent) {
        return new SiteMenuStoredEvent(id, domainEvent);
    }

    public int id() {
        return this.id;
    }

    public SiteMenuDomainEvent domainEvent() {
        return this.domainEvent;
    }

}
