package info.earty.content.application.event;

import info.earty.content.domain.model.common.AggregateId;

public interface Outbox<ID extends AggregateId> {
    ID aggregateId();
}
