package info.earty.content.application.event.menu;

import info.earty.content.application.event.Inbox;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SiteMenuInbox implements Inbox<SiteMenuId> {

    @EqualsAndHashCode.Include
    private SiteMenuId siteMenuId;
    private Map<WorkingPageId, SiteMenuWorkingPageInbox> workingPageInboxes;

    @Override
    public SiteMenuId aggregateId() {
        return this.siteMenuId;
    }

    public static SiteMenuInbox create(SiteMenuId siteMenuId) {
        Assert.notNull(siteMenuId, "Error creating site menu inbox; site menu id cannot be null");
        return new SiteMenuInbox(siteMenuId, new HashMap<>());
    }

    public void eventProcessed(WorkingPageId workingPageId, int eventId) {
        if (!workingPageInboxes.containsKey(workingPageId)) {
            workingPageInboxes.put(workingPageId, SiteMenuWorkingPageInbox.create(workingPageId));
        }
        workingPageInboxes.get(workingPageId).eventProcessed(eventId);
    }

    public boolean duplicateEvent(WorkingPageId workingPageId, int eventId) {
        return workingPageInboxes.containsKey(workingPageId) &&
                workingPageInboxes.get(workingPageId).duplicateEvent(eventId);
    }
}
