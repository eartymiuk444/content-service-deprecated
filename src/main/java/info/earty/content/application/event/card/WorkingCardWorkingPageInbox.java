package info.earty.content.application.event.card;

import info.earty.content.domain.model.page.WorkingPageId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class WorkingCardWorkingPageInbox {

    @EqualsAndHashCode.Include
    private final WorkingPageId workingPageId;
    private int lastProcessedEventId;

    void eventProcessed(int eventId) {
        lastProcessedEventId = eventId;
    }

    boolean duplicateEvent(int eventId) {
        return eventId <= lastProcessedEventId;
    }

    static WorkingCardWorkingPageInbox create(WorkingPageId workingPageId) {
        return new WorkingCardWorkingPageInbox(workingPageId, -1);
    }

}
