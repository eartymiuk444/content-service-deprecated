package info.earty.content.application.event.card;

import info.earty.content.domain.model.card.WorkingCardId;

import java.util.Collection;
import java.util.Optional;

public interface WorkingCardOutboxRepository {

    Optional<WorkingCardOutbox> findByWorkingCardId(WorkingCardId workingCardId);
    void add(WorkingCardOutbox workingCardOutbox);
    Collection<WorkingCardOutbox> findByEventQueueNotEmpty();
    void remove(WorkingCardOutbox workingCardOutbox);
}
