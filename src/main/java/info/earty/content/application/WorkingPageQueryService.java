package info.earty.content.application;

import info.earty.content.application.data.DraftDto;
import info.earty.content.application.data.PublishedPageDto;
import info.earty.content.domain.model.page.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class WorkingPageQueryService {

    private final WorkingPageRepository workingPageRepository;

    public DraftDto getDraft(String workingPageId) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(workingPageId))
                .orElseThrow(() -> new IllegalArgumentException("Error retrieving draft; no working page found with id"));
        return assembleDto(workingPage.draft());
    }

    public PublishedPageDto getPublishedPage(String workingPageId) {
        WorkingPage workingPage = workingPageRepository.findById(new WorkingPageId(workingPageId))
                .orElseThrow(() -> new IllegalArgumentException("Error retrieving published page; no working page found with id"));
        return assembleDto(workingPage.publishedPage());
    }

    private DraftDto assembleDto(Draft draft) {
        DraftDto draftDto = new DraftDto();
        draftDto.setTitle(draft.title().titleString());
        Map<String, DraftDto.OutlineItemDto> allOutlineItemDtos = new HashMap<>();
        DraftDto.OutlineItemDto rootOutlineItemDto = assembleDraftOutlineItemDto(
                draft.rootOutlineItem(), draft, allOutlineItemDtos);
        allOutlineItemDtos.put("", rootOutlineItemDto);

        draftDto.setAllOutlineItems(allOutlineItemDtos);

        return draftDto;
    }

    private PublishedPageDto assembleDto(PublishedPage publishedPage) {

        PublishedPageDto publishedPageDto = new PublishedPageDto();
        publishedPageDto.setTitle(publishedPage.title().titleString());
        Map<String, PublishedPageDto.OutlineItemDto> allOutlineItemDtos = new HashMap<>();
        PublishedPageDto.OutlineItemDto rootOutlineItemDto = assemblePublishedPageOutlineItemDto(
                publishedPage.rootOutlineItem(), publishedPage, allOutlineItemDtos);
        allOutlineItemDtos.put("", rootOutlineItemDto);

        publishedPageDto.setAllOutlineItems(allOutlineItemDtos);

        return publishedPageDto;
    }

    private PublishedPageDto.OutlineItemDto assemblePublishedPageOutlineItemDto(PublishedOutlineItem publishedOutlineItem,
                                                                                PublishedPage publishedPage,
                                                                                Map<String, PublishedPageDto.OutlineItemDto> allOutlineItemDtos) {

        PublishedPageDto.OutlineItemDto outlineItemDto = new PublishedPageDto.OutlineItemDto();
        outlineItemDto.setWorkingCardId(publishedOutlineItem.draftOutlineItemId().isRootId() ? null :
                publishedOutlineItem.draftOutlineItemId().workingCardId().id());
        outlineItemDto.setTitle(publishedOutlineItem.title());
        outlineItemDto.setFragment(publishedOutlineItem.hasFragment() ? publishedOutlineItem.fragment().fragmentString() : null);
        outlineItemDto.setSubItems(new ArrayList<>());

        publishedOutlineItem.subItems().forEach(subItem -> {
            PublishedPageDto.OutlineSubItemDto outlineSubItemDto = new PublishedPageDto.OutlineSubItemDto();
            outlineSubItemDto.setWorkingCardId(subItem.workingCardId().id());
            outlineSubItemDto.setTitle(subItem.title());
            outlineSubItemDto.setFragment(subItem.hasFragment() ? subItem.fragment().fragmentString() : null);

            outlineItemDto.getSubItems().add(outlineSubItemDto);

            PublishedOutlineItem childOutlineItem = publishedPage.findOutlineItem(subItem.workingCardId()).get();
            allOutlineItemDtos.put(childOutlineItem.draftOutlineItemId().workingCardId().id(),
                    assemblePublishedPageOutlineItemDto(childOutlineItem, publishedPage, allOutlineItemDtos));
        });

        return outlineItemDto;
    }

    private DraftDto.OutlineItemDto assembleDraftOutlineItemDto(DraftOutlineItem draftOutlineItem,
                                                                                Draft draft,
                                                                                Map<String, DraftDto.OutlineItemDto> allOutlineItemDtos) {

        DraftDto.OutlineItemDto outlineItemDto = new DraftDto.OutlineItemDto();
        outlineItemDto.setWorkingCardId(draftOutlineItem.id().isRootId() ? null : draftOutlineItem.id().workingCardId().id());
        outlineItemDto.setTitle(draftOutlineItem.title());
        outlineItemDto.setFragment(draftOutlineItem.hasFragment() ? draftOutlineItem.fragment().fragmentString() : null);
        outlineItemDto.setSubItems(new ArrayList<>());

        draftOutlineItem.subItems().forEach(subItem -> {
            DraftDto.OutlineSubItemDto outlineSubItemDto = new DraftDto.OutlineSubItemDto();
            outlineSubItemDto.setWorkingCardId(subItem.workingCardId().id());
            outlineSubItemDto.setTitle(subItem.title());
            outlineSubItemDto.setFragment(subItem.hasFragment() ? subItem.fragment().fragmentString() : null);

            outlineItemDto.getSubItems().add(outlineSubItemDto);

            DraftOutlineItem childOutlineItem = draft.findOutlineItem(subItem.workingCardId()).get();
            allOutlineItemDtos.put(childOutlineItem.id().workingCardId().id(),
                    assembleDraftOutlineItemDto(childOutlineItem, draft, allOutlineItemDtos));
        });

        return outlineItemDto;
    }

}
