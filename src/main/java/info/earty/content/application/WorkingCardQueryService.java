package info.earty.content.application;

import info.earty.content.application.data.DraftCardDto;
import info.earty.content.application.data.PublishedCardDto;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.card.WorkingCardRepository;
import info.earty.content.domain.model.page.WorkingPageId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WorkingCardQueryService {

    private final WorkingCardRepository workingCardRepository;

    public DraftCardDto getDraftCard(String workingCardId) {
        WorkingCard workingCard = workingCardRepository.findById(new WorkingCardId(workingCardId))
                .orElseThrow(() -> new IllegalArgumentException("Error retrieving draft card; no card found with id"));

        return assembleDraftDto(workingCard);
    }

    public PublishedCardDto getPublishedCard(String workingCardId) {
        WorkingCard workingCard = workingCardRepository.findById(new WorkingCardId(workingCardId))
                .orElseThrow(() -> new IllegalArgumentException("Error retrieving published card; no card found with id"));

        return assemblePublishedDto(workingCard);
    }

    public Set<PublishedCardDto> getPublishedCardsByWorkingPageId(String workingPageId) {
        Set<WorkingCard> workingCards = workingCardRepository.findByWorkingPageId(new WorkingPageId(workingPageId));
        return workingCards.stream().map(this::assemblePublishedDto).collect(Collectors.toSet());
    }

    public Set<DraftCardDto> getDraftCardsByWorkingPageId(String workingPageId) {
        Set<WorkingCard> workingCards = workingCardRepository.findByWorkingPageId(new WorkingPageId(workingPageId));
        return workingCards.stream().map(this::assembleDraftDto).collect(Collectors.toSet());
    }

    private DraftCardDto assembleDraftDto(WorkingCard workingCard) {
        DraftCardDto dto = new DraftCardDto();
        dto.setWorkingCardId(workingCard.id().id());
        dto.setTitle(workingCard.draftCard().title());
        dto.setText(workingCard.draftCard().text());
        dto.setImages(workingCard.draftCard().images().stream().map(image -> {
            DraftCardDto.Image dtoImage = new DraftCardDto.Image();
            dtoImage.setImageId(image.imageId().id());
            return dtoImage;
        }).collect(Collectors.toList()));
        dto.setAttachments(workingCard.draftCard().attachments().stream().map(attachment -> {
            DraftCardDto.Attachment dtoAttachment = new DraftCardDto.Attachment();
            dtoAttachment.setAttachmentId(attachment.attachmentId().id());
            dtoAttachment.setFilename(attachment.filename());
            return dtoAttachment;
        }).collect(Collectors.toList()));
        return dto;
    }

    private PublishedCardDto assemblePublishedDto(WorkingCard workingCard) {
        PublishedCardDto dto = new PublishedCardDto();
        dto.setWorkingCardId(workingCard.id().id());
        dto.setTitle(workingCard.publishedCard().title());
        dto.setText(workingCard.publishedCard().text());
        dto.setImages(workingCard.publishedCard().images().stream().map(image -> {
            PublishedCardDto.Image dtoImage = new PublishedCardDto.Image();
            dtoImage.setImageId(image.imageId().id());
            return dtoImage;
        }).collect(Collectors.toList()));
        dto.setAttachments(workingCard.publishedCard().attachments().stream().map(attachment -> {
            PublishedCardDto.Attachment dtoAttachment = new PublishedCardDto.Attachment();
            dtoAttachment.setAttachmentId(attachment.attachmentId().id());
            dtoAttachment.setFilename(attachment.filename());
            return dtoAttachment;
        }).collect(Collectors.toList()));
        return dto;
    }

}
