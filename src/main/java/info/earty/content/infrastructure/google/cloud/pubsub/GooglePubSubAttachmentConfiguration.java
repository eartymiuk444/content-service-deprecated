package info.earty.content.infrastructure.google.cloud.pubsub;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.common.collect.Lists;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import info.earty.content.application.WorkingCardCommandService;
import info.earty.content.application.command.card.AddAttachmentCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Configuration
@RequiredArgsConstructor
public class GooglePubSubAttachmentConfiguration {

    private static final String GOOGLE_APIS_AUTH_ENDPOINT = "https://www.googleapis.com/auth/cloud-platform";
    private static final String EVENT_TYPE_ATTRIBUTE = "eventType";
    private static final String OBJECT_FINALIZE_EVENT_TYPE = "OBJECT_FINALIZE";

    @Value("${earty.info.content.google.project-id}")
    private String googleProjectId;

    @Value("${earty.info.content.google.pub-sub.attachment.json-key}")
    private String googlePubSubAttachmentJsonKey;

    @Value("${earty.info.content.google.pub-sub.attachment.subscription-id}")
    private String googlePubSubAttachmentSubscriptionId;

    private final ObjectMapper objectMapper;
    private final WorkingCardCommandService workingCardCommandService;

    @PostConstruct
    public void postConstruct() {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googlePubSubAttachmentJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));

            ProjectSubscriptionName subscriptionName =
                    ProjectSubscriptionName.of(googleProjectId, googlePubSubAttachmentSubscriptionId);

            MessageReceiver receiver =
                    (PubsubMessage message, AckReplyConsumer consumer) -> {
                        if (message.getAttributesMap().get(EVENT_TYPE_ATTRIBUTE).equals(OBJECT_FINALIZE_EVENT_TYPE)) {
                            try {
                                GoogleStorageEventMessage googleStorageEventMessage = objectMapper.readValue(message.getData().toStringUtf8(), GoogleStorageEventMessage.class);
                                AddAttachmentCommand addAttachmentCommand = new AddAttachmentCommand();
                                addAttachmentCommand.setAttachmentId(googleStorageEventMessage.getName());
                                addAttachmentCommand.setWorkingCardId(googleStorageEventMessage.getMetadata().get("workingCardId"));
                                addAttachmentCommand.setAttachmentFilename(googleStorageEventMessage.getMetadata().get("filename"));
                                workingCardCommandService.addAttachment(addAttachmentCommand);
                                consumer.ack();
                            } catch (JsonProcessingException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        else {
                            consumer.ack();
                        }
                    };

            Subscriber subscriber = Subscriber.newBuilder(subscriptionName, receiver)
                    .setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

            subscriber.startAsync().awaitRunning();
            System.out.printf("Listening for messages on %s:\n", subscriptionName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
