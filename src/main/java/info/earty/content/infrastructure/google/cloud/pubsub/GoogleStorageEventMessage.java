package info.earty.content.infrastructure.google.cloud.pubsub;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleStorageEventMessage {

    private String name;
    private Map<String, String> metadata;

}
