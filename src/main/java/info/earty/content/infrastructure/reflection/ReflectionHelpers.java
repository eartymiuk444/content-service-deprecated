package info.earty.content.infrastructure.reflection;

import lombok.SneakyThrows;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class ReflectionHelpers {

    @SneakyThrows
    public static <T> T getField(Object target, String name, Class<T> clazz) {
        Field field = target.getClass().getDeclaredField(name);
        ReflectionUtils.makeAccessible(field);
        Object objectField = ReflectionUtils.getField(field, target);
        return clazz.cast(objectField);
    }

    @SneakyThrows
    public static <T> T newInstance(Class<T> clazz) {
        Constructor<T> constructor = ReflectionUtils.accessibleConstructor(clazz);
        return constructor.newInstance();
    }

    @SneakyThrows
    public static <T> Constructor<T> accessibleConstructor(Class<T> clazz, Class<?>... parameterTypes) {
        Constructor<T> constructor = ReflectionUtils.accessibleConstructor(clazz, parameterTypes);
        return constructor;
    }

    @SneakyThrows
    public static <T> T newInstance(Constructor<T> constructor, Object... initArgs) {
        return constructor.newInstance(initArgs);
    }

    @SneakyThrows
    public static void setField(Object target, String name, Object value) {
        Field field = target.getClass().getDeclaredField(name);
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, target, value);
    }
}
