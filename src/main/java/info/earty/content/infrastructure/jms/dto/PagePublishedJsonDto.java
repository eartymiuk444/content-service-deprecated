package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class PagePublishedJsonDto {
    private int id;
    private String siteMenuId;
    private String workingPageId;
    private String publishedTitle;
    private Set<String> priorPublishedCardIds;
    private Set<String> currentPublishedCardIds;
    private Instant occurredOn;
    private Set<String> publishedCardIdsRemoved;
}
