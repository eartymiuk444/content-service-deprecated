package info.earty.content.infrastructure.jms.subscribe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.content.application.SiteMenuCommandService;
import info.earty.content.application.WorkingCardCommandService;
import info.earty.content.application.command.card.*;
import info.earty.content.application.command.menu.ChangePageNameCommand;
import info.earty.content.infrastructure.jms.JmsConfiguration;
import info.earty.content.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class JmsWorkingPageMessageSubscriber {

    private final ObjectMapper objectMapper;

    private final SiteMenuCommandService siteMenuCommandService;
    private final WorkingCardCommandService workingCardCommandService;

    @JmsListener(destination = "working-page", containerFactory = "myFactory")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(JmsConfiguration.TYPE_ID_PROPERTY);

        if (type.equals(PagePublishedJsonDto.class.getName())) {
            PagePublishedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PagePublishedJsonDto.class);

            ChangePageNameCommand changePageNameCommand = new ChangePageNameCommand();
            changePageNameCommand.setEventId(dto.getId());
            changePageNameCommand.setSiteMenuId(dto.getSiteMenuId());
            changePageNameCommand.setWorkingPageId(dto.getWorkingPageId());

            changePageNameCommand.setPageName(dto.getPublishedTitle());
            siteMenuCommandService.changePageName(changePageNameCommand);

            PublishCommand publishCommand = new PublishCommand();
            publishCommand.setEventId(dto.getId());
            publishCommand.setWorkingPageId(dto.getWorkingPageId());

            publishCommand.setPublishWorkingCardIds(dto.getCurrentPublishedCardIds());
            publishCommand.setRemoveWorkingCardIds(dto.getPublishedCardIdsRemoved());
            workingCardCommandService.publish(publishCommand);
        } else if (type.equals(DraftDiscardedJsonDto.class.getName())) {
            DraftDiscardedJsonDto dto = objectMapper.readValue(message.getBody(String.class), DraftDiscardedJsonDto.class);

            DiscardCommand discardCommand = new DiscardCommand();
            discardCommand.setEventId(dto.getId());
            discardCommand.setWorkingPageId(dto.getWorkingPageId());

            discardCommand.setDiscardWorkingCardIds(dto.getCurrentDraftCardIds());
            discardCommand.setRemoveWorkingCardIds(dto.getDraftCardIdsRemoved());
            workingCardCommandService.discard(discardCommand);
        } else if (type.equals(OutlineItemAddedJsonDto.class.getName())) {
            OutlineItemAddedJsonDto dto = objectMapper.readValue(message.getBody(String.class), OutlineItemAddedJsonDto.class);

            CreateCommand createCommand = new CreateCommand();
            createCommand.setEventId(dto.getId());
            createCommand.setWorkingPageId(dto.getWorkingPageId());

            createCommand.setWorkingCardId(dto.getWorkingCardId());
            createCommand.setTitle(dto.getTitle());
            workingCardCommandService.create(createCommand);
        } else if (type.equals(OutlineItemRemovedJsonDto.class.getName())) {
            OutlineItemRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), OutlineItemRemovedJsonDto.class);

            RemoveCommand removeCommand = new RemoveCommand();
            removeCommand.setEventId(dto.getId());
            removeCommand.setWorkingPageId(dto.getWorkingPageId());

            removeCommand.setWorkingCardIds(dto.getOrphanedOutlineItems());
            workingCardCommandService.remove(removeCommand);
        } else if (type.equals(DraftOutlineItemTitleChangedJsonDto.class.getName())) {
            DraftOutlineItemTitleChangedJsonDto dto = objectMapper.readValue(message.getBody(String.class), DraftOutlineItemTitleChangedJsonDto.class);

            ChangeTitleCommand changeTitleCommand = new ChangeTitleCommand();
            changeTitleCommand.setEventId(dto.getId());
            changeTitleCommand.setWorkingPageId(dto.getWorkingPageId());

            changeTitleCommand.setWorkingCardId(dto.getWorkingCardId());
            changeTitleCommand.setTitle(dto.getTitle());

            workingCardCommandService.changeTitle(changeTitleCommand);
        } else {
            throw new IllegalArgumentException("Error receiving working-page message; unknown message type");
        }
    }

}
