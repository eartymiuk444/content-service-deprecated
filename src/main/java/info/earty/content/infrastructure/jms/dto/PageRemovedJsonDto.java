package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class PageRemovedJsonDto {
    private int id;
    private String siteMenuId;
    private String workingPageId;
    private Instant occurredOn;

}
