package info.earty.content.infrastructure.jms.subscribe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.content.application.WorkingCardCommandService;
import info.earty.content.application.WorkingPageCommandService;
import info.earty.content.application.command.card.RemoveOnPageCommand;
import info.earty.content.application.command.card.RemoveOnPagesCommand;
import info.earty.content.application.command.page.CreateCommand;
import info.earty.content.application.command.page.RemoveCommand;
import info.earty.content.application.command.page.RemoveMultipleCommand;
import info.earty.content.infrastructure.jms.JmsConfiguration;
import info.earty.content.infrastructure.jms.dto.DirectoryRemovedJsonDto;
import info.earty.content.infrastructure.jms.dto.PageAddedJsonDto;
import info.earty.content.infrastructure.jms.dto.PageRemovedJsonDto;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class JmsSiteMenuMessageSubscriber {

    private static final String SITE_MENU_QUEUE = "site-menu";

    private final ObjectMapper objectMapper;

    private final WorkingPageCommandService workingPageCommandService;
    private final WorkingCardCommandService workingCardCommandService;

    @JmsListener(destination = SITE_MENU_QUEUE, containerFactory = "myFactory")
    public void receiveMessage(Message message, @Headers Map<String, String> headers) throws JMSException, JsonProcessingException {
        String type = headers.get(JmsConfiguration.TYPE_ID_PROPERTY);

        if (type.equals(PageAddedJsonDto.class.getName())) {
            PageAddedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PageAddedJsonDto.class);

            CreateCommand createCommand = new CreateCommand();
            createCommand.setEventId(dto.getId());
            createCommand.setSiteMenuId(dto.getSiteMenuId());

            createCommand.setWorkingPageId(dto.getWorkingPageId());
            createCommand.setTitle(dto.getName());
            workingPageCommandService.createPage(createCommand);
        } else if (type.equals(PageRemovedJsonDto.class.getName())) {
            PageRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), PageRemovedJsonDto.class);

            RemoveCommand removeCommand = new RemoveCommand();
            removeCommand.setEventId(dto.getId());
            removeCommand.setSiteMenuId(dto.getSiteMenuId());

            removeCommand.setWorkingPageId(dto.getWorkingPageId());
            workingPageCommandService.removePage(removeCommand);

            RemoveOnPageCommand removeOnPageCommand = new RemoveOnPageCommand();
            removeOnPageCommand.setEventId(dto.getId());
            removeOnPageCommand.setSiteMenuId(dto.getSiteMenuId());

            removeOnPageCommand.setWorkingPageId(dto.getWorkingPageId());
            workingCardCommandService.removeCardsOnPage(removeOnPageCommand);
        } else if (type.equals(DirectoryRemovedJsonDto.class.getName())) {
            DirectoryRemovedJsonDto dto = objectMapper.readValue(message.getBody(String.class), DirectoryRemovedJsonDto.class);

            RemoveMultipleCommand removeMultipleCommand = new RemoveMultipleCommand();
            removeMultipleCommand.setEventId(dto.getId());
            removeMultipleCommand.setSiteMenuId(dto.getSiteMenuId());

            removeMultipleCommand.setWorkingPageIds(dto.getPagesRemoved());
            workingPageCommandService.removePages(removeMultipleCommand);

            RemoveOnPagesCommand removeOnPagesCommand = new RemoveOnPagesCommand();
            removeOnPagesCommand.setEventId(dto.getId());
            removeOnPagesCommand.setSiteMenuId(dto.getSiteMenuId());

            removeOnPagesCommand.setWorkingPageIds(dto.getPagesRemoved());
            workingCardCommandService.removeCardsOnPages(removeOnPagesCommand);
        } else {
            throw new IllegalArgumentException("Error receiving site-menu message; unknown message type");
        }
    }
}
