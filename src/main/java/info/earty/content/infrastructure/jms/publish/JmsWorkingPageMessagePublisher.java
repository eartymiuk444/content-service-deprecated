package info.earty.content.infrastructure.jms.publish;

import info.earty.content.application.event.page.WorkingPageMessagePublisher;
import info.earty.content.application.event.page.WorkingPageStoredEvent;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.page.*;
import info.earty.content.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static org.apache.activemq.artemis.reader.MessageUtil.JMSXGROUPID;

@Component
@RequiredArgsConstructor
public class JmsWorkingPageMessagePublisher implements WorkingPageMessagePublisher {

    private static final String WORKING_PAGE_QUEUE = "working-page";

    private final JmsTemplate jmsTemplate;

    @Override
    public void send(WorkingPageStoredEvent workingPageStoredEvent) {

        WorkingPageDomainEvent domainEvent = workingPageStoredEvent.domainEvent();

        if (domainEvent instanceof PagePublished) {
            PagePublished pagePublished = (PagePublished)domainEvent;
            PagePublishedJsonDto dto = new PagePublishedJsonDto();

            dto.setId(workingPageStoredEvent.id());
            dto.setSiteMenuId(pagePublished.siteMenuId().id());
            dto.setWorkingPageId(pagePublished.workingPageId().id());
            dto.setOccurredOn(pagePublished.occurredOn());

            dto.setPublishedTitle(pagePublished.publishedTitle().titleString());
            dto.setPriorPublishedCardIds(pagePublished.priorPublishedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setCurrentPublishedCardIds(pagePublished.currentPublishedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setPublishedCardIdsRemoved(pagePublished.publishedOutlineItemsRemoved().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));

            jmsTemplate.convertAndSend(WORKING_PAGE_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, pagePublished.workingPageId().id());
                return message;
            });
        } else if (domainEvent instanceof DraftDiscarded) {
            DraftDiscarded draftDiscarded = (DraftDiscarded)domainEvent;
            DraftDiscardedJsonDto dto = new DraftDiscardedJsonDto();

            dto.setId(workingPageStoredEvent.id());
            dto.setWorkingPageId(draftDiscarded.workingPageId().id());
            dto.setOccurredOn(draftDiscarded.occurredOn());

            dto.setPriorDraftCardIds(draftDiscarded.priorDraftOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setCurrentDraftCardIds(draftDiscarded.currentDraftOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setDraftCardIdsRemoved(draftDiscarded.draftOutlineItemsRemoved().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));

            jmsTemplate.convertAndSend(WORKING_PAGE_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, draftDiscarded.workingPageId().id());
                return message;
            });
        } else if (domainEvent instanceof OutlineItemAdded) {
            OutlineItemAdded outlineItemAdded = (OutlineItemAdded)domainEvent;
            OutlineItemAddedJsonDto dto = new OutlineItemAddedJsonDto();

            dto.setId(workingPageStoredEvent.id());
            dto.setOccurredOn(outlineItemAdded.occurredOn());
            dto.setWorkingPageId(outlineItemAdded.workingPageId().id());

            dto.setWorkingCardId(outlineItemAdded.workingCardId().id());
            dto.setTitle(outlineItemAdded.title());

            jmsTemplate.convertAndSend(WORKING_PAGE_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, outlineItemAdded.workingPageId().id());
                return message;
            });
        } else if (domainEvent instanceof OutlineItemRemoved) {
            OutlineItemRemoved outlineItemRemoved = (OutlineItemRemoved) domainEvent;
            OutlineItemRemovedJsonDto dto = new OutlineItemRemovedJsonDto();

            dto.setId(workingPageStoredEvent.id());
            dto.setOccurredOn(outlineItemRemoved.occurredOn());
            dto.setWorkingPageId(outlineItemRemoved.workingPageId().id());

            dto.setPublishedOutlineItems(outlineItemRemoved.publishedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setDraftOutlineItemsRemoved(outlineItemRemoved.draftOutlineItemsRemoved().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));
            dto.setOrphanedOutlineItems(outlineItemRemoved.orphanedOutlineItems().stream()
                    .map(WorkingCardId::id).collect(Collectors.toSet()));

            jmsTemplate.convertAndSend(WORKING_PAGE_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, outlineItemRemoved.workingPageId().id());
                return message;
            });

        } else if (domainEvent instanceof DraftOutlineItemTitleChanged) {
            DraftOutlineItemTitleChanged draftOutlineItemTitleChanged = (DraftOutlineItemTitleChanged) domainEvent;
            DraftOutlineItemTitleChangedJsonDto dto = new DraftOutlineItemTitleChangedJsonDto();

            dto.setId(workingPageStoredEvent.id());
            dto.setOccurredOn(draftOutlineItemTitleChanged.occurredOn());
            dto.setWorkingPageId(draftOutlineItemTitleChanged.workingPageId().id());

            dto.setWorkingCardId(draftOutlineItemTitleChanged.workingCardId().id());
            dto.setTitle(draftOutlineItemTitleChanged.title());

            jmsTemplate.convertAndSend(WORKING_PAGE_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, draftOutlineItemTitleChanged.workingPageId().id());
                return message;
            });
        } else {
            throw new IllegalArgumentException("Error sending working page stored event via jms; " +
                    "working page domain event is an unknown type");
        }

    }

}
