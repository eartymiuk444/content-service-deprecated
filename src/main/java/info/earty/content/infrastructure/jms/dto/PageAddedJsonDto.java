package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class PageAddedJsonDto {
    private int id;
    private String siteMenuId;
    private String workingPageId;
    private String name;
    private Instant occurredOn;
}
