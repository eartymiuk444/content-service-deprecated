package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class DraftOutlineItemTitleChangedJsonDto {

    private int id;
    private String workingPageId;
    private String workingCardId;
    private String title;
    private Instant occurredOn;

}
