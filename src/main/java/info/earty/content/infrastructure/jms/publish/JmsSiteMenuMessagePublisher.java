package info.earty.content.infrastructure.jms.publish;

import info.earty.content.application.event.menu.SiteMenuMessagePublisher;
import info.earty.content.application.event.menu.SiteMenuStoredEvent;
import info.earty.content.domain.model.menu.DirectoryRemoved;
import info.earty.content.domain.model.menu.PageAdded;
import info.earty.content.domain.model.menu.PageRemoved;
import info.earty.content.domain.model.menu.SiteMenuDomainEvent;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.jms.dto.DirectoryRemovedJsonDto;
import info.earty.content.infrastructure.jms.dto.PageAddedJsonDto;
import info.earty.content.infrastructure.jms.dto.PageRemovedJsonDto;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static org.apache.activemq.artemis.reader.MessageUtil.JMSXGROUPID;

@Component
@RequiredArgsConstructor
public class JmsSiteMenuMessagePublisher implements SiteMenuMessagePublisher {

    private static final String SITE_MENU_QUEUE = "site-menu";

    private final JmsTemplate jmsTemplate;

    @Override
    public void send(SiteMenuStoredEvent siteMenuStoredEvent) {
        SiteMenuDomainEvent domainEvent = siteMenuStoredEvent.domainEvent();
        if (domainEvent instanceof PageAdded) {
            PageAdded pageAdded = (PageAdded)domainEvent;
            PageAddedJsonDto dto = new PageAddedJsonDto();

            dto.setId(siteMenuStoredEvent.id());
            dto.setSiteMenuId(pageAdded.siteMenuId().id());
            dto.setOccurredOn(pageAdded.occurredOn());

            dto.setWorkingPageId(pageAdded.workingPageId().id());
            dto.setName(pageAdded.name().nameString());
            jmsTemplate.convertAndSend(SITE_MENU_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, pageAdded.siteMenuId().id());
                return message;
            });
        } else if (domainEvent instanceof PageRemoved) {
            PageRemoved pageRemoved = (PageRemoved) domainEvent;
            PageRemovedJsonDto dto = new PageRemovedJsonDto();

            dto.setId(siteMenuStoredEvent.id());
            dto.setSiteMenuId(pageRemoved.siteMenuId().id());
            dto.setOccurredOn(pageRemoved.occurredOn());

            dto.setWorkingPageId(pageRemoved.workingPageId().id());
            jmsTemplate.convertAndSend(SITE_MENU_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, pageRemoved.siteMenuId().id());
                return message;
            });
        } else if (domainEvent instanceof DirectoryRemoved) {
            DirectoryRemoved directoryRemoved = (DirectoryRemoved) domainEvent;
            DirectoryRemovedJsonDto dto = new DirectoryRemovedJsonDto();

            dto.setId(siteMenuStoredEvent.id());
            dto.setSiteMenuId(directoryRemoved.siteMenuId().id());
            dto.setOccurredOn(directoryRemoved.occurredOn());

            dto.setPagesRemoved(directoryRemoved.pagesRemoved().stream().map(WorkingPageId::id)
                    .collect(Collectors.toSet()));
            jmsTemplate.convertAndSend(SITE_MENU_QUEUE, dto, message -> {
                message.setStringProperty(JMSXGROUPID, directoryRemoved.siteMenuId().id());
                return message;
            });
        } else {
            throw new IllegalArgumentException("Error sending working page stored event via jms; " +
                    "working page domain event is an unknown type");
        }

    }
}
