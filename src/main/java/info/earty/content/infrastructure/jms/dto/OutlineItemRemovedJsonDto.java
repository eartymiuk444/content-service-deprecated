package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class OutlineItemRemovedJsonDto {

    private int id;
    private String workingPageId;
    private Set<String> publishedOutlineItems;
    private Set<String> draftOutlineItemsRemoved;
    private Set<String> orphanedOutlineItems;
    private Instant occurredOn;

}
