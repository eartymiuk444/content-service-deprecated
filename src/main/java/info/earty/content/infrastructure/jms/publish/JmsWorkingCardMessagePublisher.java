package info.earty.content.infrastructure.jms.publish;

import info.earty.content.application.event.card.WorkingCardMessagePublisher;
import info.earty.content.application.event.card.WorkingCardStoredEvent;
import info.earty.content.domain.model.attachment.AttachmentId;
import info.earty.content.domain.model.card.*;
import info.earty.content.domain.model.image.ImageId;
import info.earty.workingcard.infrastructure.jms.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static org.apache.activemq.artemis.reader.MessageUtil.JMSXGROUPID;

@Component
@RequiredArgsConstructor
public class JmsWorkingCardMessagePublisher implements WorkingCardMessagePublisher {

    private static final String WORKING_CARD_TOPIC = "working-card";

    private final JmsTemplate pubSubJmsTemplate;

    @Override
    public void send(WorkingCardStoredEvent workingCardStoredEvent) {
        WorkingCardDomainEvent domainEvent = workingCardStoredEvent.domainEvent();

        if (domainEvent instanceof AddImageFailed) {
            AddImageFailed addImageFailed = (AddImageFailed) domainEvent;
            AddImageFailedJsonDto dto = new AddImageFailedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(addImageFailed.workingCardId().id());
            dto.setImageId(addImageFailed.imageId().id());
            dto.setOccurredOn(addImageFailed.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, addImageFailed.workingCardId().id());
                return message;
            });
        } else if (domainEvent instanceof AddAttachmentFailed) {
            AddAttachmentFailed addAttachmentFailed = (AddAttachmentFailed) domainEvent;
            AddAttachmentFailedJsonDto dto = new AddAttachmentFailedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(addAttachmentFailed.workingCardId().id());
            dto.setAttachmentId(addAttachmentFailed.attachmentId().id());
            dto.setOccurredOn(addAttachmentFailed.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, addAttachmentFailed.workingCardId().id());
                return message;
            });
        } else if (domainEvent instanceof ImageRemoved) {
            ImageRemoved imageRemoved = (ImageRemoved) domainEvent;
            ImageRemovedJsonDto dto = new ImageRemovedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(imageRemoved.workingCardId().id());
            dto.setImageRemoved(imageRemoved.imageRemoved().id());
            dto.setImageOrphaned(imageRemoved.imageOrphaned());
            dto.setOccurredOn(imageRemoved.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, imageRemoved.workingCardId().id());
                return message;
            });
        } else if (domainEvent instanceof AttachmentRemoved) {
            AttachmentRemoved attachmentRemoved = (AttachmentRemoved) domainEvent;
            AttachmentRemovedJsonDto dto = new AttachmentRemovedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(attachmentRemoved.workingCardId().id());
            dto.setAttachmentRemoved(attachmentRemoved.attachmentRemoved().id());
            dto.setAttachmentOrphaned(attachmentRemoved.attachmentOrphaned());
            dto.setOccurredOn(attachmentRemoved.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, attachmentRemoved.workingCardId().id());
                return message;
            });
        } else if (domainEvent instanceof CardPublished) {
            CardPublished cardPublished = (CardPublished) domainEvent;
            CardPublishedJsonDto dto = new CardPublishedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(cardPublished.workingCardId().id());
            
            dto.setPriorPublishedImages(cardPublished.priorPublishedImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setCurrentPublishedImages(cardPublished.currentPublishedImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setPublishedImagesRemoved(cardPublished.publishedImagesRemoved().stream().map(ImageId::id).collect(Collectors.toSet()));
            dto.setPublishedImagesAdded(cardPublished.publishedImagesAdded().stream().map(ImageId::id).collect(Collectors.toSet()));
            
            dto.setPriorPublishedAttachments(cardPublished.priorPublishedAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setCurrentPublishedAttachments(cardPublished.currentPublishedAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setPublishedAttachmentsRemoved(cardPublished.publishedAttachmentsRemoved().stream().map(AttachmentId::id).collect(Collectors.toSet()));
            dto.setPublishedAttachmentsAdded(cardPublished.publishedAttachmentsAdded().stream().map(AttachmentId::id).collect(Collectors.toSet()));
            
            dto.setOccurredOn(cardPublished.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, cardPublished.workingCardId().id());
                return message;
            });
        } else if (domainEvent instanceof DraftDiscarded) {
            DraftDiscarded draftDiscarded = (DraftDiscarded) domainEvent;
            CardDraftDiscardedJsonDto dto = new CardDraftDiscardedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(draftDiscarded.workingCardId().id());
            
            dto.setPriorDraftImages(draftDiscarded.priorDraftImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setCurrentImages(draftDiscarded.currentImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setDraftImagesRemoved(draftDiscarded.draftImagesRemoved().stream().map(ImageId::id).collect(Collectors.toSet()));

            dto.setPriorDraftAttachments(draftDiscarded.priorDraftAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setCurrentAttachments(draftDiscarded.currentAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setDraftAttachmentsRemoved(draftDiscarded.draftAttachmentsRemoved().stream().map(AttachmentId::id).collect(Collectors.toSet()));
            
            dto.setOccurredOn(draftDiscarded.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, draftDiscarded.workingCardId().id());
                return message;
            });
        } else if (domainEvent instanceof CardDeleted) {
            CardDeleted cardDeleted = (CardDeleted) domainEvent;
            CardDeletedJsonDto dto = new CardDeletedJsonDto();

            dto.setId(workingCardStoredEvent.id());
            dto.setWorkingCardId(cardDeleted.workingCardId().id());
            
            dto.setPublishedImages(cardDeleted.publishedImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setDraftImages(cardDeleted.draftImages().stream().map(ImageId::id).collect(Collectors.toList()));
            dto.setOrphanedImages(cardDeleted.orphanedImages().stream().map(ImageId::id).collect(Collectors.toSet()));

            dto.setPublishedAttachments(cardDeleted.publishedAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setDraftAttachments(cardDeleted.draftAttachments().stream().map(AttachmentId::id).collect(Collectors.toList()));
            dto.setOrphanedAttachments(cardDeleted.orphanedAttachments().stream().map(AttachmentId::id).collect(Collectors.toSet()));
            
            dto.setOccurredOn(cardDeleted.occurredOn());

            pubSubJmsTemplate.convertAndSend(WORKING_CARD_TOPIC, dto, message -> {
                message.setStringProperty(JMSXGROUPID, cardDeleted.workingCardId().id());
                return message;
            });
        } else {
            throw new IllegalArgumentException("Error sending working card stored event via jms; " +
                    "working card domain event is an unknown type");
        }
    }
}
