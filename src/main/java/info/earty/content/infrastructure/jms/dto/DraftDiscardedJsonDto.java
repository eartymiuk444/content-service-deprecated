package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class DraftDiscardedJsonDto {
    private int id;
    private String workingPageId;
    private Set<String> priorDraftCardIds;
    private Set<String> currentDraftCardIds;
    private Instant occurredOn;
    private Set<String> draftCardIdsRemoved;
}
