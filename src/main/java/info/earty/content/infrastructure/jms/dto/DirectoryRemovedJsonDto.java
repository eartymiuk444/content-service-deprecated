package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class DirectoryRemovedJsonDto {

    private int id;
    private String siteMenuId;
    private Set<String> pagesRemoved;
    private Instant occurredOn;

}
