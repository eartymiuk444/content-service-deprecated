package info.earty.content.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class OutlineItemAddedJsonDto {
    private int id;
    private String workingCardId;
    private String workingPageId;
    private String title;
    private Instant occurredOn;
}
