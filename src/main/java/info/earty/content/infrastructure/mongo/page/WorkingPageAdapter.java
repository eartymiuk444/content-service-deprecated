package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.application.event.page.WorkingPageSiteMenuInbox;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.page.*;
import info.earty.content.infrastructure.mongo.AggregateAdapter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.earty.content.infrastructure.reflection.ReflectionHelpers.getField;

@Component
public class WorkingPageAdapter extends
        AggregateAdapter<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage> {

    private static final String ROOT_DRAFT_OUTLINE_ITEM_ID = "ROOT_DRAFT_OUTLINE_ITEM_ID";

    @Override
    public String adaptIdToString(WorkingPageId aggregateId) {
        return aggregateId.id();
    }
    @Override
    protected void clearAggregateInternal(MongoWorkingPage document) {
        document.setSiteMenuId(null);
        document.setLastPublished(null);
        document.setPublishedPage(null);
        document.setDraft(null);
        document.setNextDraftIdInt(0);

    }

    @Override
    protected void setAggregateInternal(WorkingPage aggregate, MongoWorkingPage document) {
        //domain provides read access
        document.setSiteMenuId(aggregate.siteMenuId());
        document.setLastPublished(aggregate.lastPublished());
        document.setPublishedPage(toDocument(aggregate.publishedPage()));
        document.setDraft(toDocument(aggregate.draft()));

        //domain restricts read access
        //nextDraftIdInt
        document.setNextDraftIdInt(getField(aggregate, "nextDraftIdInt", Integer.class));
    }

    private MongoPublishedPage toDocument(PublishedPage publishedPage) {
        MongoPublishedPage mongoPublishedPage = new MongoPublishedPage();
        mongoPublishedPage.setTitle(publishedPage.title());

        Map<String, PublishedOutlineItem> allOutlineItems = new HashMap<>();
        Map<DraftOutlineItemId, PublishedOutlineItem> allOutlineItemsDomain = getField(publishedPage, "allOutlineItems", Map.class);
        allOutlineItemsDomain.forEach(((draftOutlineItemId, publishedOutlineItem) ->
                allOutlineItems.put(draftOutlineItemId.isRootId() ? ROOT_DRAFT_OUTLINE_ITEM_ID :
                        draftOutlineItemId.workingCardId().id(), publishedOutlineItem)));

        mongoPublishedPage.setAllOutlineItems(allOutlineItems);

        return mongoPublishedPage;
    }

    private MongoDraft toDocument(Draft draft) {
        MongoDraft mongoDraft = new MongoDraft();
        mongoDraft.setId(draft.id());
        mongoDraft.setTitle(draft.title());

        Map<String, DraftOutlineItem> allOutlineItems = new HashMap<>();
        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItemsDomain = getField(draft, "allOutlineItems", Map.class);
        allOutlineItemsDomain.forEach(((draftOutlineItemId, draftOutlineItem) ->
                allOutlineItems.put(draftOutlineItemId.isRootId() ? ROOT_DRAFT_OUTLINE_ITEM_ID :
                        draftOutlineItemId.workingCardId().id(), draftOutlineItem)));
        mongoDraft.setAllOutlineItems(allOutlineItems);

        Map<String, DraftOutlineItemId> parentOutlineItems = new HashMap<>();
        Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItemsDomain = getField(draft, "parentOutlineItems", Map.class);
        parentOutlineItemsDomain.forEach(((draftOutlineItemId, parentId) ->
                parentOutlineItems.put(draftOutlineItemId.isRootId() ? ROOT_DRAFT_OUTLINE_ITEM_ID :
                        draftOutlineItemId.workingCardId().id(), parentId)));
        mongoDraft.setParentOutlineItems(parentOutlineItems);

        return mongoDraft;
    }

    @Override
    protected void clearOutboxInternal(MongoWorkingPage document) {
        document.setEventQueue(null);
        document.setNextEventId(0);
    }

    @Override
    protected void setOutboxInternal(WorkingPageOutbox outbox, MongoWorkingPage document) {
        document.setEventQueue(getField(outbox, "eventQueue", List.class));
        document.setNextEventId(getField(outbox, "nextEventId", Integer.class));
    }

    @Override
    protected void clearInboxInternal(MongoWorkingPage document) {
        document.setSiteMenuInboxes(null);
    }

    @Override
    protected void setInboxInternal(WorkingPageInbox inbox, MongoWorkingPage document) {
        Map<SiteMenuId, WorkingPageSiteMenuInbox> siteMenuInboxes = getField(inbox, "siteMenuInboxes", Map.class);
        Map<String, WorkingPageSiteMenuInbox> mongoWorkingPageSiteMenuInboxes = new HashMap<>();
        siteMenuInboxes.forEach((k, v) -> mongoWorkingPageSiteMenuInboxes.put(k.id(), v));
        document.setSiteMenuInboxes(mongoWorkingPageSiteMenuInboxes);
    }

    @Override
    protected MongoWorkingPage newDocumentInstance() {
        return new MongoWorkingPage();
    }
}
