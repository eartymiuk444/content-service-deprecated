package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuInbox;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.application.event.menu.SiteMenuWorkingPageInbox;
import info.earty.content.domain.model.menu.*;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.MongoDocumentAdapter;
import info.earty.content.infrastructure.reflection.ReflectionHelpers;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.earty.content.infrastructure.reflection.ReflectionHelpers.*;

@Component
public class MongoSiteMenuAdapter implements
        MongoDocumentAdapter<MongoSiteMenu, SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox> {
    public SiteMenu reconstituteAggregate(MongoSiteMenu mongoSiteMenu) {
        Assert.notNull(mongoSiteMenu, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(mongoSiteMenu.isHasAggregate(), "Error reconstituting aggregate; the document does not contain an aggregate instance");

        Map<DirectoryId, Directory> allDirectories = new HashMap<>();
        mongoSiteMenu.getAllDirectories().forEach((directoryId, directory) -> allDirectories.put(new DirectoryId(directoryId), directory));

        Map<WorkingPageId, DirectoryItem> allPages = new HashMap<>();
        mongoSiteMenu.getAllPages().forEach((workingPageId, page) -> allPages.put(new WorkingPageId(workingPageId), page));

        Map<DirectoryId, DirectoryId> directoryParents = new HashMap<>();
        mongoSiteMenu.getDirectoryParents().forEach((directoryId, parentId) -> directoryParents.put(new DirectoryId(directoryId), parentId));

        Map<WorkingPageId, DirectoryId> pageParents = new HashMap<>();
        mongoSiteMenu.getPageParents().forEach((workingPageId, parentId) -> pageParents.put(new WorkingPageId(workingPageId), parentId));

        SiteMenu siteMenu = newInstance(accessibleConstructor(SiteMenu.class, SiteMenuId.class, DirectoryId.class, Map.class, Map.class, Map.class, Map.class),
                new SiteMenuId(mongoSiteMenu.getId()), mongoSiteMenu.getRootDirectoryId(), allDirectories, allPages, directoryParents, pageParents);

        setField(siteMenu, "nextDirectoryIdInt", mongoSiteMenu.getNextDirectoryIdInt());

        return siteMenu;
    }

    public SiteMenuOutbox reconstituteOutbox(MongoSiteMenu mongoSiteMenu) {
        Assert.notNull(mongoSiteMenu, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(mongoSiteMenu.isHasOutbox(), "Error reconstituting outbox; the document does not contain an outbox");
        return newInstance(accessibleConstructor(SiteMenuOutbox.class, SiteMenuId.class, int.class, List.class),
                new SiteMenuId(mongoSiteMenu.getId()), mongoSiteMenu.getNextEventId(), mongoSiteMenu.getEventQueue());
    }

    public SiteMenuInbox reconstituteInbox(MongoSiteMenu mongoSiteMenu) {
        Assert.notNull(mongoSiteMenu, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(mongoSiteMenu.isHasInbox(), "Error reconstituting inbox; the document does not contain an inbox");

        Map<String, SiteMenuWorkingPageInbox> mongoSiteMenuWorkingPageInboxes = mongoSiteMenu.getWorkingPageInboxes();
        Map<WorkingPageId, SiteMenuWorkingPageInbox> siteMenuWorkingPageInboxes = new HashMap<>();

        mongoSiteMenuWorkingPageInboxes.forEach((k,v) -> siteMenuWorkingPageInboxes.put(new WorkingPageId(k), v));

        return newInstance(accessibleConstructor(SiteMenuInbox.class, SiteMenuId.class, Map.class),
                new SiteMenuId(mongoSiteMenu.getId()), siteMenuWorkingPageInboxes);
    }
}
