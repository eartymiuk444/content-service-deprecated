package info.earty.content.infrastructure.mongo;

import info.earty.content.application.event.Inbox;
import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;

public interface MongoDocumentAdapter<D extends Document, ID extends AggregateId, A extends Aggregate<ID>,
        O extends Outbox<ID>, I extends Inbox<ID>> {

    A reconstituteAggregate(D document);
    O reconstituteOutbox(D document);
    I reconstituteInbox(D document);

}
