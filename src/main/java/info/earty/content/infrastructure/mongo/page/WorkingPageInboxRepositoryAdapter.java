package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageInboxRepository;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.domain.model.page.WorkingPage;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WorkingPageInboxRepositoryAdapter implements WorkingPageInboxRepository {

    private final MongoWorkingPageRepository mongoWorkingPageRepository;
    private final MongoWorkingPageAdapter mongoWorkingPageAdapter;

    @Override
    public Optional<WorkingPageInbox> findByWorkingPageId(WorkingPageId workingPageId) {
        Assert.notNull(workingPageId, "Error retrieving inbox; aggregate id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readInbox(workingPageId);
        }
        else {
            Optional<MongoWorkingPage> oMongoWorkingPage = mongoWorkingPageRepository.findById(workingPageId.id());
            return oMongoWorkingPage.map(mongoWorkingPageAdapter::reconstituteInbox);
        }
    }

    @Override
    public void add(WorkingPageInbox inbox) {
        Assert.notNull(inbox, "Error adding inbox to repository; inbox cannot be null");
        MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createInbox(inbox);
    }

    @Override
    public void remove(WorkingPageInbox inbox) {
        Assert.notNull(inbox, "Error removing inbox from repository; inbox cannot be null");
        MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteInbox(inbox.aggregateId());
    }
}
