package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuInbox;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.application.event.menu.SiteMenuWorkingPageInbox;
import info.earty.content.domain.model.menu.*;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.AggregateAdapter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.earty.content.infrastructure.reflection.ReflectionHelpers.getField;

@Component
public class SiteMenuAdapter extends
        AggregateAdapter<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu> {

    @Override
    public String adaptIdToString(SiteMenuId aggregateId) {
        return aggregateId.id();
    }

    @Override
    protected void clearAggregateInternal(MongoSiteMenu document) {
        document.setRootDirectoryId(null);
        document.setAllDirectories(null);
        document.setAllPages(null);
        document.setDirectoryParents(null);
        document.setPageParents(null);
        document.setNextDirectoryIdInt(0);
    }

    @Override
    protected void setAggregateInternal(SiteMenu aggregate, MongoSiteMenu document) {
        //domain provides read access
        document.setRootDirectoryId(aggregate.rootDirectory().id());

        //domain restricts read access
        //allDirectories
        Map<DirectoryId, Directory> allDirectoriesDomain = getField(aggregate, "allDirectories", Map.class);
        Map<Integer, Directory> allDirectories = new HashMap<>();
        allDirectoriesDomain.forEach((directoryId, directory) -> allDirectories.put(directoryId.id(), directory));
        document.setAllDirectories(allDirectories);

        //allPages
        Map<WorkingPageId, DirectoryItem> allPagesDomain = getField(aggregate, "allPages", Map.class);
        Map<String, DirectoryItem> allPages = new HashMap<>();
        allPagesDomain.forEach((workingPageId, directoryItem) -> allPages.put(workingPageId.id(), directoryItem));
        document.setAllPages(allPages);

        //directoryParents
        Map<DirectoryId, DirectoryId> directoryParentsDomain = getField(aggregate, "directoryParents", Map.class);
        Map<Integer, DirectoryId> directoryParents = new HashMap<>();
        directoryParentsDomain.forEach((directoryId, parentId) -> directoryParents.put(directoryId.id(), parentId));
        document.setDirectoryParents(directoryParents);

        //pageParents
        Map<WorkingPageId, DirectoryId> pageParentsDomain = getField(aggregate, "pageParents", Map.class);
        Map<String, DirectoryId> pageParents = new HashMap<>();
        pageParentsDomain.forEach((workingPageId, parentId) -> pageParents.put(workingPageId.id(), parentId));
        document.setPageParents(pageParents);

        //nextDirectoryIdInt
        document.setNextDirectoryIdInt(getField(aggregate, "nextDirectoryIdInt", Integer.class));
    }

    @Override
    protected void clearOutboxInternal(MongoSiteMenu document) {
        document.setEventQueue(null);
        document.setNextEventId(0);
    }

    @Override
    protected void setOutboxInternal(SiteMenuOutbox outbox, MongoSiteMenu document) {
        document.setEventQueue(getField(outbox, "eventQueue", List.class));
        document.setNextEventId(getField(outbox, "nextEventId", Integer.class));
    }

    @Override
    protected void clearInboxInternal(MongoSiteMenu document) {
        document.setWorkingPageInboxes(null);
    }

    @Override
    protected void setInboxInternal(SiteMenuInbox inbox, MongoSiteMenu document) {
        Map<WorkingPageId, SiteMenuWorkingPageInbox> siteMenuWorkingPageInboxes = getField(inbox, "workingPageInboxes", Map.class);
        Map<String, SiteMenuWorkingPageInbox> mongoSiteMenuWorkingPageInboxes = new HashMap<>();
        siteMenuWorkingPageInboxes.forEach((k, v) -> mongoSiteMenuWorkingPageInboxes.put(k.id(), v));
        document.setWorkingPageInboxes(mongoSiteMenuWorkingPageInboxes);
    }

    @Override
    protected MongoSiteMenu newDocumentInstance() {
        return new MongoSiteMenu();
    }
}