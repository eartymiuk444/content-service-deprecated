package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.card.WorkingCardOutboxRepository;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WorkingCardOutboxRepositoryAdapter implements WorkingCardOutboxRepository {

    private final MongoWorkingCardRepository mongoWorkingCardRepository;
    private final MongoWorkingCardAdapter mongoWorkingCardAdapter;

    @Override
    public Optional<WorkingCardOutbox> findByWorkingCardId(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error retrieving outbox; working card id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readOutbox(workingCardId);
        }
        else {
            Optional<MongoWorkingCard> oMongoWorkingCard = mongoWorkingCardRepository.findById(workingCardId.id());
            return oMongoWorkingCard.map(mongoWorkingCardAdapter::reconstituteOutbox);
        }
    }

    @Override
    public void add(WorkingCardOutbox outbox) {
        Assert.notNull(outbox, "Error adding outbox to repository; outbox cannot be null");
        MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createOutbox(outbox);
    }

    @Override
    public Collection<WorkingCardOutbox> findByEventQueueNotEmpty() {
        return mongoWorkingCardRepository.findByEventQueueNot(new ArrayList<>())
                .stream().map(mongoWorkingCardAdapter::reconstituteOutbox).collect(Collectors.toSet());
    }

    @Override
    public void remove(WorkingCardOutbox outbox) {
        Assert.notNull(outbox, "Error removing outbox from repository; outbox cannot be null");
        MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteOutbox(outbox.aggregateId());
    }
}
