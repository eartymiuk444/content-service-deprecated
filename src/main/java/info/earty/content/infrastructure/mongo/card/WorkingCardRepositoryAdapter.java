package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.card.WorkingCardRepository;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WorkingCardRepositoryAdapter implements WorkingCardRepository {

    private final MongoWorkingCardRepository mongoWorkingCardRepository;
    private final MongoWorkingCardAdapter mongoWorkingCardAdapter;

    @Override
    public Optional<WorkingCard> findById(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error finding working card; working card id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readAggregate(workingCardId);
        }
        else {
            Optional<MongoWorkingCard> oMongoWorkingCard = mongoWorkingCardRepository.findById(workingCardId.id());
            return oMongoWorkingCard.map(mongoWorkingCardAdapter::reconstituteAggregate);
        }
    }
    
    @Override
    public void add(WorkingCard workingCard) {
        Assert.notNull(workingCard, "Error adding working card to repository; working card cannot be null");
        MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createAggregate(workingCard);
    }
    
    @Override
    public void remove(WorkingCard workingCard) {
        Assert.notNull(workingCard, "Error removing working card from repository; working card cannot be null");
        MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteAggregate(workingCard.id());
    }

    @Override
    public boolean existsById(WorkingCardId workingCardId) {
        return this.findById(workingCardId).isPresent();
    }

    @Override
    public Set<WorkingCard> findByWorkingPageId(WorkingPageId workingPageId) {
        Assert.isTrue(!MongoUnitOfWork.exists(), "Error retrieving working cards by working page id; " +
                "a unit of work exists and multiple cards cannot be worked on as a unit");
       return mongoWorkingCardRepository.findByWorkingPageId(workingPageId).stream()
                .map(mongoWorkingCardAdapter::reconstituteAggregate).collect(Collectors.toSet());
    }

}
