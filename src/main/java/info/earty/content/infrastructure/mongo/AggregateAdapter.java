package info.earty.content.infrastructure.mongo;

import info.earty.content.application.event.Inbox;
import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;
import org.springframework.util.Assert;

public abstract class AggregateAdapter<ID extends AggregateId,
        A extends Aggregate<ID>, O extends Outbox<ID>, I extends Inbox<ID>, D extends Document> {
    public abstract String adaptIdToString(ID aggregateId);

    public D adaptToDocument(A aggregate, O outbox, I inbox, D cachedDocument) {
        Assert.isTrue(aggregate != null || outbox != null || inbox != null,
                "Error adapting aggregate, inbox, outbox to document; one of aggregate, inbox, outbox must not be null");

        D document = cachedDocument == null ? this.newDocumentInstance() : cachedDocument;

        //aggregate
        if (aggregate == null) {
            clearAggregate(document);
        } else {
            setAggregate(aggregate, document);
        }

        //outbox
        if (outbox == null) {
            clearOutbox(document);
        } else {
            setOutbox(outbox, document);
        }

        //inbox
        if (inbox == null) {
            clearInbox(document);
        } else {
            setInbox(inbox, document);
        }

        return document;
    }

    protected void clearAggregate(D document) {
        document.setHasAggregate(false);
        this.clearAggregateInternal(document);
    }
    protected void setAggregate(A aggregate, D document) {
        document.setHasAggregate(true);

        if (document.getId() == null) {
            document.setId(this.adaptIdToString(aggregate.id()));
        }

        Assert.isTrue(document.getId().equals(this.adaptIdToString(aggregate.id())),
                "Error adapting aggregate to document; the document id doesn't match the aggregate's id");
        this.setAggregateInternal(aggregate, document);
    }

    protected void clearOutbox(D document) {
        document.setHasOutbox(false);
        this.clearOutboxInternal(document);
    }
    protected void setOutbox(O outbox, D document) {
        document.setHasOutbox(true);

        if (document.getId() == null) {
            document.setId(this.adaptIdToString(outbox.aggregateId()));
        }

        Assert.isTrue(document.getId().equals(this.adaptIdToString(outbox.aggregateId())),
                "Error adapting outbox to document; the document id doesn't match the aggregate's id");
        this.setOutboxInternal(outbox, document);
    }

    protected void clearInbox(D document) {
        document.setHasInbox(false);
        this.clearInboxInternal(document);
    }

    protected void setInbox(I inbox, D document) {
        document.setHasInbox(true);

        if (document.getId() == null) {
            document.setId(this.adaptIdToString(inbox.aggregateId()));
        }

        Assert.isTrue(document.getId().equals(this.adaptIdToString(inbox.aggregateId())),
                "Error adapting inbox to document; the document id doesn't match the aggregate's id");
        this.setInboxInternal(inbox, document);
    }

    protected abstract void clearAggregateInternal(D document);
    protected abstract void setAggregateInternal(A aggregate, D document);

    protected abstract void clearOutboxInternal(D document);
    protected abstract void setOutboxInternal(O outbox, D document);

    protected abstract void clearInboxInternal(D document);
    protected abstract void setInboxInternal(I inbox, D document);

    protected abstract D newDocumentInstance();
}
