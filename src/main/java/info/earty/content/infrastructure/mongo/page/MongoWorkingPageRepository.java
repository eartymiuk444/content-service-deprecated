package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageStoredEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoWorkingPageRepository extends MongoRepository<MongoWorkingPage, String> {
    List<MongoWorkingPage> findByEventQueueNot(List<WorkingPageStoredEvent> eventQueue);
}
