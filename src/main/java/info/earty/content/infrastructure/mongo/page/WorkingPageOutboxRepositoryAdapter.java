package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.application.event.page.WorkingPageOutboxRepository;
import info.earty.content.domain.model.page.WorkingPage;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WorkingPageOutboxRepositoryAdapter implements WorkingPageOutboxRepository {
    
    private final MongoWorkingPageRepository mongoWorkingPageRepository;
    private final MongoWorkingPageAdapter mongoWorkingPageAdapter;
    
    @Override
    public Optional<WorkingPageOutbox> findByWorkingPageId(WorkingPageId workingPageId) {
        Assert.notNull(workingPageId, "Error retrieving outbox; working page id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readOutbox(workingPageId);
        }
        else {
            Optional<MongoWorkingPage> oMongoWorkingPage = mongoWorkingPageRepository.findById(workingPageId.id());
            return oMongoWorkingPage.map(mongoWorkingPageAdapter::reconstituteOutbox);
        }
    }

    @Override
    public void add(WorkingPageOutbox outbox) {
        Assert.notNull(outbox, "Error adding outbox to repository; outbox cannot be null");
        MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createOutbox(outbox);
    }

    @Override
    public Collection<WorkingPageOutbox> findByEventQueueNotEmpty() {
        return mongoWorkingPageRepository.findByEventQueueNot(new ArrayList<>())
                .stream().map(mongoWorkingPageAdapter::reconstituteOutbox).collect(Collectors.toSet());
    }

    @Override
    public void remove(WorkingPageOutbox outbox) {
        Assert.notNull(outbox, "Error removing outbox from repository; outbox cannot be null");
        MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteOutbox(outbox.aggregateId());
    }

}
