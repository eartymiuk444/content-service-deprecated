package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuInbox;
import info.earty.content.application.event.menu.SiteMenuInboxRepository;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.domain.model.menu.SiteMenu;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SiteMenuInboxRepositoryAdapter implements SiteMenuInboxRepository {

    private final MongoSiteMenuRepository mongoSiteMenuRepository;
    private final MongoSiteMenuAdapter mongoSiteMenuAdapter;

    @Override
    public Optional<SiteMenuInbox> findBySiteMenuId(SiteMenuId siteMenuId) {
        Assert.notNull(siteMenuId, "Error retrieving inbox; site menu id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readInbox(siteMenuId);
        }
        else {
            Optional<MongoSiteMenu> oMongoSiteMenu = mongoSiteMenuRepository.findById(siteMenuId.id());
            return oMongoSiteMenu.map(mongoSiteMenuAdapter::reconstituteInbox);
        }
    }

    @Override
    public void add(SiteMenuInbox inbox) {
        Assert.notNull(inbox, "Error adding inbox to repository; inbox cannot be null");
        MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createInbox(inbox);
    }

    @Override
    public void remove(SiteMenuInbox inbox) {
        Assert.notNull(inbox, "Error removing inbox from repository; inbox cannot be null");
        MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteInbox(inbox.aggregateId());
    }
}
