package info.earty.content.infrastructure.mongo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document
public abstract class Document {
    @Id
    @EqualsAndHashCode.Include
    private String id;

    //Optimistic locking version
    @Version
    private Long version;

    private boolean hasAggregate;
    private boolean hasOutbox;
    private boolean hasInbox;
}
