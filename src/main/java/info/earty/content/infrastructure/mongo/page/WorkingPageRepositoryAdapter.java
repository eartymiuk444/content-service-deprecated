package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.domain.model.page.WorkingPage;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.domain.model.page.WorkingPageRepository;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WorkingPageRepositoryAdapter implements WorkingPageRepository {

    private final MongoWorkingPageRepository mongoWorkingPageRepository;
    private final MongoWorkingPageAdapter mongoWorkingPageAdapter;

    @Override
    public Optional<WorkingPage> findById(WorkingPageId workingPageId) {
        Assert.notNull(workingPageId, "Error finding working page; working page id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readAggregate(workingPageId);
        }
        else {
            Optional<MongoWorkingPage> oMongoWorkingPage = mongoWorkingPageRepository.findById(workingPageId.id());
            return oMongoWorkingPage.map(mongoWorkingPageAdapter::reconstituteAggregate);
        }
    }

    @Override
    public void add(WorkingPage workingPage) {
        Assert.notNull(workingPage, "Error adding working page to repository; working page cannot be null");
        MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createAggregate(workingPage);
    }

    @Override
    public void remove(WorkingPage workingPage) {
        Assert.notNull(workingPage, "Error removing working page from repository; working page cannot be null");
        MongoUnitOfWork<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteAggregate(workingPage.id());
    }

    @Override
    public boolean existsById(WorkingPageId workingPageId) {
        return this.findById(workingPageId).isPresent();
    }
}
