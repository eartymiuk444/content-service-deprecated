package info.earty.content.infrastructure.mongo;

import info.earty.content.application.common.ApplicationServiceLifeCycle;
import info.earty.content.application.event.card.WorkingCardOutboxRepository;
import info.earty.content.application.event.menu.SiteMenuOutboxRepository;
import info.earty.content.application.event.page.WorkingPageOutboxRepository;
import info.earty.content.domain.model.common.Aggregate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MongoApplicationServiceLifeCycle extends ApplicationServiceLifeCycle {

    private final Map<Class<Aggregate>, AggregateAdapterGroup> aggregateAdapterGroups;

    public MongoApplicationServiceLifeCycle(SiteMenuOutboxRepository siteMenuOutboxRepository,
                                            WorkingPageOutboxRepository workingPageOutboxRepository,
                                            WorkingCardOutboxRepository workingCardOutboxRepository,
                                            List<AggregateAdapterGroup> aggregateAdapterGroups) {
        super(siteMenuOutboxRepository, workingPageOutboxRepository, workingCardOutboxRepository);

        this.aggregateAdapterGroups = new HashMap<>();
        aggregateAdapterGroups.forEach(aggregateAdapterGroup -> {
            this.aggregateAdapterGroups.put(aggregateAdapterGroup.aggregateTypeGroup().aggregateType(), aggregateAdapterGroup);
        });
    }

    public <A extends Aggregate> void begin(Class<A> aggregateType) {
        MongoUnitOfWork.start(this.aggregateAdapterGroups.get(aggregateType));
    }

    public void success() {
        MongoUnitOfWork.current().commit();
    }

    public void fail() {
        MongoUnitOfWork.current().rollback();
    }

    public void fail(RuntimeException anException) {
        this.fail();
        throw anException;
    }

    public void fail(Throwable aThrowable) throws Throwable {
        this.fail();
        throw aThrowable;
    }
}
