package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuStoredEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoSiteMenuRepository extends MongoRepository<MongoSiteMenu, String> {
    List<MongoSiteMenu> findByEventQueueNot(List<SiteMenuStoredEvent> eventQueue);
}
