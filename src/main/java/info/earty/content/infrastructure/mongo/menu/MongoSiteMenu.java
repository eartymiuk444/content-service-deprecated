package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuStoredEvent;
import info.earty.content.application.event.menu.SiteMenuWorkingPageInbox;
import info.earty.content.domain.model.menu.Directory;
import info.earty.content.domain.model.menu.DirectoryId;
import info.earty.content.domain.model.menu.DirectoryItem;
import info.earty.content.infrastructure.mongo.Document;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document("siteMenu")
public class MongoSiteMenu extends Document {
    private DirectoryId rootDirectoryId;
    private Map<Integer, Directory> allDirectories;
    private Map<String, DirectoryItem> allPages;
    private Map<Integer, DirectoryId> directoryParents;
    private Map<String, DirectoryId> pageParents;
    private int nextDirectoryIdInt;

    //Outbox or Event store
    private int nextEventId;
    private List<SiteMenuStoredEvent> eventQueue;

    //Inbox or Processed event store
    private Map<String, SiteMenuWorkingPageInbox> workingPageInboxes;
}
