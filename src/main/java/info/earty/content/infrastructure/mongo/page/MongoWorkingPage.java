package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageSiteMenuInbox;
import info.earty.content.application.event.page.WorkingPageStoredEvent;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.infrastructure.mongo.Document;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document("workingPage")
public class MongoWorkingPage extends Document {
    //Aggregate state
    private SiteMenuId siteMenuId;
    private MongoPublishedPage publishedPage;
    private MongoDraft draft;
    private Instant lastPublished;
    private int nextDraftIdInt;

    //Outbox
    private int nextEventId;
    private List<WorkingPageStoredEvent> eventQueue;

    //Inbox or Processed event store
    private Map<String, WorkingPageSiteMenuInbox> siteMenuInboxes;
}
