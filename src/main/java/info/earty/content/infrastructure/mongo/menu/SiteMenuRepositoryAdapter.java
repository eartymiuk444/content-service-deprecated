package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuInbox;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.domain.model.menu.SiteMenu;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.menu.SiteMenuRepository;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SiteMenuRepositoryAdapter implements SiteMenuRepository {

    private final MongoSiteMenuRepository mongoSiteMenuRepository;
    private final MongoSiteMenuAdapter mongoSiteMenuAdapter;

    @Override
    public Optional<SiteMenu> findById(SiteMenuId siteMenuId) {
        Assert.notNull(siteMenuId, "Error finding site menu; site menu id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readAggregate(siteMenuId);
        }
        else {
            Optional<MongoSiteMenu> oMongoSiteMenu = mongoSiteMenuRepository.findById(siteMenuId.id());
            return oMongoSiteMenu.map(mongoSiteMenuAdapter::reconstituteAggregate);
        }
    }

    @Override
    public void add(SiteMenu siteMenu) {
        Assert.notNull(siteMenu, "Error adding site menu to repository; site menu cannot be null");
        MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createAggregate(siteMenu);
    }

    @Override
    public void remove(SiteMenu siteMenu) {
        Assert.notNull(siteMenu, "Error removing site menu from repository; site menu cannot be null");
        MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteAggregate(siteMenu.id());
    }
}
