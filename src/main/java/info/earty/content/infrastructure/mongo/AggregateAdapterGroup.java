package info.earty.content.infrastructure.mongo;

import info.earty.content.application.event.Inbox;
import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.repository.MongoRepository;

@Value
@Accessors(fluent = true)
class AggregateAdapterGroup<ID extends AggregateId, A extends Aggregate<ID>,
        O extends Outbox<ID>, I extends Inbox<ID>, D extends Document> {

    AggregateTypeGroup<ID, A, O, I, D> aggregateTypeGroup;

    AggregateAdapter<ID, A, O, I, D> aggregateAdapter;
    MongoDocumentAdapter<D, ID, A, O, I> documentAdapter;
    MongoRepository<D, String> documentRepository;
}
