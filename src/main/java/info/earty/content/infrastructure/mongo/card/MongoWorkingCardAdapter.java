package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.card.WorkingCardWorkingPageInbox;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.MongoDocumentAdapter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.earty.content.infrastructure.reflection.ReflectionHelpers.*;

@Component
public class MongoWorkingCardAdapter implements
        MongoDocumentAdapter<MongoWorkingCard, WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox> {

    @Override
    public WorkingCard reconstituteAggregate(MongoWorkingCard document) {
        Assert.notNull(document, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(document.isHasAggregate(), "Error reconstituting aggregate; the document does not contain an aggregate instance");

        WorkingCardId workingCardId = new WorkingCardId(document.getId());

        accessibleConstructor(WorkingCard.class, WorkingCardId.class, WorkingPageId.class);
        WorkingCard workingCard = newInstance(accessibleConstructor(WorkingCard.class, WorkingCardId.class, WorkingPageId.class),
                workingCardId, document.getWorkingPageId());

        setField(workingCard, "publishedCard", document.getPublishedCard());
        setField(workingCard, "draftCard", document.getDraftCard());
        setField(workingCard, "nextDraftCardIdInt", document.getNextDraftCardIdInt());

        return workingCard;
    }

    @Override
    public WorkingCardOutbox reconstituteOutbox(MongoWorkingCard document) {
        Assert.notNull(document, "Error reconstituting outbox; document cannot be null");
        Assert.isTrue(document.isHasOutbox(), "Error reconstituting outbox; the document does not contain an aggregate instance");

        return newInstance(accessibleConstructor(WorkingCardOutbox.class, WorkingCardId.class, int.class, List.class),
                new WorkingCardId(document.getId()), document.getNextEventId(), document.getEventQueue());
    }

    @Override
    public WorkingCardInbox reconstituteInbox(MongoWorkingCard document) {
        Assert.notNull(document, "Error reconstituting inbox; document cannot be null");
        Assert.isTrue(document.isHasInbox(), "Error reconstituting inbox; the document does not contain an aggregate instance");

        Map<String, WorkingCardWorkingPageInbox> mongoWorkingCardWorkingPageInboxes = document.getWorkingPageInboxes();
        Map<WorkingPageId, WorkingCardWorkingPageInbox> workingCardWorkingPageInboxes = new HashMap<>();

        mongoWorkingCardWorkingPageInboxes.forEach((k,v) -> workingCardWorkingPageInboxes.put(new WorkingPageId(k), v));

        return newInstance(accessibleConstructor(WorkingCardInbox.class, WorkingCardId.class, Map.class),
                new WorkingCardId(document.getId()), workingCardWorkingPageInboxes);
    }
}
