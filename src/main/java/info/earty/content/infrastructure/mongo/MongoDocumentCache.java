package info.earty.content.infrastructure.mongo;

import info.earty.content.application.event.Inbox;
import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Value
@Accessors(fluent = true)
class MongoDocumentCache<D extends Document, ID extends AggregateId, A extends Aggregate<ID>,
        O extends Outbox<ID>, I extends Inbox<ID>> {

    String key;
    D document;
    A aggregate;
    O outbox;
    I inbox;

    static <D extends Document, ID extends AggregateId, A extends Aggregate<ID>,
            O extends Outbox<ID>, I extends Inbox<ID>>
    MongoDocumentCache<D, ID, A, O, I>
    createEmptyDocumentCache(String key) {
        Assert.notNull(key, "Error creating empty document cache; key cannot be null");
        return new MongoDocumentCache<>(key, null, null, null, null);
    }

    static <D extends Document, ID extends AggregateId, A extends Aggregate<ID>,
            O extends Outbox<ID>, I extends Inbox<ID>>
    MongoDocumentCache<D, ID, A, O, I>
    createWithDocument(String key, D document, MongoDocumentAdapter<D, ID, A, O, I> adapter) {
        Assert.notNull(key, "Error creating document cache; key cannot be null");
        Assert.notNull(document, "Error creating document cache; document cannot be null");
        return new MongoDocumentCache<>(key, document,
                document.isHasAggregate() ? adapter.reconstituteAggregate(document) : null,
                document.isHasOutbox() ? adapter.reconstituteOutbox(document) : null,
                document.isHasInbox() ?  adapter.reconstituteInbox(document) : null);
    }

    MongoDocumentCache<D, ID, A, O, I> withAggregate(String key, A aggregate) {
        Assert.isTrue(this.keyCached(key), "Error adding aggregate to document cache;" +
                "this cache is for a different key");
        Assert.isTrue(!this.hasAggregate(key), "Error adding aggregate to document cache;" +
                "the cache already contains an aggregate");
        return new MongoDocumentCache<>(key, this.document, aggregate, this.outbox, this.inbox);
    }

    MongoDocumentCache<D, ID, A, O, I> withoutAggregate(String key) {
        Assert.isTrue(this.keyCached(key), "Error removing aggregate from document cache;" +
                "this cache is for a different key");
        Assert.isTrue(this.hasAggregate(key), "Error removing aggregate from document cache;" +
                "the cache doesn't contain an aggregate");
        return new MongoDocumentCache<>(key, this.document, null, this.outbox, this.inbox);
    }

    MongoDocumentCache<D, ID, A, O, I> withOutbox(String key, O outbox) {
        Assert.isTrue(this.keyCached(key), "Error adding aggregate to document cache;" +
                "this cache is for a different key");
        Assert.isTrue(!this.hasOutbox(key), "Error adding outbox to document cache;" +
                "the cache already contains an outbox");
        return new MongoDocumentCache<>(key, this.document, this.aggregate, outbox, this.inbox);
    }


    MongoDocumentCache<D, ID, A, O, I> withoutOutbox(String key) {
        Assert.isTrue(this.keyCached(key), "Error removing outbox from document cache;" +
                "this cache is for a different key");
        Assert.isTrue(this.hasOutbox(key), "Error removing outbox from document cache;" +
                "the cache doesn't contain an outbox");
        return new MongoDocumentCache<>(key, this.document, this.aggregate, null, this.inbox);
    }

    MongoDocumentCache<D, ID, A, O, I> withInbox(String key, I inbox) {
        Assert.isTrue(this.keyCached(key), "Error adding aggregate to document cache;" +
                "this cache is for a different key");
        Assert.isTrue(!this.hasInbox(key), "Error adding inbox to document cache;" +
                "the cache already contains an inbox");
        return new MongoDocumentCache<>(key, this.document, this.aggregate, this.outbox, inbox);
    }

    MongoDocumentCache<D, ID, A, O, I> withoutInbox(String key) {
        Assert.isTrue(this.keyCached(key), "Error removing inbox from document cache;" +
                "this cache is for a different key");
        Assert.isTrue(this.hasInbox(key), "Error removing inbox from document cache;" +
                "the cache doesn't contain an outbox");
        return new MongoDocumentCache<>(key, this.document, this.aggregate, this.outbox, null);
    }

    String key() {
        return this.key;
    }

    D cachedDocument() {
        return this.document;
    }

    D adaptedDocument(AggregateAdapter<ID, A, O, I, D> aggregateAdapter) {
        return aggregateAdapter.adaptToDocument(this.aggregate, this.outbox, this.inbox, this.document);
    }

    boolean keyCached(String key) {
        return this.key().equals(key);
    }

    boolean emptyDocumentCached(String key) {
        return this.keyCached(key) && this.document == null;
    }

    boolean documentCached(String key) {
        return this.keyCached(key) && this.document != null;
    }

    boolean hasAggregate(String key) {
        return this.keyCached(key) && this.aggregate != null;
    }

    boolean hasOutbox(String key) {
        return this.keyCached(key) && this.outbox != null;
    }

    boolean hasInbox(String key) {
        return this.keyCached(key) && this.inbox != null;
    }

    boolean hasAggregateOutboxOrInbox(String key) {
        return this.hasAggregate(key) || this.hasOutbox(key) || this.hasInbox(key);
    }

    A aggregate(String key) {
        Assert.isTrue(this.hasAggregate(key),
                "Error retrieving aggregate; the cache does not contain an aggregate for the key");
        return this.aggregate;
    }

    O outbox(String key) {
        Assert.isTrue(this.hasOutbox(key),
                "Error retrieving outbox; the cache does not contain an outbox for the key");
        return this.outbox;
    }

    I inbox(String key) {
        Assert.isTrue(this.hasInbox(key),
                "Error retrieving aggregate; the cache does not contain an inbox for the key");
        return this.inbox;
    }
}
