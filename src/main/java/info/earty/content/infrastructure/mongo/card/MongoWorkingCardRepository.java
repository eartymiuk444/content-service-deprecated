package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardStoredEvent;
import info.earty.content.domain.model.page.WorkingPageId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoWorkingCardRepository extends MongoRepository<MongoWorkingCard, String> {
    List<MongoWorkingCard> findByEventQueueNot(List<WorkingCardStoredEvent> eventQueue);
    List<MongoWorkingCard> findByWorkingPageId(WorkingPageId workingPageId);
}
