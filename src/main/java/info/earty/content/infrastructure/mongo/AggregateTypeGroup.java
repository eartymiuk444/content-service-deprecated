package info.earty.content.infrastructure.mongo;

import info.earty.content.application.event.Inbox;
import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@Accessors(fluent = true)
public class AggregateTypeGroup<ID extends AggregateId, A extends Aggregate<ID>, O extends Outbox<ID>,
        I extends Inbox<ID>, D extends Document> {
    Class<ID> aggregateIdType;
    Class<A> aggregateType;
    Class<O> outboxType;
    Class<I> inboxType;
    Class<D> documentType;
}
