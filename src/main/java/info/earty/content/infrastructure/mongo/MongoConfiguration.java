package info.earty.content.infrastructure.mongo;

import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.menu.SiteMenuInbox;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.menu.SiteMenu;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.page.WorkingPage;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.card.MongoWorkingCard;
import info.earty.content.infrastructure.mongo.card.MongoWorkingCardAdapter;
import info.earty.content.infrastructure.mongo.card.MongoWorkingCardRepository;
import info.earty.content.infrastructure.mongo.card.WorkingCardAdapter;
import info.earty.content.infrastructure.mongo.menu.MongoSiteMenu;
import info.earty.content.infrastructure.mongo.menu.MongoSiteMenuAdapter;
import info.earty.content.infrastructure.mongo.menu.MongoSiteMenuRepository;
import info.earty.content.infrastructure.mongo.menu.SiteMenuAdapter;
import info.earty.content.infrastructure.mongo.page.MongoWorkingPage;
import info.earty.content.infrastructure.mongo.page.MongoWorkingPageAdapter;
import info.earty.content.infrastructure.mongo.page.MongoWorkingPageRepository;
import info.earty.content.infrastructure.mongo.page.WorkingPageAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class MongoConfiguration {

    @Value("${earty.info.content.mongo.host}")
    private String mongoHost;

    @Value("${earty.info.content.mongo.port}")
    private String mongoPort;

    @Value("${earty.info.content.mongo.username}")
    private String mongoUsername;

    @Value("${earty.info.content.mongo.secret}")
    private String mongoSecret;

    @Bean
    public MongoDatabaseFactory mongoDatabaseFactory() {
        return new SimpleMongoClientDatabaseFactory(mongoClient(), "content");
    }

    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create(String.format("mongodb://%s:%s@%s:%s", mongoUsername, mongoSecret, mongoHost, mongoPort));
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        MongoTemplate template = new MongoTemplate(mongoDatabaseFactory());//, mappingMongoConverter());
        template.setWriteConcern(WriteConcern.ACKNOWLEDGED);
        return template;
    }

//    @Bean
//    public MappingMongoConverter mappingMongoConverter() {
//        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDatabaseFactory());
//        MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
//        mongoConverter.setMapKeyDotReplacement("-DOT");
//        return mongoConverter;
//    }

    @Bean
    public AggregateAdapterGroup<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu> siteMenuAdapters(
            SiteMenuAdapter aggregateAdapter,
            MongoSiteMenuAdapter documentAdapter,
            MongoSiteMenuRepository documentRepository) {
        AggregateTypeGroup<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu> typeGroup =
                new AggregateTypeGroup<>(SiteMenuId.class, SiteMenu.class, SiteMenuOutbox.class, SiteMenuInbox.class, MongoSiteMenu.class);
        return new AggregateAdapterGroup<>(typeGroup, aggregateAdapter, documentAdapter, documentRepository);
    }

    @Bean
    public AggregateAdapterGroup<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage> workingPageAdapters(
            WorkingPageAdapter aggregateAdapter,
            MongoWorkingPageAdapter documentAdapter,
            MongoWorkingPageRepository documentRepository) {
        AggregateTypeGroup<WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox, MongoWorkingPage> typeGroup =
                new AggregateTypeGroup<>(WorkingPageId.class, WorkingPage.class, WorkingPageOutbox.class, WorkingPageInbox.class, MongoWorkingPage.class);
        return new AggregateAdapterGroup<>(typeGroup, aggregateAdapter, documentAdapter, documentRepository);
    }

    @Bean
    public AggregateAdapterGroup<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard> workingCardAdapters(
            WorkingCardAdapter aggregateAdapter,
            MongoWorkingCardAdapter documentAdapter,
            MongoWorkingCardRepository documentRepository) {
        AggregateTypeGroup<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard> typeGroup =
                new AggregateTypeGroup<>(WorkingCardId.class, WorkingCard.class, WorkingCardOutbox.class, WorkingCardInbox.class, MongoWorkingCard.class);
        return new AggregateAdapterGroup<>(typeGroup, aggregateAdapter, documentAdapter, documentRepository);
    }

}
