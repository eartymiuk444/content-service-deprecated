package info.earty.content.infrastructure.mongo.page;

import info.earty.content.domain.model.page.PublishedOutlineItem;
import info.earty.content.domain.model.page.Title;
import lombok.Data;

import java.util.Map;

@Data
public class MongoPublishedPage {
    Title title;
    Map<String, PublishedOutlineItem> allOutlineItems;
}
