package info.earty.content.infrastructure.mongo;

import info.earty.content.application.event.Inbox;
import info.earty.content.application.event.Outbox;
import info.earty.content.domain.model.common.Aggregate;
import info.earty.content.domain.model.common.AggregateId;
import org.springframework.util.Assert;

import java.util.Optional;

public class MongoUnitOfWork<ID extends AggregateId, A extends Aggregate<ID>,
        O extends Outbox<ID>, I extends Inbox<ID>, D extends Document> {

    private static final ThreadLocal<MongoUnitOfWork> THREAD_UNIT_OF_WORK = new ThreadLocal<>();

    private MongoDocumentCache<D, ID, A, O, I> cache;
    private final AggregateAdapterGroup<ID, A, O, I, D> aggregateAdapterGroup;

    public static <ID extends AggregateId, A extends Aggregate<ID>, O extends Outbox<ID>, I extends Inbox<ID>, D extends Document>
    MongoUnitOfWork<ID, A, O, I, D> start(AggregateAdapterGroup<ID, A, O, I, D> aggregateAdapterGroup) {
        MongoUnitOfWork<ID, A, O, I, D> unitOfWork = THREAD_UNIT_OF_WORK.get();
        if (unitOfWork == null) {
            unitOfWork = new MongoUnitOfWork<>(aggregateAdapterGroup);
            THREAD_UNIT_OF_WORK.set(unitOfWork);
        }
        return unitOfWork;
    }

    public static <ID extends AggregateId, A extends Aggregate<ID>, O extends Outbox<ID>, I extends Inbox<ID>, D extends Document>
    MongoUnitOfWork<ID, A, O, I, D> current() {
        MongoUnitOfWork<ID, A, O, I, D> unitOfWork = THREAD_UNIT_OF_WORK.get();
        Assert.notNull(unitOfWork, "Error retrieving current unit of work; no unit of work has been started");
        return unitOfWork;
    }

    public static boolean exists() {
        return THREAD_UNIT_OF_WORK.get() != null;
    }

    public void createAggregate(A aggregate) {
        Assert.notNull(aggregate, "Error creating aggregate; aggregate cannot be null");
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateType().equals(aggregate.getClass()),
                "Error creating aggregate; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregate.id());

        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error creating aggregate; can only work with a single document per unit of work");

        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        cache = cache.withAggregate(key, aggregate);
    }

    public Optional<A> readAggregate(ID aggregateId) {
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateIdType().equals(aggregateId.getClass()),
                "Error reading aggregate; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregateId);
        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error reading aggregate; can only read a single document per unit of work");
        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        return cache.hasAggregate(cache.key()) ? Optional.of(cache.aggregate(cache.key())) : Optional.empty();
    }

    public void deleteAggregate(ID aggregateId) {
        Assert.notNull(aggregateId, "Error deleting aggregate; aggregate id cannot be null");
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateIdType().equals(aggregateId.getClass()),
                "Error deleting aggregate; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregateId);

        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error deleting aggregate; can only work with a single document per unit of work");

        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        cache = cache.withoutAggregate(key);
    }

    public void createOutbox(O outbox) {
        Assert.notNull(outbox, "Error creating outbox; outbox cannot be null");
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().outboxType().equals(outbox.getClass()),
                "Error creating outbox; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(outbox.aggregateId());

        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error creating outbox; can only work with a single document per unit of work");

        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        cache = cache.withOutbox(key, outbox);
    }

    public Optional<O> readOutbox(ID aggregateId) {
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateIdType().equals(aggregateId.getClass()),
                "Error reading outbox; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregateId);
        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error reading outbox; can only read a single document per unit of work");
        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        return cache.hasOutbox(cache.key()) ? Optional.of(cache.outbox(cache.key())) : Optional.empty();
    }

    public void deleteOutbox(ID aggregateId) {
        Assert.notNull(aggregateId, "Error deleting outbox; aggregate id cannot be null");
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateIdType().equals(aggregateId.getClass()),
                "Error deleting outbox; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregateId);

        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error deleting outbox; can only work with a single document per unit of work");

        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        cache = cache.withoutOutbox(key);
    }

    public void createInbox(I inbox) {
        Assert.notNull(inbox, "Error creating inbox; inbox cannot be null");
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().inboxType().equals(inbox.getClass()),
                "Error creating inbox; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(inbox.aggregateId());

        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error creating inbox; can only work with a single document per unit of work");

        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        cache = cache.withInbox(key, inbox);
    }

    public Optional<I> readInbox(ID aggregateId) {
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateIdType().equals(aggregateId.getClass()),
                "Error reading inbox; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregateId);
        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error reading inbox; can only read a single document per unit of work");
        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        return cache.hasInbox(cache.key()) ? Optional.of(cache.inbox(cache.key())) : Optional.empty();
    }

    public void deleteInbox(ID aggregateId) {
        Assert.notNull(aggregateId, "Error deleting inbox; aggregate id cannot be null");
        Assert.isTrue(this.aggregateAdapterGroup.aggregateTypeGroup().aggregateIdType().equals(aggregateId.getClass()),
                "Error deleting inbox; incompatible type for the current unit of work");
        String key = this.aggregateAdapterGroup.aggregateAdapter().adaptIdToString(aggregateId);

        Assert.isTrue(this.cacheEmpty() || cache.keyCached(key),
                "Error deleting inbox; can only work with a single document per unit of work");

        if (this.cacheEmpty()) {
            this.initCache(key);
        }
        cache = cache.withoutInbox(key);
    }

    public void commit() {
        if (this.cachePopulated()) {
            if (cache.hasAggregateOutboxOrInbox(cache.key())) {
                if (cache.emptyDocumentCached(cache.key())) {
                    this.aggregateAdapterGroup.documentRepository()
                            .insert(cache.adaptedDocument(this.aggregateAdapterGroup.aggregateAdapter()));
                } else {
                    //TODO EA 8/30/2021 - Update to only do the save if something actually changed.
                    this.aggregateAdapterGroup.documentRepository()
                            .save(cache.adaptedDocument(this.aggregateAdapterGroup.aggregateAdapter()));
                }
            } else {
                if (cache.documentCached(cache.key())) {
                    this.aggregateAdapterGroup.documentRepository()
                            .delete(cache.cachedDocument());
                }
            }
        }
        THREAD_UNIT_OF_WORK.set(null);
    }

    public void rollback() {
        THREAD_UNIT_OF_WORK.set(null);
    }

    private MongoUnitOfWork(AggregateAdapterGroup<ID, A, O, I, D> aggregateAdapterGroup) {
        this.aggregateAdapterGroup = aggregateAdapterGroup;
        this.cache = null;
    }

    private boolean cacheEmpty() {
        return this.cache == null;
    }

    private boolean cachePopulated() {
        return this.cache != null;
    }

    private void initCache(String key) {
        Optional<D> oDocument = this.aggregateAdapterGroup.documentRepository().findById(key);
        cache = oDocument.map(document -> MongoDocumentCache.createWithDocument(key, document,
                this.aggregateAdapterGroup.documentAdapter())).orElseGet(
                () -> MongoDocumentCache.createEmptyDocumentCache(key));
    }
}
