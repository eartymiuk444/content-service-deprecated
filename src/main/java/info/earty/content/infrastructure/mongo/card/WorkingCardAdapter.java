package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.application.event.card.WorkingCardWorkingPageInbox;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.AggregateAdapter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.earty.content.infrastructure.reflection.ReflectionHelpers.getField;

@Component
public class WorkingCardAdapter extends AggregateAdapter<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard> {

    @Override
    public String adaptIdToString(WorkingCardId aggregateId) {
        return aggregateId.id();
    }

    @Override
    protected void clearAggregateInternal(MongoWorkingCard document) {
        document.setHasAggregate(false);
        document.setWorkingPageId(null);
        document.setPublishedCard(null);
        document.setDraftCard(null);
        document.setNextDraftCardIdInt(0);
    }

    @Override
    protected void setAggregateInternal(WorkingCard aggregate, MongoWorkingCard document) {
        //domain provides read access
        document.setWorkingPageId(aggregate.workingPageId());
        document.setPublishedCard(aggregate.publishedCard());
        document.setDraftCard(aggregate.draftCard());

        //domain restricts read access
        document.setNextDraftCardIdInt(getField(aggregate, "nextDraftCardIdInt", Integer.class));
    }

    @Override
    protected void clearOutboxInternal(MongoWorkingCard document) {
        document.setEventQueue(null);
        document.setNextEventId(0);
    }

    @Override
    protected void setOutboxInternal(WorkingCardOutbox outbox, MongoWorkingCard document) {
        document.setEventQueue(getField(outbox, "eventQueue", List.class));
        document.setNextEventId(getField(outbox, "nextEventId", Integer.class));
    }

    @Override
    protected void clearInboxInternal(MongoWorkingCard document) {
        document.setWorkingPageInboxes(null);
    }

    @Override
    protected void setInboxInternal(WorkingCardInbox inbox, MongoWorkingCard document) {
        Map<WorkingPageId, WorkingCardWorkingPageInbox> workingCardWorkingPageInboxes = getField(inbox, "workingPageInboxes", Map.class);
        Map<String, WorkingCardWorkingPageInbox> mongoWorkingCardWorkingPageInboxes = new HashMap<>();
        workingCardWorkingPageInboxes.forEach((k, v) -> mongoWorkingCardWorkingPageInboxes.put(k.id(), v));
        document.setWorkingPageInboxes(mongoWorkingCardWorkingPageInboxes);
    }

    @Override
    protected MongoWorkingCard newDocumentInstance() {
        return new MongoWorkingCard();
    }
}
