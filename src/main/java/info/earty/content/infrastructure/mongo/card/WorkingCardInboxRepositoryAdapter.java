package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardInbox;
import info.earty.content.application.event.card.WorkingCardInboxRepository;
import info.earty.content.application.event.card.WorkingCardOutbox;
import info.earty.content.domain.model.card.WorkingCard;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WorkingCardInboxRepositoryAdapter implements WorkingCardInboxRepository {

    private final MongoWorkingCardRepository mongoWorkingCardRepository;
    private final MongoWorkingCardAdapter mongoWorkingCardAdapter;

    @Override
    public Optional<WorkingCardInbox> findByWorkingCardId(WorkingCardId workingCardId) {
        Assert.notNull(workingCardId, "Error retrieving inbox; working card id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readInbox(workingCardId);
        }
        else {
            Optional<MongoWorkingCard> oMongoWorkingCard = mongoWorkingCardRepository.findById(workingCardId.id());
            return oMongoWorkingCard.map(mongoWorkingCardAdapter::reconstituteInbox);
        }
    }

    @Override
    public void add(WorkingCardInbox inbox) {
        Assert.notNull(inbox, "Error adding inbox to repository; inbox cannot be null");
        MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createInbox(inbox);
    }

    @Override
    public void remove(WorkingCardInbox inbox) {
        Assert.notNull(inbox, "Error removing inbox from repository; inbox cannot be null");
        MongoUnitOfWork<WorkingCardId, WorkingCard, WorkingCardOutbox, WorkingCardInbox, MongoWorkingCard>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteInbox(inbox.aggregateId());
    }

}
