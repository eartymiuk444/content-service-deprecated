package info.earty.content.infrastructure.mongo.menu;

import info.earty.content.application.event.menu.SiteMenuInbox;
import info.earty.content.application.event.menu.SiteMenuOutbox;
import info.earty.content.application.event.menu.SiteMenuOutboxRepository;
import info.earty.content.domain.model.menu.SiteMenu;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.infrastructure.mongo.MongoUnitOfWork;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class SiteMenuOutboxRepositoryAdapter implements SiteMenuOutboxRepository {

    private final MongoSiteMenuRepository mongoSiteMenuRepository;
    private final MongoSiteMenuAdapter mongoSiteMenuAdapter;

    @Override
    public Optional<SiteMenuOutbox> findBySiteMenuId(SiteMenuId siteMenuId) {
        Assert.notNull(siteMenuId, "Error retrieving outbox; site menu id cannot be null");
        if (MongoUnitOfWork.exists()) {
            MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                    unitOfWork = MongoUnitOfWork.current();
            return unitOfWork.readOutbox(siteMenuId);
        }
        else {
            Optional<MongoSiteMenu> oMongoSiteMenu = mongoSiteMenuRepository.findById(siteMenuId.id());
            return oMongoSiteMenu.map(mongoSiteMenuAdapter::reconstituteOutbox);
        }
    }

    @Override
    public void add(SiteMenuOutbox outbox) {
        Assert.notNull(outbox, "Error adding outbox to repository; outbox cannot be null");
        MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.createOutbox(outbox);
    }

    @Override
    public void remove(SiteMenuOutbox outbox) {
        Assert.notNull(outbox, "Error removing outbox from repository; outbox cannot be null");
        MongoUnitOfWork<SiteMenuId, SiteMenu, SiteMenuOutbox, SiteMenuInbox, MongoSiteMenu>
                unitOfWork = MongoUnitOfWork.current();
        unitOfWork.deleteOutbox(outbox.aggregateId());
    }

    @Override
    public Collection<SiteMenuOutbox> findByEventQueueNotEmpty() {
        return mongoSiteMenuRepository.findByEventQueueNot(new ArrayList<>())
                .stream().map(mongoSiteMenuAdapter::reconstituteOutbox).collect(Collectors.toSet());
    }
}
