package info.earty.content.infrastructure.mongo.card;

import info.earty.content.application.event.card.WorkingCardStoredEvent;
import info.earty.content.application.event.card.WorkingCardWorkingPageInbox;
import info.earty.content.domain.model.card.DraftCard;
import info.earty.content.domain.model.card.PublishedCard;
import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.infrastructure.mongo.Document;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@org.springframework.data.mongodb.core.mapping.Document("workingCard")
public class MongoWorkingCard extends Document {

    //Aggregate state
    private WorkingPageId workingPageId;
    private PublishedCard publishedCard;
    private DraftCard draftCard;
    private int nextDraftCardIdInt;

    //Outbox or Event store
    private int nextEventId;
    private List<WorkingCardStoredEvent> eventQueue;

    //Inbox or Processed event store
    private Map<String, WorkingCardWorkingPageInbox> workingPageInboxes;
}
