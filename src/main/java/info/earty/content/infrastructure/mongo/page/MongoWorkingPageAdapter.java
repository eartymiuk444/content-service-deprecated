package info.earty.content.infrastructure.mongo.page;

import info.earty.content.application.event.page.WorkingPageInbox;
import info.earty.content.application.event.page.WorkingPageOutbox;
import info.earty.content.application.event.page.WorkingPageSiteMenuInbox;
import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.menu.SiteMenuId;
import info.earty.content.domain.model.page.*;
import info.earty.content.infrastructure.mongo.MongoDocumentAdapter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.earty.content.infrastructure.reflection.ReflectionHelpers.*;

@Component
public class MongoWorkingPageAdapter implements MongoDocumentAdapter<MongoWorkingPage, WorkingPageId, WorkingPage, WorkingPageOutbox, WorkingPageInbox> {

    private static final String ROOT_DRAFT_OUTLINE_ITEM_ID = "ROOT_DRAFT_OUTLINE_ITEM_ID";

    @Override
    public WorkingPage reconstituteAggregate(MongoWorkingPage document) {
        Assert.notNull(document, "Error reconstituting aggregate; document cannot be null");
        Assert.isTrue(document.isHasAggregate(), "Error reconstituting aggregate; the document does not contain an aggregate instance");

        WorkingPage workingPage = newInstance(accessibleConstructor(WorkingPage.class, WorkingPageId.class, SiteMenuId.class),
                new WorkingPageId(document.getId()), document.getSiteMenuId());

        setField(workingPage, "publishedPage", reconstitutePublishedPage(document.getPublishedPage()));
        setField(workingPage, "draft", reconstituteDraft(document.getDraft()));
        setField(workingPage, "lastPublished", document.getLastPublished());
        setField(workingPage, "nextDraftIdInt", document.getNextDraftIdInt());

        return workingPage;
    }

    @Override
    public WorkingPageOutbox reconstituteOutbox(MongoWorkingPage document) {
        Assert.notNull(document, "Error reconstituting outbox; document cannot be null");
        Assert.isTrue(document.isHasOutbox(), "Error reconstituting outbox; the document does not contain an outbox");
        return newInstance(accessibleConstructor(WorkingPageOutbox.class, WorkingPageId.class, int.class, List.class),
                new WorkingPageId(document.getId()), document.getNextEventId(), document.getEventQueue());
    }

    @Override
    public WorkingPageInbox reconstituteInbox(MongoWorkingPage document) {
        Assert.notNull(document, "Error reconstituting inbox; document cannot be null");
        Assert.isTrue(document.isHasInbox(), "Error reconstituting inbox; the document does not contain an inbox");

        Map<String, WorkingPageSiteMenuInbox> mongoWorkingPageSiteMenuInboxes = document.getSiteMenuInboxes();
        Map<SiteMenuId, WorkingPageSiteMenuInbox> workingPageSiteMenuInboxes = new HashMap<>();

        mongoWorkingPageSiteMenuInboxes.forEach((k,v) -> workingPageSiteMenuInboxes.put(new SiteMenuId(k), v));

        return newInstance(accessibleConstructor(WorkingPageInbox.class, WorkingPageId.class, Map.class),
                new WorkingPageId(document.getId()), workingPageSiteMenuInboxes);
    }

    private PublishedPage reconstitutePublishedPage(MongoPublishedPage mongoPublishedPage) {

        Map<DraftOutlineItemId, PublishedOutlineItem> allOutlineItems = new HashMap<>();
        mongoPublishedPage.getAllOutlineItems().forEach((draftOutlineItemIdString, publishedOutlineItem) -> {
            DraftOutlineItemId draftOutlineItemId = draftOutlineItemIdString.equals(ROOT_DRAFT_OUTLINE_ITEM_ID) ? DraftOutlineItemId.createRootId() :
                    DraftOutlineItemId.create(new WorkingCardId(draftOutlineItemIdString));
            allOutlineItems.put(draftOutlineItemId, publishedOutlineItem);
        });

        return newInstance(accessibleConstructor(PublishedPage.class, Title.class, Map.class),
                mongoPublishedPage.getTitle(), allOutlineItems);
    }

    private Draft reconstituteDraft(MongoDraft mongoDraft) {

        Map<DraftOutlineItemId, DraftOutlineItem> allOutlineItems = new HashMap<>();
        mongoDraft.getAllOutlineItems().forEach((draftOutlineItemIdString, draftOutlineItem) -> {
            DraftOutlineItemId draftOutlineItemId = draftOutlineItemIdString.equals(ROOT_DRAFT_OUTLINE_ITEM_ID) ? DraftOutlineItemId.createRootId() :
                    DraftOutlineItemId.create(new WorkingCardId(draftOutlineItemIdString));
            allOutlineItems.put(draftOutlineItemId, draftOutlineItem);
        });

        Map<DraftOutlineItemId, DraftOutlineItemId> parentOutlineItems = new HashMap<>();
        mongoDraft.getParentOutlineItems().forEach((draftOutlineItemIdString, parentId) -> {
            DraftOutlineItemId draftOutlineItemId = DraftOutlineItemId.create(new WorkingCardId(draftOutlineItemIdString));
            parentOutlineItems.put(draftOutlineItemId, parentId);
        });

        Draft draft = newInstance(accessibleConstructor(Draft.class, DraftId.class, Map.class, Map.class),
                mongoDraft.getId(), allOutlineItems, parentOutlineItems);
        setField(draft, "title", mongoDraft.getTitle());
        return draft;
    }
}
