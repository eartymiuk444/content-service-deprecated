package info.earty.content.infrastructure.mongo.page;

import info.earty.content.domain.model.page.DraftId;
import info.earty.content.domain.model.page.DraftOutlineItem;
import info.earty.content.domain.model.page.DraftOutlineItemId;
import info.earty.content.domain.model.page.Title;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MongoDraft {
    @EqualsAndHashCode.Include
    private DraftId id;
    private Title title;
    private Map<String, DraftOutlineItem> allOutlineItems;
    private Map<String, DraftOutlineItemId> parentOutlineItems;
}
