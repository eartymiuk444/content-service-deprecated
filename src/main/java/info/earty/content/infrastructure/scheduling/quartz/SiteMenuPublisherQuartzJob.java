package info.earty.content.infrastructure.scheduling.quartz;

import info.earty.content.application.event.NotificationService;
import lombok.RequiredArgsConstructor;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@DisallowConcurrentExecution
public class SiteMenuPublisherQuartzJob implements Job {
    private final NotificationService notificationService;

    @Override
    public void execute(JobExecutionContext context) {
        notificationService.publishSiteMenuNotifications();
    }
}
