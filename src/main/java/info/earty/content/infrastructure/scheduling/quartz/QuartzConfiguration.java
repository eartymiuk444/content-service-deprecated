package info.earty.content.infrastructure.scheduling.quartz;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfiguration {

    @Bean
    public JobDetail siteMenuPublisherJobDetail() {
        return JobBuilder.newJob().ofType(SiteMenuPublisherQuartzJob.class)
                .storeDurably()
                .withIdentity("SiteMenuPublisher")
                .build();
    }

    @Bean
    public Trigger siteMenuPublisherTrigger(@Qualifier("siteMenuPublisherJobDetail") JobDetail job) {
        return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity("SiteMenuPublisherTrigger")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().repeatForever().withIntervalInSeconds(1))
                .build();
    }

    @Bean
    public JobDetail workingPagePublisherJobDetail() {
        return JobBuilder.newJob().ofType(WorkingPagePublisherQuartzJob.class)
                .storeDurably()
                .withIdentity("WorkingPagePublisher")
                .build();
    }

    @Bean
    public Trigger workingPagePublisherTrigger(@Qualifier("workingPagePublisherJobDetail") JobDetail job) {
        return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity("WorkingPagePublisherTrigger")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().repeatForever().withIntervalInSeconds(1))
                .build();
    }

    @Bean
    public JobDetail workingCardPublisherJobDetail() {
        return JobBuilder.newJob().ofType(WorkingCardPublisherQuartzJob.class)
                .storeDurably()
                .withIdentity("WorkingCardPublisher")
                .build();
    }

    @Bean
    public Trigger workingCardPublisherTrigger(@Qualifier("workingCardPublisherJobDetail") JobDetail job) {
        return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity("WorkingCardPublisherTrigger")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().repeatForever().withIntervalInSeconds(1))
                .build();
    }

//PART 2/3 TO CONFIGURE QUARTZ JDBC USING MYSQL
//    @Bean
//    @QuartzDataSource
//    public DataSource dataSource() {
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
//        dataSourceBuilder.url("jdbc:mysql://localhost:3306/db_example");
//        dataSourceBuilder.username("admin");
//        dataSourceBuilder.password("admin");
//        return dataSourceBuilder.build();
//    }

}
