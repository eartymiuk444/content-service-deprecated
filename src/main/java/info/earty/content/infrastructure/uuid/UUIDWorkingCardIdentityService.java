package info.earty.content.infrastructure.uuid;

import info.earty.content.domain.model.card.WorkingCardId;
import info.earty.content.domain.model.card.WorkingCardIdentityService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDWorkingCardIdentityService implements WorkingCardIdentityService {
    @Override
    public WorkingCardId generate() {
        return new WorkingCardId(UUID.randomUUID().toString());
    }
}
