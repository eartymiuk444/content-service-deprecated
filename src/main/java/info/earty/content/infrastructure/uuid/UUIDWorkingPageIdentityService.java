package info.earty.content.infrastructure.uuid;

import info.earty.content.domain.model.page.WorkingPageId;
import info.earty.content.domain.model.page.WorkingPageIdentityService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDWorkingPageIdentityService implements WorkingPageIdentityService {
    @Override
    public WorkingPageId generate() {
        return new WorkingPageId(UUID.randomUUID().toString());
    }
}
