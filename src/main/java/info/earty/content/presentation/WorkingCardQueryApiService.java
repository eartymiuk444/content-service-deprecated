package info.earty.content.presentation;

import info.earty.content.application.WorkingCardQueryService;
import info.earty.content.application.data.DraftCardDto;
import info.earty.content.application.data.PublishedCardDto;
import info.earty.content.presentation.data.DraftCardJsonDto;
import info.earty.content.presentation.data.PublishedCardJsonDto;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WorkingCardQueryApiService implements WorkingCardQueryApi {

    private final WorkingCardQueryService workingCardQueryService;
    private final DtoMapper dtoMapper;

    @Override
    public DraftCardJsonDto getDraftCard(String workingCardId) {
        return dtoMapper.map(workingCardQueryService.getDraftCard(workingCardId));
    }

    @Override
    public PublishedCardJsonDto getPublishedCard(String workingCardId) {
        return dtoMapper.map(workingCardQueryService.getPublishedCard(workingCardId));
    }

    @Override
    public Set<DraftCardJsonDto> getDraftCardsByWorkingPageId(String workingPageId) {
        return workingCardQueryService.getDraftCardsByWorkingPageId(workingPageId).stream()
                .map(dtoMapper::map).collect(Collectors.toSet());
    }

    @Override
    public Set<PublishedCardJsonDto> getPublishedCardsByWorkingPageId(String workingPageId) {
        return workingCardQueryService.getPublishedCardsByWorkingPageId(workingPageId).stream()
                .map(dtoMapper::map).collect(Collectors.toSet());
    }

    @Mapper(componentModel = "spring")
    interface DtoMapper {
        DraftCardJsonDto map(DraftCardDto dto);
        PublishedCardJsonDto map(PublishedCardDto dto);
    }
}
