package info.earty.content.presentation;

import info.earty.content.application.WorkingPageCommandService;
import info.earty.content.application.command.page.*;
import info.earty.content.presentation.command.page.*;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WorkingPageCommandApiService implements WorkingPageCommandApi {

    private final JsonCommandMapper jsonCommandMapper;
    private final WorkingPageCommandService workingPageCommandService;

    @Override
    public void addOutlineItem(AddOutlineItemJsonCommand jsonCommand) {
        workingPageCommandService.addOutlineItem(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void changeOutlineItemFragment(ChangeOutlineItemFragmentJsonCommand jsonCommand) {
        workingPageCommandService.changeOutlineItemFragment(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removeOutlineItem(RemoveOutlineItemJsonCommand jsonCommand) {
        workingPageCommandService.removeOutlineItem(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void moveOutlineItem(MoveOutlineItemJsonCommand jsonCommand) {
        workingPageCommandService.moveOutlineItem(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void moveOutlineSubItemUp(MoveOutlineSubItemUpJsonCommand jsonCommand) {
        workingPageCommandService.moveOutlineSubItemUp(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void moveOutlineSubItemDown(MoveOutlineSubItemDownJsonCommand jsonCommand) {
        workingPageCommandService.moveOutlineSubItemDown(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void discardDraft(DiscardDraftJsonCommand jsonCommand) {
        workingPageCommandService.discardDraft(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void publish(PublishJsonCommand jsonCommand) {
        workingPageCommandService.publish(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void changeTitle(ChangeTitleJsonCommand jsonCommand) {
        workingPageCommandService.changeTitle(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void changeOutlineItemTitle(ChangeOutlineItemTitleJsonCommand jsonCommand) {
        workingPageCommandService.changeOutlineItemTitle(jsonCommandMapper.map(jsonCommand));
    }

    @Mapper(componentModel = "spring")
    interface JsonCommandMapper {
        AddOutlineItemCommand map(AddOutlineItemJsonCommand jsonCommand);
        ChangeOutlineItemFragmentCommand map(ChangeOutlineItemFragmentJsonCommand jsonCommand);
        MoveOutlineItemCommand map(MoveOutlineItemJsonCommand jsonCommand);
        MoveOutlineSubItemUpCommand map(MoveOutlineSubItemUpJsonCommand jsonCommand);
        MoveOutlineSubItemDownCommand map(MoveOutlineSubItemDownJsonCommand jsonCommand);
        DiscardDraftCommand map(DiscardDraftJsonCommand jsonCommand);
        PublishCommand map(PublishJsonCommand jsonCommand);
        ChangeTitleCommand map(ChangeTitleJsonCommand jsonCommand);
        ChangeOutlineItemTitleCommand map(ChangeOutlineItemTitleJsonCommand jsonCommand);
        RemoveOutlineItemCommand map(RemoveOutlineItemJsonCommand jsonCommand);
    }
}
