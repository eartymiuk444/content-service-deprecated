package info.earty.content.presentation;

import info.earty.content.application.SiteMenuQueryService;
import info.earty.content.application.data.SiteMenuDto;
import info.earty.content.presentation.data.SiteMenuJsonDto;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SiteMenuQueryApiService implements SiteMenuQueryApi {

    private final SiteMenuQueryService siteMenuQueryService;
    private final SiteMenuDtoMapper siteMenuDtoMapper;

    @Override
    public SiteMenuJsonDto get() {
        return siteMenuDtoMapper.map(siteMenuQueryService.get());
    }

    @Mapper(componentModel = "spring")
    interface SiteMenuDtoMapper {
        SiteMenuJsonDto map(SiteMenuDto siteMenuDto);
        SiteMenuJsonDto.DirectoryDto map(SiteMenuDto.DirectoryDto directoryDto);
        SiteMenuJsonDto.SubDirectoryDto map(SiteMenuDto.SubDirectoryDto subDirectoryDto);
        SiteMenuJsonDto.PageDto map(SiteMenuDto.PageDto pageDto);
        default SiteMenuJsonDto.DirectoryItemDto map(SiteMenuDto.DirectoryItemDto directoryItemDto) {
            return directoryItemDto instanceof SiteMenuDto.PageDto ?
                    map((SiteMenuDto.PageDto)directoryItemDto) : map((SiteMenuDto.SubDirectoryDto)directoryItemDto);
        }
    }
}
