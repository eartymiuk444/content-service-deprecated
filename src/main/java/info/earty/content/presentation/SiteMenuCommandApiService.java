package info.earty.content.presentation;

import info.earty.content.application.SiteMenuCommandService;
import info.earty.content.application.command.menu.*;
import info.earty.content.presentation.command.menu.*;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SiteMenuCommandApiService implements SiteMenuCommandApi {

    private final SiteMenuCommandService siteMenuCommandService;
    private final JsonCommandMapper jsonCommandMapper;

    @Override
    public void create() {
        siteMenuCommandService.create();
    }

    @Override
    public void addDirectory(AddDirectoryJsonCommand jsonCommand) {
        siteMenuCommandService.addDirectory(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void moveDirectoryUp(MoveDirectoryUpJsonCommand jsonCommand) {
        siteMenuCommandService.moveDirectoryUp(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void moveDirectoryDown(MoveDirectoryDownJsonCommand jsonCommand) {
        siteMenuCommandService.moveDirectoryDown(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void moveDirectory(MoveDirectoryJsonCommand jsonCommand) {
        siteMenuCommandService.moveDirectory(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void addPage(AddPageJsonCommand jsonCommand) {
        siteMenuCommandService.addPage(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void movePageUp(MovePageUpJsonCommand jsonCommand) {
        siteMenuCommandService.movePageUp(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void movePageDown(MovePageDownJsonCommand jsonCommand) {
        siteMenuCommandService.movePageDown(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void movePage(MovePageJsonCommand jsonCommand) {
        siteMenuCommandService.movePage(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removeDirectory(RemoveDirectoryJsonCommand jsonCommand) {
        siteMenuCommandService.removeDirectory(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removePage(RemovePageJsonCommand jsonCommand) {
        siteMenuCommandService.removePage(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void editDirectory(EditDirectoryJsonCommand jsonCommand) {
        siteMenuCommandService.editDirectory(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void changePagePathSegment(ChangePagePathSegmentJsonCommand jsonCommand) {
        siteMenuCommandService.changePagePathSegment(jsonCommandMapper.map(jsonCommand));
    }

    @Mapper(componentModel = "spring")
    interface JsonCommandMapper {
        AddDirectoryCommand map(AddDirectoryJsonCommand jsonCommand);
        MoveDirectoryUpCommand map(MoveDirectoryUpJsonCommand jsonCommand);
        MoveDirectoryDownCommand map(MoveDirectoryDownJsonCommand jsonCommand);
        MoveDirectoryCommand map(MoveDirectoryJsonCommand jsonCommand);
        AddPageCommand map(AddPageJsonCommand jsonCommand);
        MovePageUpCommand map(MovePageUpJsonCommand jsonCommand);
        MovePageDownCommand map(MovePageDownJsonCommand jsonCommand);
        MovePageCommand map(MovePageJsonCommand jsonCommand);
        RemoveDirectoryCommand map(RemoveDirectoryJsonCommand jsonCommand);
        RemovePageCommand map(RemovePageJsonCommand jsonCommand);
        EditDirectoryCommand map(EditDirectoryJsonCommand jsonCommand);
        ChangePagePathSegmentCommand map(ChangePagePathSegmentJsonCommand jsonCommand);
    }
}
