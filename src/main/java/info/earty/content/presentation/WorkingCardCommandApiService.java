package info.earty.content.presentation;

import info.earty.content.application.WorkingCardCommandService;
import info.earty.content.application.command.card.ChangeTextCommand;
import info.earty.content.application.command.card.RemoveAttachmentCommand;
import info.earty.content.application.command.card.RemoveImageCommand;
import info.earty.content.presentation.command.card.ChangeTextJsonCommand;
import info.earty.content.presentation.data.RemoveAttachmentJsonCommand;
import info.earty.content.presentation.data.RemoveImageJsonCommand;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WorkingCardCommandApiService implements WorkingCardCommandApi {

    private final WorkingCardCommandService workingCardCommandService;
    private final JsonCommandMapper jsonCommandMapper;

    @Override
    public void changeText(ChangeTextJsonCommand jsonCommand) {
        workingCardCommandService.changeText(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removeImage(RemoveImageJsonCommand jsonCommand) {
        workingCardCommandService.removeImage(jsonCommandMapper.map(jsonCommand));
    }

    @Override
    public void removeAttachment(RemoveAttachmentJsonCommand jsonCommand) {
        workingCardCommandService.removeAttachment(jsonCommandMapper.map(jsonCommand));
    }

    @Mapper(componentModel = "spring")
    interface JsonCommandMapper {
        ChangeTextCommand map(ChangeTextJsonCommand jsonCommand);
        RemoveImageCommand map(RemoveImageJsonCommand jsonCommand);
        RemoveAttachmentCommand map(RemoveAttachmentJsonCommand jsonCommand);
    }
}
