package info.earty.content.domain.model.page;

import info.earty.content.domain.model.menu.SiteMenuId;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class WorkingPageFactoryTests {

    private final WorkingPageFactory workingPageFactory = new WorkingPageFactory();

    @Test
    void createPage() {
        WorkingPage workingPage = workingPageFactory.create(new SiteMenuId("menu"), new WorkingPageId("page"), Title.create("Working Page"));
        assertNotNull(workingPage);
        assertEquals(workingPage.siteMenuId().id(), "menu");
        assertEquals(workingPage.id().id(), "page");
        assertNotNull(workingPage.publishedPage());
        assertNotNull(workingPage.draft());
        assertEquals(workingPage.publishedPage().title().titleString(), "Working Page");
    }

}
