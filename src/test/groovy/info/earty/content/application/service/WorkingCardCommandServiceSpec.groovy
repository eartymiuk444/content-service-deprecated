package info.earty.content.application.service

import info.earty.content.application.WorkingCardCommandService
import info.earty.content.application.command.card.DiscardCommand
import info.earty.content.application.command.card.PublishCommand
import info.earty.content.application.common.ApplicationServiceLifeCycle
import info.earty.content.application.event.card.WorkingCardInbox
import info.earty.content.application.event.card.WorkingCardInboxRepository
import info.earty.content.application.event.card.WorkingCardOutboxRepository
import info.earty.content.domain.model.card.WorkingCard
import info.earty.content.domain.model.card.WorkingCardFactory
import info.earty.content.domain.model.card.WorkingCardId
import info.earty.content.domain.model.card.WorkingCardRepository
import info.earty.content.domain.model.page.WorkingPageId
import spock.lang.Specification

class WorkingCardCommandServiceSpec extends Specification {

    def workingCardFactoryCollaborator = new WorkingCardFactory()
    
    def workingCardRepositoryCollaborator = Mock(WorkingCardRepository)
    def workingCardOutboxRepositoryCollaborater = Mock(WorkingCardOutboxRepository)
    def workingCardInboxRepositoryCollaborater = Mock(WorkingCardInboxRepository)
    def applicationServiceLifeCycleCollaborator = Mock(ApplicationServiceLifeCycle)

    def workingCardCommandService = new WorkingCardCommandService(workingCardFactoryCollaborator,
            workingCardRepositoryCollaborator, workingCardOutboxRepositoryCollaborater,
            workingCardInboxRepositoryCollaborater, applicationServiceLifeCycleCollaborator)

    def "discard cards command"() {
        given: "a working page id, working card ids"
        WorkingPageId workingPageId = new WorkingPageId("page")

        WorkingCardId discardCardId1 = new WorkingCardId("discard card 1")
        WorkingCardId discardCardId2 = new WorkingCardId("discard card 2")

        WorkingCardId removeCardId1 = new WorkingCardId("remove card 1");

        and: "a discard command and the event id that triggered that command"
        int triggeringEventId = 1
        DiscardCommand discardCommand = new DiscardCommand()
        discardCommand.setEventId(triggeringEventId)
        discardCommand.setWorkingPageId(workingPageId.id())
        discardCommand.setDiscardWorkingCardIds([discardCardId1.id(), discardCardId2.id()] as Set)
        discardCommand.setRemoveWorkingCardIds([removeCardId1.id()] as Set)

        and: "working cards corresponding with the discard card ids with draft changes"
        String originalTitle = "title"
        WorkingCard discardCard1 = new WorkingCardFactory().create(discardCardId1, workingPageId, originalTitle)
        discardCard1.draftCard().changeText("draft text")
        discardCard1.draftCard().changeTitle("draft title")
        workingCardRepositoryCollaborator.findById(discardCardId1) >> Optional.of(discardCard1)

        WorkingCard discardCard2 = new WorkingCardFactory().create(discardCardId2, workingPageId, originalTitle)
        discardCard2.draftCard().changeText("draft text")
        discardCard2.draftCard().changeTitle("draft title")
        workingCardRepositoryCollaborator.findById(discardCardId2) >> Optional.of(discardCard2)

        and: "working cards corresponding with the remove card ids"
        WorkingCard removeCard1 = new WorkingCardFactory().create(removeCardId1, workingPageId, "")
        workingCardRepositoryCollaborator.findById(removeCardId1) >> Optional.of(removeCard1)

        and: "empty inboxes for the cards being discarded"
        WorkingCardInbox discardCardInbox1 = WorkingCardInbox.create(discardCardId1)
        workingCardInboxRepositoryCollaborater.findByWorkingCardId(discardCardId1) >> Optional.of(discardCardInbox1)

        WorkingCardInbox discardCardInbox2 = WorkingCardInbox.create(discardCardId2)
        workingCardInboxRepositoryCollaborater.findByWorkingCardId(discardCardId2) >> Optional.of(discardCardInbox2)

        when:
        workingCardCommandService.discard(discardCommand)

        then: "the appropriate cards have their drafts discarded"
        discardCard1.draftCard().title() == originalTitle
        discardCard1.draftCard().text() == ""
        discardCard2.draftCard().title() == originalTitle
        discardCard2.draftCard().text() == ""

        and: "the appropriate inboxes have the event triggering the discard command marked as processed"
        discardCardInbox1.duplicateEvent(workingPageId, 1)
        discardCardInbox2.duplicateEvent(workingPageId, 1)

        and: "the appropriate cards are removed"
        1 * workingCardRepositoryCollaborator.remove(removeCard1)
    }

    def "publish cards command"() {
        given: "a working page id, working card ids"
        WorkingPageId workingPageId = new WorkingPageId("page")

        WorkingCardId publishCardId1 = new WorkingCardId("publish card 1")
        WorkingCardId publishCardId2 = new WorkingCardId("publish card 2")

        WorkingCardId removeCardId1 = new WorkingCardId("remove card 1");

        and: "a publish command and the event id that triggered that command"
        int triggeringEventId = 1
        PublishCommand publishCommand = new PublishCommand()
        publishCommand.setEventId(triggeringEventId)
        publishCommand.setWorkingPageId(workingPageId.id())
        publishCommand.setPublishWorkingCardIds([publishCardId1.id(), publishCardId2.id()] as Set)
        publishCommand.setRemoveWorkingCardIds([removeCardId1.id()] as Set)

        and: "working cards corresponding with the publish card ids with draft changes"
        String draftTitle = "draft title"
        String draftText = "draft text"

        WorkingCard publishCard1 = new WorkingCardFactory().create(publishCardId1, workingPageId, "title")
        publishCard1.draftCard().changeText(draftText)
        publishCard1.draftCard().changeTitle(draftTitle)
        workingCardRepositoryCollaborator.findById(publishCardId1) >> Optional.of(publishCard1)

        WorkingCard publishCard2 = new WorkingCardFactory().create(publishCardId2, workingPageId, "title")
        publishCard2.draftCard().changeText(draftText)
        publishCard2.draftCard().changeTitle(draftTitle)
        workingCardRepositoryCollaborator.findById(publishCardId2) >> Optional.of(publishCard2)

        and: "working cards corresponding with the remove card ids"
        WorkingCard removeCard1 = new WorkingCardFactory().create(removeCardId1, workingPageId, "")
        workingCardRepositoryCollaborator.findById(removeCardId1) >> Optional.of(removeCard1)

        and: "empty inboxes for the cards being published"
        WorkingCardInbox publishCardInbox1 = WorkingCardInbox.create(publishCardId1)
        workingCardInboxRepositoryCollaborater.findByWorkingCardId(publishCardId1) >> Optional.of(publishCardInbox1)

        WorkingCardInbox publishCardInbox2 = WorkingCardInbox.create(publishCardId2)
        workingCardInboxRepositoryCollaborater.findByWorkingCardId(publishCardId2) >> Optional.of(publishCardInbox2)

        when:
        workingCardCommandService.publish(publishCommand)

        then: "the appropriate cards have their drafts published"
        publishCard1.draftCard().title() == draftTitle
        publishCard1.draftCard().text() == draftText
        publishCard2.draftCard().title() == draftTitle
        publishCard2.draftCard().text() == draftText

        and: "the appropriate inboxes have the event triggering the publish command marked as processed"
        publishCardInbox1.duplicateEvent(workingPageId, 1)
        publishCardInbox2.duplicateEvent(workingPageId, 1)

        and: "the appropriate cards are removed"
        1 * workingCardRepositoryCollaborator.remove(removeCard1)
    }
}
