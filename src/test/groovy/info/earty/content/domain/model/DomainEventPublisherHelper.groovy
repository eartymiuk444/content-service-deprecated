package info.earty.content.domain.model

import info.earty.content.domain.model.common.DomainEvent
import info.earty.content.domain.model.common.DomainEventPublisher
import info.earty.content.domain.model.common.DomainEventSubscriber

class DomainEventPublisherHelper {

    def static subscribeAndCaptureAll() {
        List<DomainEvent> domainEvents = new ArrayList<>()
        DomainEventPublisher.instance().reset()
        DomainEventPublisher.instance().subscribe(new DomainEventSubscriber<DomainEvent>() {
            @Override
            void handleEvent(DomainEvent aDomainEvent) {
                domainEvents.add(aDomainEvent)
            }
            @Override
            Class<DomainEvent> subscribedToEventType() {
                return DomainEvent.class
            }
        })
        return domainEvents
    }

}
