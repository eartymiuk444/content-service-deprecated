package info.earty.content.domain.model.page

import info.earty.content.domain.model.menu.SiteMenuId
import spock.lang.Specification

class WorkingPageFactorySpec extends Specification {

    private final static WorkingPageFactory workingPageFactory = new WorkingPageFactory();

    def "creating a working page with the factory"() {
        given: "a site menu id, a working page id and a title"
        SiteMenuId siteMenuId = new SiteMenuId("menu")
        WorkingPageId workingPageId = new WorkingPageId("1")
        String title = "Working Page"

        when: "a page is created with the id and title"
        WorkingPage workingPage = workingPageFactory.create(siteMenuId, workingPageId, Title.create(title))

        then: "a working page is created with a published page, a draft page, and that title"
        workingPage.publishedPage().title().titleString() == title
        workingPage.draft().title().titleString() == title
    }

}
