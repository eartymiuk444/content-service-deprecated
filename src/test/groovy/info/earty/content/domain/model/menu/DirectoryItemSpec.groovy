package info.earty.content.domain.model.menu


import info.earty.content.domain.model.page.WorkingPageFactory
import info.earty.content.domain.model.page.WorkingPageId
import spock.lang.Specification

class DirectoryItemSpec extends Specification{

    def siteMenuFactoryCollaborator = new SiteMenuFactory()
    def workingPageFactoryCollaborator = new WorkingPageFactory()

    def "not equals"() {
        given: "a site menu with two directories with the same names and path segments"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        PathSegment pathSegment = new PathSegment("directory1")

        siteMenu.addDirectory(DirectoryItemName.create("Directory1"), pathSegment, siteMenu.rootDirectory().id())
        DirectoryId directoryId1 = siteMenu.rootDirectory().items().get(0).directoryId()

        siteMenu.addDirectory(DirectoryItemName.create("Directory1"), pathSegment, directoryId1)

        DirectoryItem directoryItem1 = siteMenu.rootDirectory().items().get(0)
        DirectoryItem directoryItem2 = siteMenu.findDirectory(directoryId1).get().items().get(0)

        expect: "that the two directory items are not equal"
        directoryItem1 != directoryItem2
    }

    def "a page is not equal to a directory"() {
        given: "a site menu with a directory and a page with the same name and path segment"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        PathSegment pathSegment = new PathSegment("path1")

        siteMenu.addDirectory(DirectoryItemName.create("DirectoryItem1"), pathSegment, siteMenu.rootDirectory().id())
        DirectoryId directoryId1 = siteMenu.rootDirectory().items().get(0).directoryId()

        siteMenu.addPage(new WorkingPageId("1"), DirectoryItemName.create("DirectoryItem1"), pathSegment, directoryId1)

        DirectoryItem directoryItem1 = siteMenu.rootDirectory().items().get(0)
        DirectoryItem directoryItem2 = siteMenu.findDirectory(directoryId1).get().items().get(0)

        expect: "that the page is not equal to the directory"
        directoryItem1 != directoryItem2
    }

}
