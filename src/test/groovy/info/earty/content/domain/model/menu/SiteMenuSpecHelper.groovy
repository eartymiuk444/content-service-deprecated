package info.earty.content.domain.model.menu

class SiteMenuSpecHelper {

    def static printMenu(SiteMenu siteMenu) {
        System.out.println("Printing Site Menu for Test Clarity")
        Directory rootDirectory = siteMenu.rootDirectory()
        rootDirectory.items().forEach(item -> {
            System.out.println(item)
            if (item.isSubDirectory()) {
                Directory directory = siteMenu.findDirectory(item.directoryId()).get()
                printMenuHelper(siteMenu, directory, 1)
            }
        })
        System.out.println()
    }

    def static printMenuHelper(SiteMenu siteMenu, Directory directory, int indent) {
        directory.items().forEach(directoryItem -> {
            System.out.println("\t".repeat(indent) + directoryItem)
            if (directoryItem.isSubDirectory()) {
                Directory childDirectory = siteMenu.findDirectory(directoryItem.directoryId()).get()
                printMenuHelper(siteMenu, childDirectory, indent + 1)
            }
        })
    }

}
