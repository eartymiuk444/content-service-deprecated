package info.earty.content.domain.model.page

import info.earty.content.domain.model.card.WorkingCard
import info.earty.content.domain.model.card.WorkingCardId
import info.earty.content.domain.model.menu.SiteMenuId

class WorkingPageSpecHelper {

    static WorkingPage workingPageWithDraftWithMultiLayerSubItemsForTest(WorkingPageFactory workingPageFactory,
                                                                         int numLayers, int outlineItemsPerLayer) {
        WorkingPage workingPage = workingPageFactory.create(new SiteMenuId("menu"), new WorkingPageId("1"), Title.create("Working Page"))

        int currId = 0
        for (int i = 0; i < outlineItemsPerLayer; i++) {

            WorkingCardId cardId = new WorkingCardId(Integer.toString(currId))
            String cardTitle = "Card " + currId

            workingPage.addOutlineItemToRoot(cardId, cardTitle)
            currId++

            if (numLayers > 1) {
                currId = addOutlineLayersToPageDraft( workingPage, cardId, outlineItemsPerLayer, currId, numLayers - 1)
            }
        }

        return workingPage
    }

    static int addOutlineLayersToPageDraft(WorkingPage workingPage, WorkingCardId parentCardId,
                                    int numOutlineItems, int currIdInt, int numSubLayers) {
        for (int i = 0; i < numOutlineItems; i++) {
            WorkingCardId cardId = new WorkingCardId(Integer.toString(currIdInt))
            String cardTitle = "Card " + currIdInt
            workingPage.addOutlineItemToRoot(cardId, cardTitle)
            workingPage.draft().moveOutlineItemToParent(cardId, parentCardId)

            currIdInt++
            if (numSubLayers > 1) {
                currIdInt = addOutlineLayersToPageDraft(workingPage, cardId, numOutlineItems, currIdInt, numSubLayers - 1)
            }
        }

        return currIdInt
    }

    static int addOutlineLayersToPageDraft(List<WorkingCard> workingCards, WorkingPage workingPage, WorkingCardId parentCardId,
                                           int numOutlineItems, int currIdInt, int numSubLayers) {
        for (int i = 0; i < numOutlineItems; i++) {
            WorkingCard card = workingCards.get(currIdInt)
            workingPage.addOutlineItemToRoot(card.id(), card.draftCard().title())
            workingPage.draft().moveOutlineItemToParent(card.id(), parentCardId)

            currIdInt++
            if (numSubLayers > 1) {
                currIdInt = addOutlineLayersToPageDraft(workingCards, workingPage, card.id(), numOutlineItems, currIdInt, numSubLayers - 1)
            }
        }

        return currIdInt
    }

    def static printDraft(WorkingPage workingPage) {
        System.out.println("Printing Page Draft for Test Clarity")
        System.out.println("Page Draft Title: " + workingPage.draft().title())
        Draft draft = workingPage.draft()
        draft.rootOutlineItem().subItems().forEach(rootOutlineSubItem -> {
            System.out.println(rootOutlineSubItem)
            DraftOutlineItem rootOutlineItem = draft.findOutlineItem(rootOutlineSubItem.workingCardId()).get()
            printDraftHelper(draft, rootOutlineItem, 1)
        })
        System.out.println()
    }

    def static printDraftHelper(Draft draft, DraftOutlineItem draftOutlineItem, int indent) {
        draftOutlineItem.subItems().forEach(subItem -> {
            System.out.println("\t".repeat(indent) + subItem)
            DraftOutlineItem outlineItem = draft.findOutlineItem(subItem.workingCardId()).get()
            printDraftHelper(draft, outlineItem, indent + 1)
        })
    }

    def static printPublished(WorkingPage workingPage) {
        System.out.println("Printing Published Page for Test Clarity")
        System.out.println("Published Page Title: " + workingPage.publishedPage().title())
        PublishedPage publishedPage = workingPage.publishedPage()
        publishedPage.rootOutlineItem().subItems().forEach(rootOutlineSubItem -> {
            System.out.println(rootOutlineSubItem)
            PublishedOutlineItem rootOutlineItem = publishedPage.findOutlineItem(rootOutlineSubItem.workingCardId()).get()
            printPublishedHelper(publishedPage, rootOutlineItem, 1)
        })
        System.out.println()
    }

    def static printPublishedHelper(PublishedPage publishedPage, PublishedOutlineItem publishedOutlineItem, int indent) {
        publishedOutlineItem.subItems().forEach(subItem -> {
            System.out.println("\t".repeat(indent) + subItem)
            PublishedOutlineItem outlineItem = publishedPage.findOutlineItem(subItem.workingCardId()).get()
            printPublishedHelper(publishedPage, outlineItem, indent + 1)
        })
    }

}
