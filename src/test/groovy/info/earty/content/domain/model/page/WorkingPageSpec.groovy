package info.earty.content.domain.model.page

import info.earty.content.domain.model.DomainEventPublisherHelper
import info.earty.content.domain.model.card.WorkingCardId
import info.earty.content.domain.model.common.DomainEvent
import spock.lang.Specification

import java.util.stream.Collectors

import static info.earty.content.domain.model.page.WorkingPageSpecHelper.*

class WorkingPageSpec extends Specification {

    def workingPageFactoryCollaborator = new WorkingPageFactory()

    def "publish a working page's draft"() {
        given: "an empty working page with a non-trivial draft"
        WorkingPage workingPage = workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 2, 2)
        workingPage.draft().addFragmentToOutlineItem(new WorkingCardId("1"), new Fragment("aFragment"))
        printPublished(workingPage)
        printDraft(workingPage)

        when: "the working page is published"
        workingPage.publish()
        printPublished(workingPage)

        then: "the published page matches the non-trivial draft"
        PublishedPage publishedPage = workingPage.publishedPage()
        Draft draft = workingPage.draft()
        List<PublishedOutlineSubItem> rootPublishedOutlineSubItems = publishedPage.rootOutlineItem().subItems()
        List<DraftOutlineSubItem> rootDraftOutlineSubItems = draft.rootOutlineItem().subItems()

        assert rootPublishedOutlineSubItems.size() == rootDraftOutlineSubItems.size()

        for (int i = 0; i < rootPublishedOutlineSubItems.size(); i++) {
            PublishedOutlineSubItem publishedOutlineSubItem = rootPublishedOutlineSubItems.get(i)
            DraftOutlineSubItem draftOutlineSubItem = rootDraftOutlineSubItems.get(i)
            assert publishedOutlineSubItem.workingCardId() ==
                    draftOutlineSubItem.workingCardId()
            assert publishedOutlineSubItem.title() ==
                    draftOutlineSubItem.title()
            assert (!publishedOutlineSubItem.hasFragment() && !draftOutlineSubItem.hasFragment()) ||
                    (publishedOutlineSubItem.fragment() == draftOutlineSubItem.fragment())
            verifyNonRootOutlineItem(publishedPage, draft,
                    publishedPage.findOutlineItem(publishedOutlineSubItem.workingCardId()).get(),
                    draft.findOutlineItem(draftOutlineSubItem.workingCardId()).get())
        }
    }

    def static verifyNonRootOutlineItem(PublishedPage publishedPage, Draft draft, PublishedOutlineItem publishedOutlineItem, DraftOutlineItem draftOutlineItem) {
        List<PublishedOutlineSubItem> nonRootPublishedOutlineSubItems = publishedOutlineItem.subItems()
        List<DraftOutlineSubItem> nonRootDraftOutlineSubItems = draftOutlineItem.subItems()

        assert nonRootPublishedOutlineSubItems.size() == nonRootDraftOutlineSubItems.size()

        for (int i = 0; i < nonRootPublishedOutlineSubItems.size(); i++) {
            PublishedOutlineSubItem publishedOutlineSubItem = nonRootPublishedOutlineSubItems.get(i)
            DraftOutlineSubItem draftOutlineSubItem = nonRootDraftOutlineSubItems.get(i)
            assert publishedOutlineSubItem.workingCardId() ==
                    draftOutlineSubItem.workingCardId()
            assert publishedOutlineSubItem.title() ==
                    draftOutlineSubItem.title()
            assert (!publishedOutlineSubItem.hasFragment() && !draftOutlineSubItem.hasFragment()) ||
                    (publishedOutlineSubItem.fragment() == draftOutlineSubItem.fragment())

            verifyNonRootOutlineItem(publishedPage, draft,
                    publishedPage.findOutlineItem(publishedOutlineSubItem.workingCardId()).get(),
                    draft.findOutlineItem(draftOutlineSubItem.workingCardId()).get())
        }
    }

    def "discard a working page's draft"() {
        given: "a non-trivial working page with a draft that differs from its published page"
        WorkingPage workingPage = workingPageWithDraftWithMultiLayerSubItemsForTest(
                this.workingPageFactoryCollaborator, 2, 2)
        workingPage.publish()
        printPublished(workingPage)

        PublishedPage originalPublishedPage = workingPage.publishedPage()
        workingPage.removeOutlineItem(new WorkingCardId("0"))
        printDraft(workingPage)

        and: "a domain event publisher and generic subscriber"
        List<DomainEvent> domainEvents = DomainEventPublisherHelper.subscribeAndCaptureAll()

        when: "the draft is discarded and the working page is published"
        workingPage.discardDraft()
        workingPage.publish()
        printPublished(workingPage)

        then: "the new draft matches the original published page"
        workingPage.publishedPage() == originalPublishedPage

        and: "a DraftDiscarded domain event is published with the expected current card ids and published card ids removed"
        domainEvents.size() == 2
        domainEvents.get(0).getClass() == DraftDiscarded.class
        DraftDiscarded draftDiscarded = (DraftDiscarded)domainEvents.get(0)
        draftDiscarded.workingPageId() == workingPage.id()
        Set<WorkingCardId> expectedCurrent =
                (["0","1","2","3","4","5"] as Set).stream().map(WorkingCardId::new).collect(Collectors.toSet())
        Set<WorkingCardId> expectedPrior =
                (["3","4","5"] as Set).stream().map(WorkingCardId::new).collect(Collectors.toSet())
        Set<WorkingCardId> expectedRemoved = new HashSet<>()
        draftDiscarded.currentDraftOutlineItems().containsAll(expectedCurrent)
        draftDiscarded.priorDraftOutlineItems().containsAll(expectedPrior)
        draftDiscarded.draftOutlineItemsRemoved().containsAll(expectedRemoved)
        draftDiscarded.currentDraftOutlineItems().size() == 6
        draftDiscarded.priorDraftOutlineItems().size() == 3
        draftDiscarded.draftOutlineItemsRemoved().size() == 0
    }

    def "publish a working page"() {
        given: "a multi-layer published working page"

        WorkingPage workingPage = workingPageWithDraftWithMultiLayerSubItemsForTest(workingPageFactoryCollaborator, 2, 3)
        workingPage.publish()
        printPublished(workingPage)

        and: "draft removals to the working page"
        workingPage.removeOutlineItem(new WorkingCardId("4"))
        printDraft(workingPage)

        and: "a domain event publisher and generic subscriber"
        List<DomainEvent> domainEvents = DomainEventPublisherHelper.subscribeAndCaptureAll()

        when: "the page is published"
        workingPage.publish()

        then: "the page is published"
        PublishedPage publishedPage = workingPage.publishedPage()
        workingPage.publish()
        publishedPage == workingPage.publishedPage()

        and: "a PagePublished domain event is published with the expected current card ids and published card ids removed"
        domainEvents.size() == 2
        domainEvents.get(0).getClass() == PagePublished.class
        PagePublished pagePublished = (PagePublished)domainEvents.get(0)
        pagePublished.workingPageId() == workingPage.id()
        Set<WorkingCardId> expectedCurrent =
                (["0","1","2","3","8","9","10","11"] as Set).stream().map(WorkingCardId::new).collect(Collectors.toSet())
        Set<WorkingCardId> expectedPrior =
                (["1","2","3","4","5","6","7","8","9","10","11"] as Set).stream().map(WorkingCardId::new).collect(Collectors.toSet())
        Set<WorkingCardId> expectedRemoved =
                (["4","5","6","7"] as Set).stream().map(WorkingCardId::new).collect(Collectors.toSet())
        pagePublished.currentPublishedOutlineItems().containsAll(expectedCurrent)
        pagePublished.priorPublishedOutlineItems().containsAll(expectedPrior)
        pagePublished.publishedOutlineItemsRemoved().containsAll(expectedRemoved)
        pagePublished.currentPublishedOutlineItems().size() == 8
        pagePublished.priorPublishedOutlineItems().size() == 12
        pagePublished.publishedOutlineItemsRemoved().size() == 4
    }
}
