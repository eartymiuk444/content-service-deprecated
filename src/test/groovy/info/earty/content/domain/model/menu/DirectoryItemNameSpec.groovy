package info.earty.content.domain.model.menu

import spock.lang.Specification

class DirectoryItemNameSpec extends Specification {

    def "trying to create an empty name"() {
        when: "trying to create a name with an empty string"
        DirectoryItemName.create("")

        then: "an exception with the expected message is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "name must include at least one character"
    }

    def "trying to create a null name"() {
        when: "trying to create a name with null"
        DirectoryItemName.create(null)

        then: "an exception with the expected message is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "name cannot be null"
    }

    def "creating a name with a valid string"() {
        given: "a valid name string"
        String nameString = "A Valid Name"

        when: "trying to create a name with that string"
        DirectoryItemName name = DirectoryItemName.create(nameString)

        then: "the name is created with the expected string"
        name.nameString() == nameString
    }

}
