package info.earty.content.domain.model.page

import info.earty.content.domain.model.card.WorkingCardId
import spock.lang.Specification

class DraftOutlineItemSpec extends Specification {

    def "try to add duplicate fragments to a draft outline item; to the parent and then to a sub-item"() {
        given: "a parent draft outline item with at least one sub-item and a fragment"

        DraftOutlineItem draftOutlineItem = DraftOutlineItem.create(new WorkingCardId("1"), "Card1")
        draftOutlineItem.addSubItem(DraftOutlineSubItem.create(new WorkingCardId("2"), "ChildCard2"))

        Fragment fragment = new Fragment("fragment")

        when: "trying to add a duplicate fragment to the parent and one of its sub-items"
        draftOutlineItem.addFragment(fragment)
        draftOutlineItem.addFragmentToSubItem(new WorkingCardId("2"), fragment)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "DraftOutlineItem: adding this fragment to the sub-item results in duplicate fragments"
    }

    def "try to add duplicate fragments to a draft outline item; to the sub-item and then to the parent"() {
        given: "a parent draft outline item with at least one sub-item and a fragment"

        DraftOutlineItem draftOutlineItem = DraftOutlineItem.create(new WorkingCardId("1"), "Card1")
        draftOutlineItem.addSubItem(DraftOutlineSubItem.create(new WorkingCardId("2"), "ChildCard2"))

        Fragment fragment = new Fragment("fragment")

        when: "trying to add a duplicate fragment to the parent and one of its sub-items"
        draftOutlineItem.addFragmentToSubItem(new WorkingCardId("2"), fragment)
        draftOutlineItem.addFragment(fragment)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "DraftOutlineItem: setting the fragment to this value results in duplicate fragments"
    }

}
