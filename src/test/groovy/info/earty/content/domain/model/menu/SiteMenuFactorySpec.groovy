package info.earty.content.domain.model.menu

import spock.lang.Specification

class SiteMenuFactorySpec extends Specification {

    private final static SiteMenuFactory siteMenuFactory = new SiteMenuFactory();

    def "creating a site menu with the factory"() {
        given: "a site menu id"
        SiteMenuId siteMenuId = new SiteMenuId("1")

        when: "a site menu is created with the id"
        SiteMenu siteMenu = siteMenuFactory.create(siteMenuId)

        then: "a site is created with a root directory that does not have a path segment"
        !siteMenu.rootDirectory().hasPathSegment()
        siteMenu.findDirectory(siteMenu.rootDirectory().id()).isPresent()
    }

}
