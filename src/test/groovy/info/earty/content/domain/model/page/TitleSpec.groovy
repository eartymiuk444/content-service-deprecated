package info.earty.content.domain.model.page

import spock.lang.Specification

class TitleSpec extends Specification {

    def "trying to create an empty title"() {
        when: "trying to create a title with an empty string"
        Title.create("")

        then: "an exception with the expected message is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "title must include at least one character"
    }

    def "trying to create a null title"() {
        when: "trying to create a title with null"
        Title.create(null)

        then: "an exception with the expected message is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "title cannot be null"
    }

    def "creating a title with a valid string"() {
        given: "a valid title string"
        String titleString = "A Valid Title"

        when: "trying to create a title with that string"
        Title title = Title.create(titleString)

        then: "the title is created with the expected string"
        title.titleString() == titleString
    }

}
