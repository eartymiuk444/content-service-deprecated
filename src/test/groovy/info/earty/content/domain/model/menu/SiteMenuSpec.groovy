package info.earty.content.domain.model.menu

import info.earty.content.domain.model.page.WorkingPageId
import spock.lang.Specification

import static info.earty.content.domain.model.menu.SiteMenuSpecHelper.printMenu

class SiteMenuSpec extends Specification {

    def siteMenuFactoryCollaborator = new SiteMenuFactory()

    def "adding a directory to a menu root"() {
        given: "a blank site menu"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))

        when: "a directory is added to the menu's root directory"
        siteMenu.addDirectory(DirectoryItemName.create("D1"), new PathSegment("d1"), siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        then: "then the root directory contains the added directory"
        siteMenu.rootDirectory().items().size() == 1
        DirectoryItem rootItem1 = siteMenu.rootDirectory().items().get(0)
        rootItem1.isSubDirectory() && rootItem1.name().nameString() == "D1" && rootItem1.pathSegment() == new PathSegment("d1")

        and: "the added directory can be found on the menu by its directory id"
        Directory rootDirectory1 = siteMenu.findDirectory(rootItem1.directoryId()).get()
        rootDirectory1.name().nameString() == "D1" && rootDirectory1.pathSegment() == new PathSegment("d1")
    }

    def "adding a directory to a menu non-root directory"() {
        given: "a site menu with a non-root directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("D1"), new PathSegment("d1"), siteMenu.rootDirectory().id())
        Directory nonRootDirectory = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(0).directoryId()).get()

        when: "a directory is added to the menu's non-root directory"
        siteMenu.addDirectory(DirectoryItemName.create("D2"), new PathSegment("d2"), nonRootDirectory.id())

        printMenu(siteMenu)

        then: "then the non-root directory contains the added directory"
        nonRootDirectory.items().size() == 1
        DirectoryItem nonRootDirectoryItem = nonRootDirectory.items().get(0)
        nonRootDirectoryItem.isSubDirectory() && nonRootDirectoryItem.name().nameString() == "D2" && nonRootDirectoryItem.pathSegment() == new PathSegment("d2")

        and: "the added directory can be found on the menu by its directory id"
        Directory nestedNonRootDirectory = siteMenu.findDirectory(nonRootDirectoryItem.directoryId()).get()
        nestedNonRootDirectory.name().nameString() == "D2" && nestedNonRootDirectory.pathSegment() == new PathSegment("d2")
    }

    def "adding a page to a menu root"() {
        given: "a blank site menu and a working page to add to the menu root"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        DirectoryItemName pageTitle = DirectoryItemName.create("Page 1")
        PathSegment pagePathSegment = new PathSegment("p1")

        when: "a page is added to the menu's root directory"
        siteMenu.addPage(new WorkingPageId("1"), pageTitle, pagePathSegment, siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        then: "then the root directory contains the added page"
        siteMenu.rootDirectory().items().size() == 1

        DirectoryItem rootItem0 = siteMenu.rootDirectory().items().get(0)
        rootItem0.isPage() && rootItem0.name() == pageTitle && rootItem0.pathSegment() == pagePathSegment

        and: "the added page can be found on the menu by its working page id"
        DirectoryItem rootPage0 = siteMenu.findPage(rootItem0.workingPageId()).get()
        rootPage0.isPage() && rootPage0.name() == pageTitle && rootItem0.pathSegment() == pagePathSegment
    }

    def "adding a page to a non menu root"() {
        given: "a site menu with a non-root directory and a working page to add to the non-root directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("D1"), new PathSegment("d1"), siteMenu.rootDirectory().id())
        Directory nonRootDirectory = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(0).directoryId()).get()
        DirectoryItemName pageTitle = DirectoryItemName.create("Page 1")

        when: "a page is added to the menu's non-root directory"
        siteMenu.addPage(new WorkingPageId("1"), pageTitle, new PathSegment("p1"), nonRootDirectory.id())

        printMenu(siteMenu)

        then: "then the non-root directory contains the added page"
        nonRootDirectory.items().size() == 1

        DirectoryItem addedPage = nonRootDirectory.items().get(0)
        addedPage.isPage() && addedPage.name() == pageTitle && addedPage.pathSegment() == new PathSegment("p1")

        and: "the added page can be found on the menu by its working page id"
        DirectoryItem sameAddedPage = siteMenu.findPage(new WorkingPageId("1")).get()
        sameAddedPage == addedPage
    }

    def "trying to add a page more than once"() {
        given: "a site menu with a page"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        DirectoryItemName pageTitle = DirectoryItemName.create("Page 1")
        siteMenu.addPage(new WorkingPageId("1"), pageTitle, new PathSegment("page1"), siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        when: "a working page matching the already added page is added"
        siteMenu.addPage(new WorkingPageId("1"), pageTitle, new PathSegment("p1"), siteMenu.rootDirectory().id())

        then: "then the expected error is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error adding page to parent; menu already contains this page"
    }

    def "trying to add a page more than once to a menu but unique within a directory"() {
        given: "a site menu with a page and a directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        DirectoryItemName pageTitle = DirectoryItemName.create("Page 1")

        siteMenu.addPage(new WorkingPageId("1"), pageTitle, new PathSegment("page1"), siteMenu.rootDirectory().id())

        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        DirectoryId directoryId = siteMenu.rootDirectory().items().get(1).directoryId()

        printMenu(siteMenu)

        when: "a working page matching the already added page is added to the directory"
        siteMenu.addPage(new WorkingPageId("1"), pageTitle, new PathSegment("p1"), directoryId)

        then: "then the expected error is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error adding page to parent; menu already contains this page"
    }

    def "moving a directory up on a menu"() {
        given: "a site menu with a directory with multiple sub-directories and a directory to move up in that directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 2"), new PathSegment("directory2"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 3"), new PathSegment("directory3"), siteMenu.rootDirectory().id())

        DirectoryId toMoveUpId = siteMenu.rootDirectory().items().get(1).directoryId()
        DirectoryId effectivelyMovedDownId = siteMenu.rootDirectory().items().get(0).directoryId()

        printMenu(siteMenu)

        when: "a directory is moved up in the directory with multiple sub-directories"
        siteMenu.rootDirectory().moveSubDirectoryUp(toMoveUpId)

        printMenu(siteMenu)

        then: "the directory is moved up and the directory previously in that position is effectively moved down"
        siteMenu.rootDirectory().items().get(0).directoryId() == toMoveUpId
        siteMenu.rootDirectory().items().get(1).directoryId() == effectivelyMovedDownId
    }

    def "moving a directory down on a menu"() {
        given: "a site menu with a directory with multiple sub-directories and a directory to move down in that directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 2"), new PathSegment("directory2"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 3"), new PathSegment("directory3"), siteMenu.rootDirectory().id())

        DirectoryId toMoveDownId = siteMenu.rootDirectory().items().get(1).directoryId()
        DirectoryId effectivelyMovedUpId = siteMenu.rootDirectory().items().get(2).directoryId()

        printMenu(siteMenu)

        when: "a directory is moved down in the directory with multiple sub-directories"
        siteMenu.rootDirectory().moveSubDirectoryDown(toMoveDownId)

        printMenu(siteMenu)

        then: "the expected directory is moved down and the directory previously in that position is effectively moved up"
        siteMenu.rootDirectory().items().get(2).directoryId() == toMoveDownId
        siteMenu.rootDirectory().items().get(1).directoryId() == effectivelyMovedUpId
    }

    def "moving a directory to a different parent directory"() {
        given: "a site menu with 2 directories in the root; 1 with a sub-directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 2"), new PathSegment("directory2"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 3"), new PathSegment("directory3"), siteMenu.rootDirectory().items().get(0).directoryId())

        Directory directory1 = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(0).directoryId()).get()
        Directory directory2 = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(1).directoryId()).get()
        Directory directory3 = siteMenu.findDirectory(directory1.items().get(0).directoryId()).get()

        printMenu(siteMenu)

        when: "the 1 sub-directory is moved to the other directory in the root"
        siteMenu.moveDirectory(directory3.id(), directory2.id())

        printMenu(siteMenu)

        then: "the old parent no longer has that directory as a sub-directory"
        directory1.items().isEmpty()

        and: "the new parent has that directory as a sub-directory"
        directory2.items().size() == 1
        directory2.items().get(0).directoryId() == directory3.id()
    }

    def "moving a page to a different parent directory"() {
        given: "a site menu with 2 directories in the root; 1 with a page"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 2"), new PathSegment("directory2"), siteMenu.rootDirectory().id())

        WorkingPageId workingPageId = new WorkingPageId("1")
        DirectoryItemName pageTitle = DirectoryItemName.create("Page 1")
        siteMenu.addPage(workingPageId, pageTitle, new PathSegment("page1"), siteMenu.rootDirectory().items().get(0).directoryId())

        Directory directory1 = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(0).directoryId()).get()
        Directory directory2 = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(1).directoryId()).get()

        printMenu(siteMenu)

        when: "the page is moved to the other directory in the root"
        siteMenu.movePage(workingPageId, directory2.id())

        printMenu(siteMenu)

        then: "the old parent no longer has that page"
        directory1.items().isEmpty()

        and: "the new parent has that page"
        directory2.items().size() == 1
        directory2.items().get(0).workingPageId() == workingPageId
    }

    def "change a directory's name"() {
        given: "a site menu with a directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        DirectoryId directoryId = siteMenu.rootDirectory().items().get(0).directoryId()

        printMenu(siteMenu)

        when: "the directory's name is changed"
        siteMenu.changeDirectoryName(directoryId, DirectoryItemName.create("New Directory 1"))

        printMenu(siteMenu)

        then: "the name change is reflected for both the directory and the sub-directory"
        siteMenu.findDirectory(directoryId).get().name().nameString() == "New Directory 1"
        siteMenu.rootDirectory().items().get(0).name().nameString() == "New Directory 1"
    }

    def "change a directory's path segment"() {
        given: "a site menu with a directory and a path segment to update to"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        DirectoryId directoryId = siteMenu.rootDirectory().items().get(0).directoryId()
        PathSegment pathSegment = new PathSegment("newDirectory1")

        printMenu(siteMenu)

        when: "the directory's path segment is changed"
        siteMenu.changeDirectoryPathSegment(directoryId, pathSegment)

        printMenu(siteMenu)

        then: "the path segment change is reflected for both the directory and the sub-directory"
        siteMenu.findDirectory(directoryId).get().pathSegment() == pathSegment
        siteMenu.rootDirectory().items().get(0).pathSegment() == pathSegment
    }

    def "attempt to duplicate a path segment in a directory"() {
        given: "a site menu with at least two directories"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 2"), new PathSegment("directory2"), siteMenu.rootDirectory().id())

        DirectoryId directoryId1 = siteMenu.rootDirectory().items().get(0).directoryId()

        printMenu(siteMenu)

        when: "attempting to update the first directory's path segment to be the same as the second"
        siteMenu.changeDirectoryPathSegment(directoryId1, new PathSegment("directory2"))

        then: "an error is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error changing sub-directory path segment; the directory already contains a directory item with the path segment"
    }

    def "change a page's name"() {
        given: "a site menu with a page"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))

        WorkingPageId workingPageId = new WorkingPageId("1")
        DirectoryItemName pageTitle = DirectoryItemName.create("Page 1")
        siteMenu.addPage(workingPageId, pageTitle, new PathSegment("page1"), siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        when: "the page's name is changed"
        siteMenu.changePageName(workingPageId, DirectoryItemName.create("Page 2"))

        printMenu(siteMenu)

        then: "the name change is reflected for both the menu and the directory"
        siteMenu.findPage(workingPageId).get().name().nameString() == "Page 2"
        siteMenu.rootDirectory().items().get(0).name().nameString() == "Page 2"
    }

    def "change a page's path segment"() {
        given: "a site menu with a page and a path segment to update to"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        WorkingPageId workingPageId = new WorkingPageId("1")
        siteMenu.addPage(workingPageId, DirectoryItemName.create("Page 1"), new PathSegment("page1"), siteMenu.rootDirectory().id())
        PathSegment pathSegment = new PathSegment("newPage1")

        printMenu(siteMenu)

        when: "the page's path segment is changed"
        siteMenu.changePagePathSegment(workingPageId, pathSegment)

        printMenu(siteMenu)

        then: "the path segment change is reflected for both the menu and the directory"
        siteMenu.findPage(workingPageId).get().pathSegment() == pathSegment
        siteMenu.rootDirectory().items().get(0).pathSegment() == pathSegment
    }

    def "attempt to remove the root directory"() {
        given: "a site menu"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))

        printMenu(siteMenu)

        when: "trying to remove the root directory"
        siteMenu.removeDirectory(siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        then: "the expected error is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error removing directory; cannot remove the root directory"
    }

    def "attempt to change the root directory's name"() {
        given: "a site menu"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))

        printMenu(siteMenu)

        when: "trying to change the root directory name"
        siteMenu.changeDirectoryName(siteMenu.rootDirectory().id(), DirectoryItemName.create("ROOT"))

        printMenu(siteMenu)

        then: "the expected error is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error changing directory name; cannot change the name of the root directory"
    }

    def "attempt to move the root directory to a new parent"() {
        given: "a site menu with a non-root directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        when: "trying to move the root directory to a new parent"
        siteMenu.moveDirectory(siteMenu.rootDirectory().id(), siteMenu.rootDirectory().items().get(0).directoryId())

        printMenu(siteMenu)

        then: "the expected error is thrown"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "Error moving directory; root directory cannot be moved"
    }

    def "remove a directory"() {
        given: "a site menu with a single non-root directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())

        DirectoryId directoryId = siteMenu.rootDirectory().items().get(0).directoryId()

        printMenu(siteMenu)

        when: "the single directory is removed"
        siteMenu.removeDirectory(directoryId)

        printMenu(siteMenu)

        then: "the site menu has no non-root directories"
        siteMenu.rootDirectory().items().isEmpty()
    }

    def "remove a page"() {
        given: "a site menu with a single page"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))

        WorkingPageId workingPageId = new WorkingPageId("1")
        siteMenu.addPage(workingPageId, DirectoryItemName.create("Page 1"), new PathSegment("page1"), siteMenu.rootDirectory().id())

        printMenu(siteMenu)

        when: "the single page is removed"
        siteMenu.removePage(workingPageId)

        printMenu(siteMenu)

        then: "the site menu's root has no items"
        siteMenu.rootDirectory().items().isEmpty()
    }

    def "remove a directory with multiple layers"() {
        given: "a site menu with a directory that has directory items"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))

        //ROOT
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())
        siteMenu.addDirectory(DirectoryItemName.create("Directory 2"), new PathSegment("directory2"), siteMenu.rootDirectory().id())
        siteMenu.addPage(new WorkingPageId("1"), DirectoryItemName.create("Page1"), new PathSegment("page1"), siteMenu.rootDirectory().id())

        //Directory 1
        DirectoryId directoryId1 = siteMenu.rootDirectory().items().get(0).directoryId()
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1-1"), new PathSegment("directory1-1"), directoryId1)
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1-2"), new PathSegment("directory1-2"), directoryId1)
        siteMenu.addPage(new WorkingPageId("1-1"), DirectoryItemName.create("Page1-1"), new PathSegment("page1-1"), directoryId1)

        //Directory 1-2
        DirectoryId directoryId1_2 = siteMenu.findDirectory(directoryId1).get().items().get(1).directoryId()
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1-2-1"), new PathSegment("directory1-1"), directoryId1_2)
        siteMenu.addPage(new WorkingPageId("1-2-1"), DirectoryItemName.create("Page1-2-1"), new PathSegment("page1-2-1"), directoryId1_2)
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1-2-2"), new PathSegment("directory1-2"), directoryId1_2)

        printMenu(siteMenu)

        when: "the directory with multiple layers is removed"
        siteMenu.removeDirectory(directoryId1)

        printMenu(siteMenu)

        then: "the site menu completely removes that directory (including any nested directories and pages)"
        siteMenu.findDirectory(directoryId1).isEmpty()
        siteMenu.findDirectory(directoryId1_2).isEmpty()
        siteMenu.rootDirectory().items().size() == 2
    }

    def "making a directory its own parent"() {
        given: "a site menu with a single non-root directory"
        SiteMenu siteMenu = siteMenuFactoryCollaborator.create(new SiteMenuId("1"))
        siteMenu.addDirectory(DirectoryItemName.create("Directory 1"), new PathSegment("directory1"), siteMenu.rootDirectory().id())

        Directory directory1 = siteMenu.findDirectory(siteMenu.rootDirectory().items().get(0).directoryId()).get()

        printMenu(siteMenu)

        when: "trying to make the directory its own parent"
        siteMenu.moveDirectory(directory1.id(), directory1.id())

        printMenu(siteMenu)

        then: "it fails with the expected message"
        Exception e = thrown(IllegalArgumentException)
        e.getMessage() == "SiteMenu: cannot make a directory its own parent"
    }

}
