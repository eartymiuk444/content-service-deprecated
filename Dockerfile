FROM ibm-semeru-runtimes:open-11-jre
COPY target/content-1.2.0.jar content-1.2.0.jar
ENTRYPOINT ["java","-jar","/content-1.2.0.jar"]